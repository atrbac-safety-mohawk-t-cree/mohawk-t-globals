# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.1.0] - 2015-08-16
### Fixed

- Fixed conversion error which produced a system that was not always correct

### Bugs

- AGTUniv10 takes a really long time, and a lot of refinement steps; 
each refinement step is quick, so the abstraction-refinement process might need to updated

### Updated

- Updated the README, forgot some default text and replaced it with informative information


## [1.0.1] - 2015-08-15
### Fixed

- Conversion errors for Mohawk, ASASPTime NSA, and TROLE

### Added

- Changelog
- Version File

