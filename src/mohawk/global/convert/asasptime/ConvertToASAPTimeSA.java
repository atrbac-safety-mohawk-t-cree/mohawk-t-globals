package mohawk.global.convert.asasptime;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.stringtemplate.v4.ST;

import mohawk.global.convert.ConvertTo;
import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;
import mohawk.global.pieces.reduced.query.ASAPTimeSA_Query;
import mohawk.global.pieces.reduced.rules.ASAPTimeSA_Rule;
import mohawk.global.timing.MohawkTiming;

public class ConvertToASAPTimeSA extends ConvertTo {
    public static final Logger logger = Logger.getLogger("mohawk");
    public int canEnableVersion = 2;

    public ConvertToASAPTimeSA(MohawkTiming timing) {
        super(timing);
        tPrefix = "ConvertToASAPTimeSA";
    }

    @Override
    public String convert(MohawkT m, File file, Boolean writeToFile) {
        try {
            ArrayList<ASAPTimeSA_Rule> newRules = new ArrayList<ASAPTimeSA_Rule>();
            ArrayList<Rule> workableRules = new ArrayList<Rule>();

            // Deep Copy Helpers
            /* Timing */timing.startTimer(tPrefix + "_" + "deepcopy");
            RoleHelper roleHelper = new RoleHelper(m.roleHelper);
            TimeIntervalHelper timeIntervalHelper = new TimeIntervalHelper(m.timeIntervalHelper);
            Query workableQuery = new Query(m.query);
            /* Timing */timing.stopTimer(tPrefix + "_" + "deepcopy");

            // removeEnableDisableRules and alwaysOnRules are Deep Copies
            /* Timing */timing.startTimer(tPrefix + "_removeEnableDisableRules");
            // Reduction (3)
            switch (canEnableVersion) {
                case 1 :
                    workableRules = roleHelper.removeEnableDisableRules(m.getAllRules());
                    break;

                case 2 :
                    workableRules = roleHelper.removeEnableDisableRulesv2(m.getAllRules());
                    break;
            }
            /* Timing */timing.stopTimer(tPrefix + "_removeEnableDisableRules");

            if (logger.isLoggable(Level.FINE)) {
                logger.fine("[STEPWISE] Query Before roleHelper.alwaysOnRules Conversion: " + workableQuery);
                logger.fine("[STEPWISE] Rules Before roleHelper.alwaysOnRules Conversion: " + workableRules);
            }

            /* Timing */timing.startTimer(tPrefix + "_" + "alwaysOnRules");
            // Reduction (4)
            workableRules = roleHelper.alwaysOnRules(workableRules, workableQuery, timeIntervalHelper);
            /* Timing */timing.stopTimer(tPrefix + "_" + "alwaysOnRules");

            /* Timing */timing.startTimer(tPrefix + "_" + "reduceToTimeslots");
            // Reduction (2)
            timeIntervalHelper.allowZeroTimeslot = false;// time-slots start from t1 NOT t0!
            timeIntervalHelper.reduceToTimeslots();// Reduce TimeIntervals to Timeslots

            roleHelper.allowZeroRole = false;// roles start from 1 NOT 0!
            roleHelper.setupSortedRoles();// Reduce Roles to Integers

            /* Timing */timing.stopTimer(tPrefix + "_" + "reduceToTimeslots");

            if (logger.isLoggable(Level.FINE)) {
                logger.fine("[STEPWISE] Query Before ASAPTimeSA_Query Conversion: " + workableQuery);
                logger.fine("[STEPWISE] Rules Before toASAPTimeSA_Rules Conversion: " + workableRules);
            }

            // Convert Query to Reduced ASAPTime SA Query and add any extra rules as needed
            // Reduction (1)
            ASAPTimeSA_Query query = toASAPTimeSA_Query(workableQuery, workableRules, roleHelper, timeIntervalHelper);

            // Convert Rules into Reduced ASAPTime SA Rules
            /* Timing */timing.startTimer(tPrefix + "_" + "toASAPTimeSA_Rules");
            for (Rule r : workableRules) {
                newRules.addAll(toASAPTimeSA_Rules(r, roleHelper, timeIntervalHelper));
            }
            /* Timing */timing.stopTimer(tPrefix + "_" + "toASAPTimeSA_Rules");

            // Stats and logging
            numberOfRules = newRules.size();
            if (logger.isLoggable(Level.FINE)) {
                logger.fine("Rules: " + m.getAllRules());
                logger.fine("Reduced Rules: " + newRules);
            }

            // Generate the Converted String
            /* Timing */timing.startTimer(tPrefix + "_" + "template");
            String template = ConvertTo.readFile(this.getClass().getResourceAsStream("ASAPTimeSATemplate.st"));
            ST st = new ST(template);
            st.add("numRoles", roleHelper.numberOfRoles());
            st.add("numTimeslots", timeIntervalHelper.sizeReduced());
            st.add("goalRole", query.goalRole);
            st.add("goalTimeslot", query.goalTimeslot);
            st.add("rules_sa", newRules);
            convertedStr = st.render();
            /* Timing */timing.stopTimer(tPrefix + "_" + "template");

            // Write the converted string out to "file + getFileExtenstion()"
            if (writeToFile) {
                /* Timing */timing.startTimer(tPrefix + "_" + "Files.write");
                File convertedFile = new File(file.getAbsolutePath() + getFileExtenstion());

                if (!convertedFile.exists()) {
                    convertedFile.createNewFile();
                }
                Files.write(convertedFile.toPath(), convertedStr.getBytes());

                /** Legacy code: use if the Template Files are not working
                 * Charset charset = Charset.forName("US-ASCII");
                 * try (BufferedWriter writer = Files.newBufferedWriter(convertedFile.toPath(), charset)) {
                 * writer.write(convertedStr);
                 * 
                 * for (ASAPTimeSA_Rule r : newRules) {
                 * writer.write(r.getString() + "\n");
                 * }
                 * writer.flush();
                 * writer.close();
                 * } catch (IOException x) {
                 * System.err.format("IOException: %s%n", x);
                 * } */

                /* Timing */timing.stopTimer(tPrefix + "_" + "Files.write");
            }
            lastError = null;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.warning(errors.toString());

            logger.warning("[ERROR] Unable to convert to ASAPTime NSA: " + e.getMessage());
            lastError = "Error ConvertToRanise.convert.Exception #1";
        }

        return convertedStr;
    }

    /** Directly converts the Query's timeslot to the reduced timeslot and then it copies the Goal Role is there is only
     * 1. If there is more than 1 Goal Role in the Query then it will create a new Goal Role that is unique and create a
     * new Rule such that only someone who has reached the Goal Roles in the original Query can assign this new Goal
     * Role and thus make the system REACHABLE.
     * 
     * From the paper, this is the reduction (1) Query, Type 1
     * 
     * WARNING: if this function has to create a new role then it will call roleHelper.addUniqueRole();
     * 
     * @param query
     * @param rules
     * @param roleHelper
     * @param timeIntervalHelper
     * @return */
    public ASAPTimeSA_Query toASAPTimeSA_Query(Query query, ArrayList<Rule> rules, RoleHelper roleHelper,
            TimeIntervalHelper timeIntervalHelper) {
        ASAPTimeSA_Query q = new ASAPTimeSA_Query();

        // Set the Goal Timeslot
        SortedSet<Integer> goalTimeslots = timeIntervalHelper.indexOfReduced(query._timeslot);
        if (goalTimeslots.size() != 1) {
            logger.severe("goalTimeslots size: " + goalTimeslots.size());
            throw new IllegalArgumentException("The reduced query must have only one timeslot");
        }
        q.goalTimeslot = goalTimeslots.first();

        // Set the Goal Role
        if (query._roles.size() == 0) {
            logger.severe("query._roles size: " + query._roles.size());
            throw new IllegalArgumentException("The query must have atleast one Goal Role");
        }

        if (query._roles.size() == 1) {
            q.goalRole = roleHelper.indexOf(query._roles.get(0));
        } else {
            // Create a new role to act as the Goal Role
            Role newGoalRole = roleHelper.addUniqueRole("goalRole");
            Rule newRule = new Rule(query, newGoalRole);
            rules.add(newRule);

            q.goalRole = roleHelper.indexOf(newGoalRole);
        }

        return q;
    }

    /** @param rule
     * @param roleHelper
     * @param timeIntervalHelper
     * @return */
    public ArrayList<ASAPTimeSA_Rule> toASAPTimeSA_Rules(Rule rule, RoleHelper roleHelper,
            TimeIntervalHelper timeIntervalHelper) {

        ArrayList<ASAPTimeSA_Rule> rules = new ArrayList<ASAPTimeSA_Rule>();

        SortedSet<Integer> adminInterval = timeIntervalHelper.indexOfReduced(rule._adminTimeInterval);
        SortedSet<Integer> roleSchedule = new TreeSet<Integer>();
        ArrayList<Integer> precondition = new ArrayList<Integer>();

        // Convert RoleSchedule to the New Timeslots
        if (rule._roleSchedule.size() != 0) {
            for (TimeSlot ts : rule._roleSchedule) {
                roleSchedule.addAll(timeIntervalHelper.indexOfReduced(ts));
            }
        } else {
            // Grab all of the reduced timeslots
            roleSchedule = timeIntervalHelper.indexOfReduced(timeIntervalHelper.getAllTimeInterval());
        }

        // Convert Preconditions to the Role Indexes
        // System.out.println("[toASAPTimeSA_Rules] Rule: " + rule);
        Integer id;
        for (Role r : rule._preconditions) {
            id = roleHelper.indexOf(r);
            if (id == null) {
                logger.log(Level.SEVERE, "Could not find role: " + r + " in " + roleHelper.toString());
                throw new NullPointerException("Could not find role: " + r + " in " + roleHelper.toString());
            }
            precondition.add(id);
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Timeslots: " + timeIntervalHelper._timeIntervals + "; Reduced Timeslots: "
                    + timeIntervalHelper._reducedTimeIntervals);
            logger.fine("Admin Interval: " + adminInterval + "Precondition: " + rule._preconditions
                    + "; Role Schedule: " + roleSchedule + "; Target Role: " + rule._role);
        }

        ASAPTimeSA_Rule rule_t;
        for (Integer adminTimeslot : adminInterval) {
            for (Integer roleTimeslot : roleSchedule) {
                rule_t = new ASAPTimeSA_Rule();

                rule_t.ruleType = rule._type.toRanise();
                if (!rule._adminRole.isAllRoles()) {
                    logger.log(Level.SEVERE, "Rule does not have TRUE for the admin role: " + rule);
                    throw new IllegalArgumentException("Rule contains a Admin Role that isn't TRUE!");
                }
                rule_t.adminTime = adminTimeslot;
                rule_t.role = roleHelper.indexOf(rule._role);
                rule_t.precondition = precondition;
                rule_t.roleTime = roleTimeslot;

                if (logger.isLoggable(Level.FINE)) {
                    logger.fine("Adding to rules: " + rule_t);
                }

                rules.add(rule_t);
            }
        }

        return rules;
    }

    @Override
    public String getFileExtenstion() {
        return fileExt.ASASPTime_SA;
    }

}
