package mohawk.global.convert.mohawk;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.stringtemplate.v4.ST;

import mohawk.global.convert.ConvertTo;
import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;
import mohawk.global.pieces.reduced.query.Mohawk_Query;
import mohawk.global.pieces.reduced.rules.MohawkCA_Rule;
import mohawk.global.pieces.reduced.rules.MohawkCR_Rule;
import mohawk.global.timing.MohawkTiming;

public class ConvertToMohawk extends ConvertTo {
    public static final Logger logger = Logger.getLogger("mohawk");
    public boolean shortRolenames = false;
    public int canEnableVersion = 2;

    public ConvertToMohawk(MohawkTiming timing) {
        super(timing);
        tPrefix = "ConvertToMohawk";
    }
    public ConvertToMohawk(MohawkTiming timing, String timerPrefix) {
        super(timing);
        tPrefix = timerPrefix;
    }

    @Override
    public String convert(MohawkT m, File f, Boolean writeToFile) {
        ArrayList<MohawkCA_Rule> newCARules = new ArrayList<MohawkCA_Rule>();
        ArrayList<MohawkCR_Rule> newCRRules = new ArrayList<MohawkCR_Rule>();
        ArrayList<Rule> workableRules = new ArrayList<Rule>();

        // Deep Copy Helpers
        /* Timing */timing.startTimer(tPrefix + "_" + "deepcopy");
        RoleHelper roleHelper = new RoleHelper(m.roleHelper);
        RoleHelper roleHelperTemporality = null;
        TimeIntervalHelper timeIntervalHelper = new TimeIntervalHelper(m.timeIntervalHelper);
        Query workableQuery = new Query(m.query);
        /* Timing */timing.stopTimer(tPrefix + "_deepcopy");

        try {
            /* Timing */timing.startTimer(tPrefix + "_removeEnableDisableRules");
            // Reduction (3)
            switch (canEnableVersion) {
                case 1 :
                    workableRules = roleHelper.removeEnableDisableRules(m.getAllRules());
                    break;

                case 2 :
                    workableRules = roleHelper.removeEnableDisableRulesv2(m.getAllRules());
                    break;
            }
            /* Timing */timing.stopTimer(tPrefix + "_removeEnableDisableRules");

            // Convert Query to Reduced ASAPTime SA Query and add any extra rules as needed
            // Mohawk_Query query = toMohawk_Query(workableQuery, workableRules, roleHelper, timeIntervalHelper);

            /* Timing */timing.startTimer(tPrefix + "_alwaysOnRules");
            // Reduction (4)
            workableRules = roleHelper.alwaysOnRules(workableRules, workableQuery, timeIntervalHelper);
            /* Timing */timing.stopTimer(tPrefix + "_alwaysOnRules");

            // Convert Query to Reduced ASAPTime SA Query and add any extra rules as needed
            // Reduction (1)
            Mohawk_Query query = toMohawk_Query(workableQuery, workableRules, roleHelper, timeIntervalHelper);

            /* Timing */timing.startTimer(tPrefix + "_removeUninvokablePrecondtions");
            // Reduction (6) - Optimization
            workableRules = roleHelper.removeUninvokablePrecondtions(workableRules, false);
            /* Timing */timing.stopTimer(tPrefix + "_removeUninvokablePrecondtions");

            if (logger.isLoggable(Level.FINE)) {
                logger.fine("[STEPWISE] Rules Before roleHelperTemporality.removePrecondtionsCanRevoke Conversion: "
                        + workableRules);
            }

            /* Timing */timing.startTimer(tPrefix + "_removePrecondtionsCanRevoke");
            // Reduction (6)
            workableRules = roleHelper.removePrecondtionsCanRevoke(workableRules, false);
            /* Timing */timing.stopTimer(tPrefix + "_removePrecondtionsCanRevoke");

            if (logger.isLoggable(Level.FINE)) {
                logger.fine(
                        "[STEPWISE] Rules Before roleHelperTemporality.removeTemporality Conversion: " + workableRules);
            }

            /* Timing */timing.startTimer(tPrefix + "_removeTemporality");
            // Reduction (5)
            roleHelperTemporality = new RoleHelper(roleHelper);
            ArrayList<Object> reducedRules = roleHelperTemporality.removeTemporality(workableRules, timeIntervalHelper);
            /* Timing */timing.stopTimer(tPrefix + "_removeTemporality");

            if (logger.isLoggable(Level.FINE)) {
                logger.fine("[STEPWISE] Rules After CA and CR Conversion: " + workableRules);
            }

            if (shortRolenames) {
                logger.info("Creating Short Rolenames");
                roleHelperTemporality.setupSortedRoles();
            }

            // Convert Rules into Reduced Mohawk CA and CR Rules (5)
            /* Timing */timing.startTimer(tPrefix + "_convertRules");
            for (Object r : reducedRules) {
                if (r instanceof MohawkCA_Rule) {
                    if (shortRolenames) {
                        ((MohawkCA_Rule) r).shortRolenames(roleHelperTemporality);
                    }
                    newCARules.add((MohawkCA_Rule) r);
                } else if (r instanceof MohawkCR_Rule) {
                    if (shortRolenames) {
                        ((MohawkCR_Rule) r).shortRolenames(roleHelperTemporality);
                    }
                    newCRRules.add((MohawkCR_Rule) r);
                } else {
                    throw new IllegalArgumentException("Can only convert Can Assign and Can Revoke Rules in Mohawk");
                }
            }
            /* Timing */timing.stopTimer(tPrefix + "_convertRules");

            // Reduction (1)
            boolean queryFound = query.finalize(roleHelperTemporality, shortRolenames);
            if (queryFound == false) { throw new Exception("The query could not find the Goal Role!"); }
            // Stats and logging
            numberOfRules = newCARules.size() + newCRRules.size();

            /* Timing */timing.startTimer(tPrefix + "_template");
            String template = ConvertTo.readFile(this.getClass().getResourceAsStream("mohawkTemplate.st"));
            ST st = new ST(template);
            if (shortRolenames) {
                st.add("roles", roleHelperTemporality.getArrayShortNames());
                st.add("adminuser", "AU");
                st.add("adminrole", "A");
                st.add("users", new String[]{"AU", "User"});
                st.add("specuser", "User");// Can't use 'U'
            } else {
                st.add("roles", roleHelperTemporality.getArray());
                st.add("adminuser", "AdminUser");
                st.add("adminrole", "AdminRole");
                st.add("users", new String[]{"AdminUser", "User"});
                st.add("specuser", "User");
            }
            st.add("specrole", query.specRole);
            st.add("canrevoke", newCRRules);
            st.add("canassign", newCARules);
            convertedStr = st.render();
            /* Timing */timing.stopTimer(tPrefix + "_template");

            if (writeToFile) {
                /* Timing */timing.startTimer(tPrefix + "_writeFile");
                File convertedFile = new File(f.getAbsolutePath() + getFileExtenstion());

                if (!convertedFile.exists()) {
                    convertedFile.createNewFile();
                }
                Files.write(convertedFile.toPath(), convertedStr.getBytes());
                /* Timing */timing.stopTimer(tPrefix + "_writeFile");
            }

            lastError = null;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.warning(errors.toString());

            logger.warning("[ERROR] Unable to convert to Mohawk: " + e.getMessage());
            lastError = "Error ConvertToRanise.convert.Exception #1";

            if (logger.isLoggable(Level.FINE)) {
                if (roleHelperTemporality == null) {
                    roleHelper.dumpRoleSet("ToMohawkRoleHelperDump.txt");
                    roleHelper.dumpHashedRoles("ToMohawkRoleHelperDump_Hashed.txt");
                } else {
                    roleHelperTemporality.dumpRoleSet("ToMohawkRoleHelperDump.txt");
                    roleHelperTemporality.dumpHashedRoles("ToMohawkRoleHelperDump_Hashed.txt");
                }
            }
        }
        return convertedStr;
    }

    private Mohawk_Query toMohawk_Query(Query query, ArrayList<Rule> newCARules, RoleHelper roleHelper,
            TimeIntervalHelper timeIntervalHelper) throws Exception {
        // Checking Conditions
        if (query._roles.size() <= 0) { throw new Exception("The query must have atleast one Goal Role"); }

        Role newGoalRole = roleHelper.addUniqueRole("goalRole");
        Mohawk_Query q = new Mohawk_Query(newGoalRole, query._timeslot);
        Rule newRule = new Rule();
        newRule._type = RuleType.ASSIGN;
        newRule._adminRole = Role.allRoles();
        newRule._adminTimeInterval = timeIntervalHelper.getAllTimeInterval();
        newRule._preconditions.addAll(query._roles);
        newRule._roleSchedule = new ArrayList<TimeSlot>(Arrays.asList(query._timeslot));
        newRule._role = newGoalRole;

        newCARules.add(newRule);

        return q;
    }

    @Override
    public String getFileExtenstion() {
        return fileExt.Mohawk;
    }

}
