package mohawk.global.convert.nusmv;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.Logger;

import org.stringtemplate.v4.ST;

import mohawk.global.convert.ConvertTo;
import mohawk.global.pieces.*;
import mohawk.global.pieces.reduced.nusmv.NuSMVRule_Role;
import mohawk.global.timing.MohawkTiming;

public class ConvertToNuSMV extends ConvertTo {
    public static final Logger logger = Logger.getLogger("mohawk");

    /**
     * Allows for multiple version of the reduction. Currently implemented version are:
     * <li><b>1</b> - Normal reduction
     * <li><b>2</b> - Reduces the size of the RoleEnablement to only =
     *                |admin-roles \cup CE/CD-target-roles \cup CE/CD-precondition-roles|x|timeslots|
     */
    public int VERSION = 1;

    /** If {@link ConvertToNuSMV#convertedFile} has the parameter {@code writeToFile} set to TRUE, then this field will
     * be updated with the file that contains the converted SMV string. */
    public File convertedFile = null;

    public ConvertToNuSMV(MohawkTiming timing) {
        super(timing);
        tPrefix = "ConvertToNuSMV";
    }

    public ConvertToNuSMV(MohawkTiming timing, String timerPrefix) {
        super(timing);
        tPrefix = timerPrefix;
    }

    @Override
    public String getFileExtenstion() {
        return ".smv";
    }

    /** Converts Mohawk+T policies to equivalent NuSMV Model Checker instances
     * <br/>
     * <br/>
     * {@inheritDoc} */
    @Override
    public String convert(MohawkT m, File file, Boolean writeToFile) {
        try {
            /* Timing */timing.startTimer(tPrefix + "_" + "sortRolesReduceTimeslots");

            switch (VERSION) {
                case 1 :
                default :
                    logger.info("[ConvertToNuSMV] Using Verison 1");
                    break;
                case 2 :
                    logger.info("[ConvertToNuSMV] Using Verison 2");
                    break;
            }

            //////////////////////////////////////////////////////////////////////////////////////////
            // REDUCTION
            // Reduce Roles to Integers
            m.roleHelper.allowZeroRole = false;// roles start from 1 NOT 0!
            m.roleHelper.setupSortedRoles();
            // Reduce TimeIntervals to Timeslots
            m.timeIntervalHelper.allowZeroTimeslot = false;// time-slots start from t1 NOT t0!
            // Reduction (2)
            m.timeIntervalHelper.reduceToTimeslots();
            /* Timing */timing.stopTimer(tPrefix + "_" + "sortRolesReduceTimeslots");

            m.indexAllRules();
            // REDUCTION
            //////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////
            // GATHER TEMPLATE VARIABLE INFORMATION
            Set<Role> adminRoles = m.roleHelper.getAdminRoles();

            Integer caRuleCount = m.canAssign.numberOfRules();
            Integer crRuleCount = m.canRevoke.numberOfRules();
            Integer ceRuleCount = m.canEnable.numberOfRules();
            Integer cdRuleCount = m.canDisable.numberOfRules();

            int numUsers = adminRoles.size() + 1;

            /* Generate a list of users (roles + 1) to loop through */
            ArrayList<Integer> users = new ArrayList<>(numUsers);
            for (int i = 1; i <= numUsers; i++) {
                users.add(i);
            }

            /* Generate a list of Role/Timeslots that need to be state variables */
            Set<RoleTimeSlot> roleTimeslots = getUserRoleTimeslots(m);

            /* Generate a list of Role/Timeslots that need to be state variables */
            Set<RoleTimeSlot> enabledRoleTimeslots = getEnabledRoles(m, VERSION);

            /* Generate a list of NuSMVRule_Role */
            ArrayList<NuSMVRule_Role> enabledRoleRules = getEnabledRoleRules(m, enabledRoleTimeslots);

            /* Generate a list of NuSMVRule_Role */
            ArrayList<NuSMVRule_Role> roleTimeslotRules = getRoleTimeslotRules(m, roleTimeslots);

            // ---------------------------------------------------------------------
            /* Timing */timing.startTimer(tPrefix + "_" + "ruleMappingString");
            String ruleMappingStr = ruleMapping(m);
            /* Timing */timing.stopTimer(tPrefix + "_" + "ruleMappingString");

            /* Timing */timing.startTimer(tPrefix + "_" + "roleMappingString");
            String roleMappingStr = roleTimeslotMapping(m);
            /* Timing */timing.stopTimer(tPrefix + "_" + "roleMappingString");
            // GATHER TEMPLATE VARIABLE INFORMATION
            //////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////
            // GENERATE THE CONVERTED STRING
            /* Timing */timing.startTimer(tPrefix + "_" + "template");

            String templateName = "";
            switch (VERSION) {
                case 2 :
                    templateName = "nusmvTemplate-v2.st";
                    break;
                case 1 :
                default :
                    templateName = "nusmvTemplate-v1.st";
            }

            String template = ConvertTo.readFile(this.getClass().getResourceAsStream(templateName));
            ST st = new ST(template);
            st.add("policyFile", m.policyFilename);
            st.add("generatorName", m.generatorName);
            st.add("policyComment", m.comment);
            st.add("datetime", Calendar.getInstance().getTime().toString());
            st.add("numUsers", numUsers);
            st.add("numRoles", m.roleHelper.numberOfRoles());
            st.add("numAdminRoles", adminRoles.size());
            st.add("numTimeSlots", m.timeIntervalHelper.sizeReduced());

            st.add("queryStr", m.query.getString());
            st.add("goalRoles",
                    RoleTimeSlot.grabAll(m.query._roles, m.query._timeslot, m.timeIntervalHelper, m.roleHelper));

            st.add("numCARules", caRuleCount);
            st.add("numCRRules", crRuleCount);
            st.add("numCERules", ceRuleCount);
            st.add("numCDRules", cdRuleCount);

            st.add("caruleslist", stringList("CA", caRuleCount)
                    + ((caRuleCount > 0 & (crRuleCount > 0 | ceRuleCount > 0 | cdRuleCount > 0)) ? "," : ""));
            st.add("crruleslist", stringList("CR", crRuleCount)
                    + ((crRuleCount > 0 & (ceRuleCount > 0 | cdRuleCount > 0)) ? "," : ""));
            st.add("ceruleslist", stringList("CE", ceRuleCount) + ((ceRuleCount > 0 & (cdRuleCount > 0)) ? "," : ""));
            st.add("cdruleslist", stringList("CD", cdRuleCount));

            st.add("users", users);
            st.add("roleTimeslots", roleTimeslots);
            st.add("enabledRoles", enabledRoleTimeslots);

            st.add("enabledRoleRules", enabledRoleRules);
            st.add("roleTimeslotRules", roleTimeslotRules);

            convertedStr = st.render();
            /* Timing */timing.stopTimer(tPrefix + "_" + "template");
            // GENERATE THE CONVERTED STRING
            //////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////
            // WRITE NUSMV TO FILE
            /** Write the converted string out to {@link ConvertToNuSMV#convertedFile} */
            if (writeToFile) {
                /* Timing */timing.startTimer(tPrefix + "_" + "Files.write");
                if (file.getAbsolutePath().endsWith(getFileExtenstion())) {
                    convertedFile = file;
                } else {
                    convertedFile = new File(file.getAbsolutePath() + getFileExtenstion());
                }

                if (!convertedFile.exists()) {
                    convertedFile.createNewFile();
                }
                logger.info("Writing NuSMV file to:" + convertedFile.getAbsolutePath());
                Files.write(convertedFile.toPath(), convertedStr.getBytes());

                // Rule Mapping file
                File ruleMapFile = new File(convertedFile.getAbsolutePath() + ".rulemapping");

                if (!ruleMapFile.exists()) {
                    ruleMapFile.createNewFile();
                }
                logger.info("Writing Rule Mapping file to:" + ruleMapFile.getAbsolutePath());
                Files.write(ruleMapFile.toPath(), ruleMappingStr.getBytes());

                // Role Mapping file
                File roleMapFile = new File(convertedFile.getAbsolutePath() + ".roletimemapping");

                if (!roleMapFile.exists()) {
                    roleMapFile.createNewFile();
                }
                logger.info("Writing Role Mapping file to:" + roleMapFile.getAbsolutePath());
                Files.write(roleMapFile.toPath(), roleMappingStr.getBytes());
                /* Timing */timing.stopTimer(tPrefix + "_" + "Files.write");
            }
            // WRITE NUSMV TO FILE
            //////////////////////////////////////////////////////////////////////////////////////////

            // If we have reached here without any exceptions, then we did so successfully and no error
            lastError = null;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.warning(errors.toString());

            logger.warning("[ERROR] Unable to convert to NuSMV: " + e.getMessage());
            lastError = "Error ConvertToRanise.convert.Exception #1";
        }

        return convertedStr;
    }

    private ArrayList<NuSMVRule_Role> getRoleTimeslotRules(MohawkT m, Set<RoleTimeSlot> roleTimeslots) {
        /** Role Index --> NuSMVRule_Role{} */
        HashMap<Integer, NuSMVRule_Role> roleMap = new HashMap<>(m.numberOfRoles());

        for (RoleTimeSlot rts : roleTimeslots) {
            if (!roleMap.containsKey(rts._role)) {
                roleMap.put(rts._role, new NuSMVRule_Role(rts._role));
            }
            NuSMVRule_Role tmp = roleMap.get(rts._role);
            if (tmp == null) {
                // How did this happen!!
                throw new ArrayIndexOutOfBoundsException("Something happened that I cannot really explain!?");
            }

            tmp.addTimeslot(rts._time);
        }

        ArrayList<Rule> allRules = new ArrayList<>(m.canAssign.numberOfRules() + m.canRevoke.numberOfRules());
        allRules.addAll(m.canAssign.getRules());
        allRules.addAll(m.canRevoke.getRules());

        for (Rule r : allRules) {
            Integer role = m.roleHelper.indexOf(r._role);

            NuSMVRule_Role tmp = roleMap.get(role);
            if (tmp != null) {
                tmp.addRule(m, r);
            } else {
                logger.fine("getEnabledRoleRules: Skipping role: " + r._role);
            }
        }

        return new ArrayList<NuSMVRule_Role>(roleMap.values());
    }

    private ArrayList<NuSMVRule_Role> getEnabledRoleRules(MohawkT m, Set<RoleTimeSlot> enabledRoles) {
        /** Role Index --> NuSMVRule_Role{} */
        HashMap<Integer, NuSMVRule_Role> roleMap = new HashMap<>(m.numberOfRoles());

        for (RoleTimeSlot rts : enabledRoles) {
            if (!roleMap.containsKey(rts._role)) {
                roleMap.put(rts._role, new NuSMVRule_Role(rts._role));
            }
            NuSMVRule_Role tmp = roleMap.get(rts._role);
            if (tmp == null) {
                // How did this happen!!
                throw new ArrayIndexOutOfBoundsException("Something happened that I cannot really explain!?");
            }

            tmp.addTimeslot(rts._time);
        }

        ArrayList<Rule> allRules = new ArrayList<>(m.canEnable.numberOfRules() + m.canDisable.numberOfRules());
        allRules.addAll(m.canEnable.getRules());
        allRules.addAll(m.canDisable.getRules());

        for (Rule r : allRules) {
            Integer role = m.roleHelper.indexOf(r._role);

            NuSMVRule_Role tmp = roleMap.get(role);
            if (tmp != null) {
                tmp.addRule(m, r);
            } else {
                logger.fine("getEnabledRoleRules: Skipping role: " + r._role);
            }
        }

        return new ArrayList<NuSMVRule_Role>(roleMap.values());
    }

    private Set<RoleTimeSlot> getEnabledRoles(MohawkT m, int version) {
        Set<RoleTimeSlot> enabledRoles = new HashSet<>();

        switch (version) {
            default :
            case 1 : {
                // GRAB ALL ROLES x TIMESLOTS
                for (int r = 1; r <= m.roleHelper._hashedRoles.size(); r++) {
                    for (int t = 1; t <= m.timeIntervalHelper.getNumberOfTimeSlots(); t++) {
                        enabledRoles.add(new RoleTimeSlot(r, t));
                    }
                }
            }
                break;
            case 2 : {
                // Grab only the roles that are used in CE/CD preconditions and Admin conditions
                for (int r = 1; r <= m.roleHelper.getAdminRoles().size(); r++) {
                    for (int t = 1; t <= m.timeIntervalHelper.getNumberOfTimeSlots(); t++) {
                        enabledRoles.add(new RoleTimeSlot(r, t));
                    }
                }
            }
        }

        return enabledRoles;
    }

    private Set<RoleTimeSlot> getUserRoleTimeslots(MohawkT m) {
        Set<RoleTimeSlot> roleTimeslots = new HashSet<>();
        /* TODO: limit the number of required state variables
        // List of Assignable Roles
        for (Rule r : m.canAssign.getRules()) {
            roleTimeslots.addAll(RoleTimeSlot.grabAll(r, m.timeIntervalHelper, m.roleHelper));
        }*/

        for (int r = 1; r <= m.roleHelper._hashedRoles.size(); r++) {
            for (int t = 1; t <= m.timeIntervalHelper.getNumberOfTimeSlots(); t++) {
                roleTimeslots.add(new RoleTimeSlot(r, t));
            }
        }
        return roleTimeslots;
    }

    private String ruleMapping(MohawkT m) {
        StringBuilder sb = new StringBuilder(50 * m.numberOfRules());
        sb.append("/*\nRule Mapping\n\n");

        if (m.generatedComment == null) {
            m.generateComment(false);
        }

        sb.append(m.generatedComment).append("\n\n");

        sb.append("CanAssign Rules : ").append(m.canAssign.numberOfRules());
        sb.append("\nCanRevoke Rules : ").append(m.canRevoke.numberOfRules());
        sb.append("\nCanEnable Rules : ").append(m.canEnable.numberOfRules());
        sb.append("\nCanDisable Rules: ").append(m.canDisable.numberOfRules());
        sb.append("\n*/\n\n").append(Rule.getInfoCodeHeader());
        int i;

        sb.append("\n\nCanAssign Rules:\n");
        i = 1;
        for (Rule r : m.canAssign.getRules()) {
            sb.append(r.getString(i, 0)).append("\n");
            i++;
        }
        sb.append("-----------------------------------------------------------------\n");

        sb.append("\n\nCanRevoke Rules:\n");
        i = 1;
        for (Rule r : m.canRevoke.getRules()) {
            sb.append(r.getString(i, 0)).append("\n");
            i++;
        }
        sb.append("-----------------------------------------------------------------\n");

        sb.append("\n\nCanEnable Rules:\n");
        i = 1;
        for (Rule r : m.canEnable.getRules()) {
            sb.append(r.getString(i, 0)).append("\n");
            i++;
        }
        sb.append("-----------------------------------------------------------------\n");

        sb.append("\n\nCanDisable Rules:\n");
        i = 1;
        for (Rule r : m.canDisable.getRules()) {
            sb.append(r.getString(i, 0)).append("\n");
            i++;
        }
        sb.append("-----------------------------------------------------------------\n");

        return sb.toString();
    }

    private String roleTimeslotMapping(MohawkT m) {
        StringBuilder sb = new StringBuilder(50 * m.numberOfRules());
        sb.append("Role Mapping\n");
        sb.append("-----------------------------------------------------------------\n");
        sb.append("Input Role Name = NuSMV Role Name\n");

        for (String r : m.roleHelper._hashedRoles.keySet()) {
            sb.append(r).append(" = r").append(m.roleHelper._hashedRoles.get(r)).append("\n");
        }
        sb.append("-----------------------------------------------------------------\n\n\n");

        sb.append("Time Interval Mapping\n");
        sb.append("-----------------------------------------------------------------\n");
        sb.append("Input Time Interval = NuSMV Time Slot\n");

        for (TimeInterval ti : m.timeIntervalHelper._reducedTimeIntervals.keySet()) {
            sb.append(ti).append(" = t").append(m.timeIntervalHelper._reducedTimeIntervals.get(ti)).append("\n");
        }
        sb.append("-----------------------------------------------------------------\n");

        return sb.toString();
    }

    private String stringList(String prefix, Integer count) {
        return stringList(prefix, count, ",");
    }

    private String stringList(String prefix, Integer count, String suffix) {
        StringBuilder sb = new StringBuilder((prefix.length() + count.toString().length()) * count);

        for (int i = 1; i <= count - 1; i++) {
            sb.append(prefix).append(i).append(suffix);
        }
        /*Add the last one*/
        if (count > 0) {
            sb.append(prefix).append(count);
        }

        return sb.toString();
    }
}
