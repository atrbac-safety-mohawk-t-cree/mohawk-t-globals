package mohawk.global.convert.tred;

import mohawk.global.timing.MohawkTiming;

public class ConvertToTRule extends ConvertToTRole {
    public ConvertToTRule(MohawkTiming timing) {
        super(timing);
    }

    @Override
    public String getFileExtenstion() {
        return fileExt.TRule;
    }
}
