package mohawk.global.convert.tred;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.stringtemplate.v4.ST;

import mohawk.global.convert.ConvertTo;
import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;
import mohawk.global.pieces.reduced.query.TRole_Query;
import mohawk.global.pieces.reduced.rules.TRole_Rule;
import mohawk.global.timing.MohawkTiming;

public class ConvertToTRole extends ConvertTo {
    public static final Logger logger = Logger.getLogger("mohawk");
    public int canEnableVersion = 2;

    public ConvertToTRole(MohawkTiming timing) {
        super(timing);
        tPrefix = "ConvertToTRole";
    }

    @Override
    public String convert(MohawkT m, File file, Boolean writeToFile) {
        try {
            ArrayList<TRole_Rule> newRules = new ArrayList<TRole_Rule>();
            ArrayList<Rule> workableRules = new ArrayList<Rule>();

            // Deep Copy Helpers
            /* Timing */timing.startTimer(tPrefix + "_" + "deepcopy");
            RoleHelper roleHelper = new RoleHelper(m.roleHelper);
            TimeIntervalHelper timeIntervalHelper = new TimeIntervalHelper(m.timeIntervalHelper);
            Query workableQuery = new Query(m.query);
            /* Timing */timing.stopTimer(tPrefix + "_" + "deepcopy");

            /* Timing */timing.startTimer(tPrefix + "_removeEnableDisableRules");
            // Reduction (3)
            switch (canEnableVersion) {
                case 1 :
                    workableRules = roleHelper.removeEnableDisableRules(m.getAllRules());
                    break;

                case 2 :
                    workableRules = roleHelper.removeEnableDisableRulesv2(m.getAllRules());
                    break;
            }
            /* Timing */timing.stopTimer(tPrefix + "_removeEnableDisableRules");

            /* Timing */timing.startTimer(tPrefix + "_" + "alwaysOnRules");
            // Reduction (4)
            workableRules = roleHelper.alwaysOnRules(workableRules, workableQuery, timeIntervalHelper);
            /* Timing */timing.stopTimer(tPrefix + "_" + "alwaysOnRules");

            // Reduce Roles to Integers
            roleHelper.allowZeroRole = false;// roles start from 1 NOT 0!
            roleHelper.setupSortedRoles();

            // Convert Query to Reduced ASAPTime SA Query and add any extra rules as needed
            TRole_Query query = toTRole_Query(workableQuery, workableRules, roleHelper, timeIntervalHelper);

            // Finalize the Query
            // Reduction (1)
            query.finalize(roleHelper);

            // Stats and logging
            numberOfRules = newRules.size();
            if (logger.isLoggable(Level.FINE)) {
                logger.fine("Rules: " + m.getAllRules());
                logger.fine("Reduced Rules: " + newRules);
            }

            // Generate the Converted String
            /* Timing */timing.startTimer(tPrefix + "_" + "template");
            String template = ConvertTo.readFile(this.getClass().getResourceAsStream("uzunTemplate.st"));
            ST st = new ST(template);
            st.add("numRoles", roleHelper.numberOfRoles());
            st.add("numTimeSlots", timeIntervalHelper.getNumberOfTimeSlots());
            st.add("goalStates", query.goalRoles);
            String header = st.render();
            /* Timing */timing.stopTimer(tPrefix + "_" + "template");

            // Potentially write the converted string out to "file + getFileExtenstion()"
            if (writeToFile) {
                /* Timing */timing.startTimer(tPrefix + "_" + "FileOutputStream");
                /*
                File convertedFile = new File(file.getAbsolutePath() + getFileExtenstion());
                FileOutputStream out = new FileOutputStream(convertedFile);
                
                out.write(header.getBytes());
                // Convert Rules into Reduced ASAPTime NSA Rules
                for (Rule r : workableRules) {
                    //newRules.add(new TRole_Rule(r, roleHelper, timeIntervalHelper));
                    out.write(new TRole_Rule(r, roleHelper, timeIntervalHelper).getString().getBytes());
                }
                out.flush();
                out.close();
                */

                Charset charset = Charset.forName("US-ASCII");
                try (BufferedWriter writer = Files
                        .newBufferedWriter(Paths.get(file.getAbsolutePath() + getFileExtenstion()), charset)) {
                    writer.write(header);
                    // Convert Rules into Reduced ASAPTime NSA Rules
                    for (Rule r : workableRules) {
                        // newRules.add(new TRole_Rule(r, roleHelper, timeIntervalHelper));
                        writer.write(new TRole_Rule(r, roleHelper, timeIntervalHelper).getString());
                    }
                } catch (IOException x) {
                    System.err.format("IOException: %s%n", x);
                }

                convertedStr = "Outputed string to the file: " + file.getAbsolutePath() + getFileExtenstion();
                /* Timing */timing.stopTimer(tPrefix + "_" + "FileOutputStream");
            } else {
                /* Timing */timing.startTimer(tPrefix + "_" + "StringBuilder");
                StringBuilder sb = new StringBuilder(header);

                // Convert Rules into Reduced ASAPTime NSA Rules
                for (Rule r : workableRules) {
                    // newRules.add(new TRole_Rule(r, roleHelper, timeIntervalHelper));
                    sb.append(new TRole_Rule(r, roleHelper, timeIntervalHelper).getString());
                }
                convertedStr = sb.toString();
                /* Timing */timing.stopTimer(tPrefix + "_" + "StringBuilder");
            }

            lastError = null;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.warning(errors.toString());

            logger.warning("[ERROR] Unable to convert to TRole: " + e.getMessage());
            lastError = "Error ConvertToRanise.convert.Exception #1";
        }

        return convertedStr;
    }

    /** This function creates a new goal role and create a Can Assign rule to assign this new goal role to anyone who
     * satisfies the original Query's preconditions C and timeslot l. The query returned stored the Mohawk-T version of
     * the Goal Role and thus after the Role Helper converts the Roles into Integer references this query must call it's
     * finalize() method to convert the goal role into it's integer form.
     * 
     * From the paper, this is the reduction (1) Query, Type 2
     * 
     * WARNING: if this function has to create a new role then it will call roleHelper.addUniqueRole();
     * 
     * @param query
     * @param rules
     * @param roleHelper
     * @param timeIntervalHelper
     * @return */
    public TRole_Query toTRole_Query(Query query, ArrayList<Rule> rules, RoleHelper roleHelper,
            TimeIntervalHelper timeIntervalHelper) {
        // Checking Conditions
        if (query._roles.size() == 0) {
            logger.severe("The query must have atleast one Goal Role: " + query);
            throw new IllegalArgumentException("The query must have atleast one Goal Role");
        }

        Role newGoalRole = roleHelper.addUniqueRole("goalRole");
        TRole_Query q = new TRole_Query(newGoalRole);
        Rule newRule = new Rule();
        newRule._type = RuleType.ASSIGN;
        newRule._adminRole = Role.allRoles();
        newRule._adminTimeInterval = timeIntervalHelper.getAllTimeInterval();
        newRule._preconditions.addAll(query._roles);
        newRule._roleSchedule = new ArrayList<TimeSlot>(Arrays.asList(query._timeslot));
        newRule._role = newGoalRole;

        rules.add(newRule);

        return q;
    }

    @Override
    public String getFileExtenstion() {
        return fileExt.TRole;
    }
}
