package mohawk.global.testing;

import static org.junit.Assert.assertEquals;

import java.util.*;

import org.junit.Test;

import mohawk.global.pieces.Role;

public class RoleTests {

    @Test
    public void testHashSet() {
        Set<Role> roles = new HashSet<>();

        roles.add(new Role("abc", "", true));
        assertEquals(1, roles.size());
        roles.add(new Role("abc", "", false));
        assertEquals(1, roles.size());
        roles.add(new Role("abc", "2", false));
        assertEquals(2, roles.size());
    }

    @Test
    public void testTreeSet() {
        Set<Role> roles = new TreeSet<>();

        roles.add(new Role("abc", "", true));
        assertEquals(1, roles.size());
        roles.add(new Role("abc", "", false));
        assertEquals(1, roles.size());
        roles.add(new Role("abc", "2", false));
        assertEquals(2, roles.size());
    }

}
