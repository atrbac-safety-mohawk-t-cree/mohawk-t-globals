package mohawk.global.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import mohawk.global.pieces.*;

public class PiecesTests {
    public static Role adminRole = new Role("adminRole");
    public static Role role1 = new Role("role1");
    public static Role role1_dup = new Role("role1");
    public static Role role2 = new Role("role2");
    public static Role role3 = new Role("role3");

    public static ArrayList<Role> truePre = new ArrayList<Role>();
    public static ArrayList<Role> role1Pre = new ArrayList<Role>();

    public static ArrayList<TimeSlot> schedule1 = new ArrayList<TimeSlot>();

    public static TimeInterval ti1 = new TimeInterval(1, 3);
    public static TimeInterval ti1_dup = new TimeInterval(1, 3);
    public static TimeInterval ti2 = new TimeInterval(2, 2);
    public static TimeInterval ti3 = new TimeInterval(2, 4);

    public static TimeSlot ts1 = new TimeSlot(1);
    public static TimeSlot ts1_dup = new TimeSlot(1);
    public static TimeSlot ts2 = new TimeSlot(2);

    public static Rule rule1 = null;
    public static Rule rule1_dup = null;
    public static Rule rule2 = null;
    public static Rule rule3 = null;
    public static Rule rule4 = null;

    @BeforeClass
    public static void setup() {
        // Preconditions
        role1Pre.add(role1);

        // Role Schedules
        schedule1.add(ts1);

        // Rules
        rule1 = new Rule(RuleType.ASSIGN, adminRole, ti1, truePre, schedule1, role1);
        rule1_dup = new Rule(RuleType.ASSIGN, adminRole, ti1, truePre, schedule1, role1_dup);
        rule2 = new Rule(RuleType.ASSIGN, adminRole, ti1, truePre, schedule1, role2);
        rule3 = new Rule(RuleType.ASSIGN, adminRole, ti1, role1Pre, schedule1, role2);
        rule4 = new Rule(RuleType.ASSIGN, adminRole, ti1, role1Pre, schedule1, role3);
    }

    @Test
    public void RuleEqualsTest() {
        // TRUE
        assertEquals(rule1, new Rule(rule1));
        assertEquals(rule1, rule1_dup);

        // FALSE
        assertNotEquals(rule2, rule1);
    }

    @Test
    public void RuleHashCodeTest() {
        // TRUE
        assertEquals(rule1.hashCode(), new Rule(rule1).hashCode());
        assertEquals(rule1.hashCode(), rule1_dup.hashCode());

        // FALSE
        assertNotEquals(rule2.hashCode(), rule1.hashCode());
    }

    @Test
    public void RoleEqualsTest() {
        // TRUE
        assertEquals(role1, new Role(role1));
        assertEquals(role1, role1_dup);

        // FALSE
        assertNotEquals(role2, role1);
    }

    @Test
    public void RoleHashCodeTest() {
        // TRUE
        assertEquals(role1.hashCode(), new Role(role1).hashCode());
        assertEquals(role1.hashCode(), role1_dup.hashCode());

        // FALSE
        assertNotEquals(role2.hashCode(), role1.hashCode());
    }

    @Test
    public void TimeIntervalEqualsTest() {
        // TRUE
        assertEquals(ti1, new TimeInterval(ti1));
        assertEquals(ti1, ti1_dup);
        assertEquals(ti2, ts2);

        // FALSE
        assertNotEquals(ti2, ti1);
    }

    @Test
    public void TimeIntervalHashCodeTest() {
        // TRUE
        assertEquals(ti1.hashCode(), new TimeInterval(ti1).hashCode());
        assertEquals(ti1.hashCode(), ti1_dup.hashCode());
        assertEquals(ti2.hashCode(), ts2.hashCode());

        // FALSE
        assertNotEquals(ti2.hashCode(), ti1.hashCode());
    }

    @Test
    public void testCanBlock_addRule() {
        CanAssign ca = new CanAssign();
        ca.addRule(rule1);
        ca.addRule(rule1_dup);
        ca.addRule(rule2);
        ca.addRule(rule3);
        ca.addRule(rule4);

        HashMap<Role, ArrayList<Rule>> caMap = ca.getTargetRoleToRules();

        assertEquals(3, caMap.size());
        assertEquals(2, caMap.get(role1).size());
        assertEquals(2, caMap.get(role2).size());
        assertEquals(1, caMap.get(role3).size());

        // System.out.println(caMap);
    }
}
