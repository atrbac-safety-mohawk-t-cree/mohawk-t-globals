package mohawk.global.testing;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.TreeSet;

import org.junit.Test;

import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;

public class TimeIntervalHelperRegressionTests {
    @Test
    public void overlappingTimeIntervals() {
        TimeIntervalHelper helper = simpleOverlapHelper();

        System.out.println(helper.toString());

        assertEquals("Wrong number of time intervals", (Integer) 2, helper.size());
    }

    @Test
    public void timeSlotsAndTimeIntervals() {
        TimeIntervalHelper helper = complexOverlapHelper();

        System.out.println(helper.toString());

        assertEquals("Wrong number of time intervals", (Integer) 4, helper.size());
    }

    @Test
    public void compareTimeSlotToInterval() {
        TimeIntervalHelper helper = new TimeIntervalHelper();

        helper.add(new TimeInterval(1, 3));
        helper.add(new TimeInterval(2, 5));
        helper.add(new TimeInterval(3));
        helper.add(new TimeSlot(3)); // should not be added as there exists a TimeInterval that is equivalent

        System.out.println(helper.toString());

        assertEquals("Wrong number of time intervals", (Integer) 3, helper.size());
    }

    @Test
    public void incasedTimeInterval() {
        TimeIntervalHelper helper = new TimeIntervalHelper();

        TimeInterval t1 = new TimeInterval(1, 6);
        TimeInterval t2 = new TimeInterval(1, 3);

        helper.add(t1);
        helper.add(new TimeInterval(2, 5));
        helper.add(t2);

        System.out.println("Comparision t1.t2: " + t1.compareTo(t2));
        System.out.println("Comparision t2.t1: " + t2.compareTo(t1));
        System.out.println(helper.toString());

        assertFalse("Incorrectly labeling two intervals as equal", t1.equals(t2));
        assertFalse("Incorrectly labeling two intervals as equal", t2.equals(t1));
        assertEquals("Wrong number of time intervals", (Integer) 3, helper.size());
    }

    @Test
    public void timeIntervalSplitting() {
        System.out.println("TimeIntervalSplit Tests:");
        TimeIntervalSplit tis;
        Integer A, B, X, Y; // Temporary parameters for testing
        Integer L1, L2, M1, M2, H1, H2; // correct results for the testing parameters

        System.out.print("Case 1: A < X < B < Y ... ");
        A = 1;
        B = 5;
        X = 3;
        Y = 7;
        L1 = 1;
        L2 = 2;
        M1 = 3;
        M2 = 5;
        H1 = 6;
        H2 = 7;
        tis = splitInterval(A, B, X, Y);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertEquals("Middle start time not equal", M1, tis.middle.getStartTime());
        assertEquals("Middle finish time not equal", M2, tis.middle.getFinishTime());
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        // Test by changing the order
        System.out.print("Case 1.a: X < A < Y < B ... ");
        tis = splitInterval(X, Y, A, B);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertEquals("Middle start time not equal", M1, tis.middle.getStartTime());
        assertEquals("Middle finish time not equal", M2, tis.middle.getFinishTime());
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 2: A < B = X < Y ... ");
        A = 1;
        B = 5;
        X = 5;
        Y = 7;
        L1 = 1;
        L2 = 4;
        M1 = 5;
        M2 = 5;
        H1 = 6;
        H2 = 7;
        tis = splitInterval(A, B, X, Y);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertEquals("Middle start time not equal", M1, tis.middle.getStartTime());
        assertEquals("Middle finish time not equal", M2, tis.middle.getFinishTime());
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 3: A < X < Y < B ... ");
        A = 1;
        B = 7;
        X = 2;
        Y = 5;
        L1 = 1;
        L2 = 1;
        M1 = 2;
        M2 = 5;
        H1 = 6;
        H2 = 7;
        tis = splitInterval(A, B, X, Y);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertEquals("Middle start time not equal", M1, tis.middle.getStartTime());
        assertEquals("Middle finish time not equal", M2, tis.middle.getFinishTime());
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 4: A < X = Y < B ... ");
        A = 1;
        B = 7;
        X = 2;
        Y = 2;
        L1 = 1;
        L2 = 1;
        M1 = 2;
        M2 = 2;
        H1 = 3;
        H2 = 7;
        tis = splitInterval(A, B, X, Y);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertEquals("Middle start time not equal", M1, tis.middle.getStartTime());
        assertEquals("Middle finish time not equal", M2, tis.middle.getFinishTime());
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 4: A < Z < B ... ");
        A = 1;
        B = 7;
        X = 2;
        Y = 2;
        L1 = 1;
        L2 = 1;
        M1 = 2;
        M2 = 2;
        H1 = 3;
        H2 = 7;
        tis = splitInterval(A, B, X);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertEquals("Middle start time not equal", M1, tis.middle.getStartTime());
        assertEquals("Middle finish time not equal", M2, tis.middle.getFinishTime());
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 4.a: A < Z < B ... ");
        A = 1;
        B = 7;
        X = 2;
        Y = 2;
        L1 = 1;
        L2 = 1;
        M1 = 2;
        M2 = 2;
        H1 = 3;
        H2 = 7;
        tis = splitIntervalR(A, B, X);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertEquals("Middle start time not equal", M1, tis.middle.getStartTime());
        assertEquals("Middle finish time not equal", M2, tis.middle.getFinishTime());
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 5: A = X < Y < B ... ");
        A = 1;
        B = 7;
        X = 1;
        Y = 2;
        L1 = 1;
        L2 = 2;
        M1 = -1;
        M2 = -1;
        H1 = 3;
        H2 = 7;
        tis = splitInterval(A, B, X, Y);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertNull("Middle is not null", tis.middle);
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 6: A = Z < B ... ");
        A = 1;
        B = 7;
        X = 1;
        Y = 1;
        L1 = 1;
        L2 = 1;
        M1 = -1;
        M2 = -1;
        H1 = 2;
        H2 = 7;
        tis = splitInterval(A, B, X);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertNull("Middle is not null", tis.middle);
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 7: A < X < Y = B ... ");
        A = 1;
        B = 7;
        X = 5;
        Y = 7;
        L1 = 1;
        L2 = 4;
        M1 = -1;
        M2 = -1;
        H1 = 5;
        H2 = 7;
        tis = splitInterval(A, B, X, Y);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertNull("Middle is not null", tis.middle);
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 8: A < Z = B ... ");
        A = 1;
        B = 7;
        X = 7;
        Y = 7;
        L1 = 1;
        L2 = 6;
        M1 = -1;
        M2 = -1;
        H1 = 7;
        H2 = 7;
        tis = splitInterval(A, B, X);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertNull("Middle is not null", tis.middle);
        assertEquals("Higher start time not equal", H1, tis.higher.getStartTime());
        assertEquals("Higher finish time not equal", H2, tis.higher.getFinishTime());
        System.out.println("Passed");

        System.out.print("Case 9: A = X < B = Y ... ");
        A = 1;
        B = 7;
        X = 1;
        Y = 7;
        L1 = 1;
        L2 = 7;
        M1 = -1;
        M2 = -1;
        H1 = -1;
        H2 = -1;
        tis = splitInterval(A, B, X, Y);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertNull("Middle is not null", tis.middle);
        assertNull("Higher is not null", tis.higher);
        System.out.println("Passed");

        System.out.print("Case 10: A = B = Z ... ");
        A = 3;
        B = 3;
        X = 3;
        Y = 3;
        L1 = 3;
        L2 = 3;
        M1 = -1;
        M2 = -1;
        H1 = -1;
        H2 = 1;
        tis = splitInterval(A, B, X);
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertNull("Middle is not null", tis.middle);
        assertNull("Higher is not null", tis.higher);
        System.out.println("Passed");

        System.out.print("Case 10.a: Z1 = Z2 ... ");
        A = 3;
        B = 3;
        X = 3;
        Y = 3;
        L1 = 3;
        L2 = 3;
        M1 = -1;
        M2 = -1;
        H1 = -1;
        H2 = 1;
        tis = new TimeIntervalSplit(new TimeSlot(A), new TimeSlot(X));
        assertEquals("Lower start time not equal", L1, tis.lower.getStartTime());
        assertEquals("Lower finish time not equal", L2, tis.lower.getFinishTime());
        assertNull("Middle is not null", tis.middle);
        assertNull("Higher is not null", tis.higher);
        System.out.println("Passed");
    }

    @Test
    public void testReduceToTimeslots() {
        System.out.println("Testing simpleOverlapHelper:");
        TimeIntervalHelper helper = simpleOverlapHelper(); // t1-t3, t2-t5

        helper.allowZeroTimeslot = false;
        System.out.println("Normal: " + helper.toString());
        helper.reduceToTimeslots();
        System.out.println("Reduced: " + helper._reducedTimeIntervals.toString());

        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(1)),
                helper.indexOfReduced(new TimeInterval(1, 1)));
        assertEquals("Incorrect spanning TimeInterval", new TreeSet<Integer>(Arrays.asList(1, 2)),
                helper.indexOfReduced(new TimeInterval(1, 2)));
        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(2)),
                helper.indexOfReduced(new TimeInterval(2, 3)));
        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(3)),
                helper.indexOfReduced(new TimeInterval(4, 5)));

        System.out.println("Testing complexOverlapHelper:");
        helper = complexOverlapHelper(); // t1-t3, t2-t5, t3-t3, t4-t4

        helper.allowZeroTimeslot = false;
        System.out.println("Normal: " + helper.toString());
        helper.reduceToTimeslots();
        System.out.println("Reduced: " + helper._reducedTimeIntervals.toString());

        System.out.println("Overlap: " + helper.indexOfReduced(new TimeInterval(2, 5)));

        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(1, 2, 3)),
                helper.indexOfReduced(new TimeInterval(1, 3)));
        assertEquals("Incorrect spanning TimeInterval", new TreeSet<Integer>(Arrays.asList(2, 3, 4, 5)),
                helper.indexOfReduced(new TimeInterval(2, 5)));
        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(3)),
                helper.indexOfReduced(new TimeInterval(3, 3)));
        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(4)),
                helper.indexOfReduced(new TimeInterval(4, 4)));

        System.out.println("Testing simpleOverlapHelper2:");
        helper = simpleOverlapHelper2(); // t1-t1, t2-t2

        helper.allowZeroTimeslot = false;
        System.out.println("Normal: " + helper.toString());
        helper.reduceToTimeslots();
        System.out.println("Reduced: " + helper._reducedTimeIntervals.toString());

        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(1)),
                helper.indexOfReduced(new TimeInterval(1, 1)));
        assertEquals("Incorrect spanning TimeInterval", new TreeSet<Integer>(Arrays.asList(1, 2)),
                helper.indexOfReduced(new TimeInterval(1, 2)));
        assertEquals("Could not return the correct index", new TreeSet<Integer>(Arrays.asList(2)),
                helper.indexOfReduced(new TimeInterval(2, 2)));
    }

    private TimeIntervalHelper simpleOverlapHelper() {
        TimeIntervalHelper helper = new TimeIntervalHelper();

        helper.add(new TimeInterval(1, 3));
        helper.add(new TimeInterval(2, 5));

        return helper;
    }

    private TimeIntervalHelper simpleOverlapHelper2() {
        TimeIntervalHelper helper = new TimeIntervalHelper();

        helper.add(new TimeInterval(1));
        helper.add(new TimeInterval(2));

        return helper;
    }

    private TimeIntervalHelper complexOverlapHelper() {
        TimeIntervalHelper helper = new TimeIntervalHelper();

        helper.add(new TimeInterval(1, 3));
        helper.add(new TimeInterval(2, 5));
        helper.add(new TimeInterval(3));
        helper.add(new TimeSlot(4));
        return helper;
    }

    private TimeIntervalHelper complexOverlapHelper2() {
        TimeIntervalHelper helper = new TimeIntervalHelper();

        helper.add(new TimeInterval(0, 3));
        helper.add(new TimeInterval(1, 4));
        helper.add(new TimeInterval(5, 8));
        helper.add(new TimeInterval(6, 6));
        helper.add(new TimeInterval(8, 10));
        return helper;
    }

    @Test
    public void complexReducedOverlap() {
        System.out.println("Testing complexOverlapHelper:");
        TimeIntervalHelper helper = complexOverlapHelper2(); // t1-t3, t2-t5, t3-t3, t4-t4

        helper.allowZeroTimeslot = false;
        System.out.println("Normal: " + helper.toString());
        helper.reduceToTimeslots();
        System.out.println("Reduced: " + helper._reducedTimeIntervals.toString());

        System.out.println("Overlap: " + helper.indexOfReduced(new TimeInterval(2, 5)));
    }

    private TimeIntervalSplit splitInterval(Integer A, Integer B, Integer X) {
        TimeInterval AB = new TimeInterval(A, B);
        TimeSlot Z = new TimeSlot(X);

        return new TimeIntervalSplit(AB, Z);
    }

    private TimeIntervalSplit splitIntervalR(Integer A, Integer B, Integer X) {
        TimeInterval AB = new TimeInterval(A, B);
        TimeSlot Z = new TimeSlot(X);

        return new TimeIntervalSplit(Z, AB);
    }

    private TimeIntervalSplit splitInterval(Integer A, Integer B, Integer X, Integer Y) {
        TimeInterval AB = new TimeInterval(A, B);
        TimeInterval XY = new TimeInterval(X, Y);

        return new TimeIntervalSplit(AB, XY);
    }
}
