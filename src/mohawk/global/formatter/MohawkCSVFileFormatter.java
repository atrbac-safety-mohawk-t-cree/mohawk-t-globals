package mohawk.global.formatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;

public class MohawkCSVFileFormatter extends Formatter {
    public final static Logger logger = Logger.getLogger("mohawk");
    public static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss.SSSZ");

    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);
        builder.append(getDateTime(record.getMillis())).append(",");
        builder.append(record.getSourceClassName()).append(",");
        builder.append(record.getSourceMethodName()).append(",");
        builder.append(record.getLevel()).append(",");
        builder.append(StringEscapeUtils.escapeCsv(formatMessage(record)));
        builder.append("\n");
        return builder.toString();
    }

    public static String csvHeaders() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("Datetime").append(",");
        builder.append("Classname").append(",");
        builder.append("Method").append(",");
        builder.append("Level").append(",");
        builder.append("Message");
        builder.append("\n");
        return builder.toString();
    }

    public String getHead(Handler h) {
        return super.getHead(h);
    }

    public String getTail(Handler h) {
        return super.getTail(h);
    }

    public static String getDateTime(long millis) {
        return df.format(new Date(millis));
    }
}
