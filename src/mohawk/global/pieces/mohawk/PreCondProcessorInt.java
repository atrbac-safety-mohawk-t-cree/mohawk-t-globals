/**
 * 			RBAC2SMV - Tool for converting a RBAC specification
 *                     to NuSMV specification
 *
 *			@author Karthick Jayaraman
 */
package mohawk.global.pieces.mohawk;

import java.util.Map;

/** @author kjayaram */
public class PreCondProcessorInt {

    PreCondition pcPreCond;
    // Map<Integer,String> mRoleIndex;
    Map<String, Integer> mRole2Index;

    public PreCondProcessorInt(/* Map<Integer,String> inRoleIndex, */Map<String, Integer> inRole2Index) {
        // mRoleIndex = inRoleIndex;
        mRole2Index = inRole2Index;
        pcPreCond = new PreCondition();
    }

    /* NEVER USED
    private int getMapKey(Map<Integer, String> inMap, String inString) {
    
        for (int i = 0; i < inMap.size(); i++) {
    
            if (inMap.get(i).equals(inString)) { return i; }
        }
    
        System.out.println("Error - PreCondProcessorInt::getMapIndex - Value not found in map");
        return 0;
    }
    */

    // Add a positive pre condition for a role.
    public void add(String strRole) throws Exception {

        int iRoleIndex = 0; // mRole2Index.get(strRole); //getMapKey(mRoleIndex,strRole);
        // if(iRoleIndex == null)
        // System.out.println(strRole);

        if (mRole2Index.get(strRole) == null) {
            throw new Exception("Precondition is trying to add a role " + strRole + " that does not exist");
        } else iRoleIndex = mRole2Index.get(strRole);

        pcPreCond.addConditional(iRoleIndex, 1);
    }

    // Add a negative precondition for a role
    public void addNeg(String strRole) {

        int iRoleIndex = mRole2Index.get(strRole); // getMapKey(mRoleIndex,strRole);
        pcPreCond.addConditional(iRoleIndex, 2);
    }

    public PreCondition result() throws Exception {
        return pcPreCond;
    }
}
