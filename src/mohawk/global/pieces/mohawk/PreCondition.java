/**
 * The MIT License
 * 
 * Copyright (c) 2010 Karthick Jayaraman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

package mohawk.global.pieces.mohawk;

import java.util.*;

/** @author Karthick Jayaraman */
public class PreCondition {
    private Map<Integer, Integer> mBitVector;

    public PreCondition() {
        mBitVector = new HashMap<Integer, Integer>();
    }

    /*
     * addConditional sets the specified role index to either 1 or 2.
     * 1 - refers to positive condition 2 - refers to negative condition.
     */
    public void addConditional(int roleindex, int value) {
        mBitVector.put(roleindex, value);
    }

    public void delConditional(int roleindex) {
        mBitVector.remove(roleindex);
    }

    public int getConditional(int roleindex) {
        return mBitVector.get(roleindex);
    }

    public int size() {
        return mBitVector.size();
    }

    public Set<Integer> keySet() {
        return mBitVector.keySet();
    }

    public Vector<Integer> valueSet() {
        return new Vector<Integer>(mBitVector.values());
    }

    @Override
    public String toString() {
        if (size() == 0) { return "TRUE"; }
        boolean first = true;
        StringBuilder str = new StringBuilder();

        for (Integer i : keySet()) {
            if (first == false) {
                str.append(" & ");
            }
            first = false;

            if (mBitVector.get(i) == 2) {
                str.append("-");
            }

            str.append(i);
        }

        return str.toString();
    }
}
