package mohawk.global.pieces;

import java.util.*;

import mohawk.global.comparator.RuleComparator;

public class CanBlock {
    public static final boolean USE_ROLE_TO_RULES_MAP = true;

    protected RuleType _type;
    protected ArrayList<Rule> _rules = new ArrayList<Rule>();
    protected HashMap<Role, ArrayList<Rule>> _roleToRules = new HashMap<>();
    protected Integer largestPrecondtion = 0;
    protected Integer largestRoleSchedule = 0;
    protected Integer startableRules = 0;
    protected Integer trulyStartableRules = 0;

    /** 
     * Returns the number of rules
     * @return
     */
    public int size() {
        return _rules.size();
    }

    /**
     * Returns the number of rules
     * @return
     */
    public int numberOfRules() {
        return _rules.size();
    }

    public ArrayList<Rule> getRules() {
        return _rules;
    }

    public Rule[] getRulesArray() {
        return _rules.toArray(new Rule[size()]);
    }

    /**
     * Returns the mutable Map object for the Target Roles to Rules.
     * @return Map where given a role, the map gives all of the rules where that rule is a target role. 
     *          Can return <b>NULL</b> if {@link CanBlock#USE_ROLE_TO_RULES_MAP} == FALSE
     */
    public HashMap<Role, ArrayList<Rule>> getTargetRoleToRules() {
        if (!USE_ROLE_TO_RULES_MAP) { return null; }
        return _roleToRules;
    }

    public HashMap<Role, HashSet<TimeSlot>> getTargetRoleToTimeslots() {
        HashMap<Role, HashSet<TimeSlot>> tm = new HashMap<>();

        HashSet<TimeSlot> t;
        for (Rule r : _rules) {
            if (!tm.containsKey(r._role)) {
                tm.put(r._role, new HashSet<TimeSlot>());
            }
            t = tm.get(r._role);

            for (TimeSlot ts : r._roleSchedule) {
                t.add(ts);
            }
        }

        return tm;
    }

    public void addRule(Rule r) {
        r.update();
        _rules.add(r);

        if (USE_ROLE_TO_RULES_MAP) {
            if (!_roleToRules.containsKey(r._role)) {
                _roleToRules.put(r._role, new ArrayList<Rule>());
            }
            _roleToRules.get(r._role).add(r);
        }

        largestPrecondtion = Math.max(largestPrecondtion, r._preconditions.size());
        largestRoleSchedule = Math.max(largestRoleSchedule, r._roleSchedule.size());

        if (r.isRuleStartable()) {
            startableRules++;
        }
        if (r.isRuleTrulyStartable()) {
            trulyStartableRules++;
        }
    }

    /** Indexes all rules in this CanBlock */
    public void indexRules() {
        Integer index = 1;

        for (Rule r : _rules) {
            r.ruleIndex = index;
            index++;
        }
    }

    /** Removes all of the rules from the CanBlock */
    public void clearRules() {
        _rules.clear();
    }

    /** Returns the MohawkT representation of this class
     * 
     * @return */
    public String getString(Boolean verboseComments) {
        if (_rules.size() == 0) { return _type + " { }"; }

        StringBuilder s = new StringBuilder();

        if (verboseComments) {
            s.append("/* \n").append(" * Number of Rules       : " + size() + "\n")
                    .append(" * Largest Precondition  : " + largestPrecondtion + "\n")
                    .append(" * Largest Role Schedule : " + largestRoleSchedule + "\n")
                    .append(" * Startable Rules       : " + startableRules + "\n")
                    .append(" * Truly Startable Rules : " + trulyStartableRules + "\n").append(" */\n");

        }
        s.append(_type + " {\n");

        Integer i = 0;
        Integer padding = (int) Math.log10(_rules.size()) + 1;

        // Sort the rules by their information Code
        _rules.sort(new RuleComparator());
        for (Rule r : _rules) {
            i++;
            s.append("    " + r.getString(i, padding) + "\n");
        }

        s.append("}");

        return s.toString();
    }

    @Override
    public String toString() {
        return _type + " Block (" + _rules.size() + ")";
    }
}
