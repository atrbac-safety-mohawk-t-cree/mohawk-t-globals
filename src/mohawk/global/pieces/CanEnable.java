package mohawk.global.pieces;

public class CanEnable extends CanBlock {
    public CanEnable() {
        super();
        _type = RuleType.ENABLE;
    }
}
