package mohawk.global.pieces;

public class TimeSlot extends TimeInterval {

    public TimeSlot(Integer startFinish) {
        super(startFinish);
    }

    /** Deep Copy Constructor
     * 
     * @param ts */
    public TimeSlot(TimeSlot ts) {
        super((TimeInterval) ts);
    }

    @Override
    public String toString() {
        return "t" + _start;
    }
}
