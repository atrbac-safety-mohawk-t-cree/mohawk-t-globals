package mohawk.global.pieces.reduced;

import java.util.ArrayList;

import mohawk.global.pieces.reduced.query.ASAPTimeNSA_Query;
import mohawk.global.pieces.reduced.query.ASAPTimeSA_Query;
import mohawk.global.pieces.reduced.rules.ASAPTimeNSA_Rule;

public class ASAPTimeNSA {
    public String generatorName = ""; // to be filled in with the generator
    public String comment = ""; // to be filled in with the generator

    public Integer numberOfRoles;
    public Integer numberOfTimeslots;
    public ASAPTimeNSA_Query query = new ASAPTimeSA_Query();
    public ArrayList<ASAPTimeNSA_Rule> rules = new ArrayList<ASAPTimeNSA_Rule>();

    public ASAPTimeNSA(String generatorName) {
        this.generatorName = generatorName;
    }

    public boolean addRule(ASAPTimeNSA_Rule newRule) {
        rules.add(newRule);
        return true;
    }

    public String getString() {
        StringBuilder s = new StringBuilder();

        s.append("CONFIG " + numberOfRoles + " " + numberOfTimeslots + "\n");
        s.append("GOAL " + query.goalRole + " " + query.goalTimeslot + "\n");

        for (ASAPTimeNSA_Rule r : rules) {
            s.append(r.getString()).append("\n");
        }
        return s.toString();
    }

    @Override
    public String toString() {
        return getString();
    }
}
