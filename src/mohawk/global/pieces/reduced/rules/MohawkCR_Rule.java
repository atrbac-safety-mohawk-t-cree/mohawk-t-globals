package mohawk.global.pieces.reduced.rules;

import java.util.logging.Logger;

import mohawk.global.helper.RoleHelper;
import mohawk.global.pieces.Rule;
import mohawk.global.pieces.TimeInterval;

public class MohawkCR_Rule {
    public static final Logger logger = Logger.getLogger("mohawk");
    public String role;

    public MohawkCR_Rule() {}

    public MohawkCR_Rule(Rule r) throws Exception {
        if (r._preconditions.size() != 0) {
            logger.severe("Cannot convert to a Mohawk CR rule because it has predoncitions: " + r);
            throw new Exception("Mohawk CR Rules must have NO preconditions!");
        }
        this.role = r._role.getName();
    }

    public MohawkCR_Rule(Rule r, String suffix) throws Exception {
        this(r);
        role = role + suffix;
    }

    public MohawkCR_Rule(Rule r, TimeInterval ti) throws Exception {
        this(r, ti.standardRoleName());
    }

    public void shortRolenames(RoleHelper rh) {
        role = "r" + rh.indexOf(role);
    }

}
