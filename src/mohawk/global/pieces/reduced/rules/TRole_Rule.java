package mohawk.global.pieces.reduced.rules;

import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.Role;
import mohawk.global.pieces.Rule;
import mohawk.global.pieces.TimeSlot;

public class TRole_Rule {
    public String ruleType;
    public Integer startTime;
    public Integer finishTime;
    public Integer role;
    public Integer[] precondition;
    public Integer[] roleSchedule;

    public TRole_Rule() {}

    public TRole_Rule(Rule r, RoleHelper rh, TimeIntervalHelper tih) {
        ruleType = r._type.toUzun();
        startTime = r._adminTimeInterval.getStartTime();
        finishTime = r._adminTimeInterval.getFinishTime();
        role = rh.indexOf(r._role);

        precondition = new Integer[rh.numberOfRoles()];
        Integer i = 0;
        int loc = 0;
        for (Role role : rh.getArray()) {
            loc = r._preconditions.indexOf(role.toLimitedRole());
            if (loc != -1) {
                if (r._preconditions.get(loc)._not) {
                    precondition[i] = -1;
                } else {
                    precondition[i] = 1;
                }
            } else {
                precondition[i] = 0;
            }

            i++;
        }

        roleSchedule = new Integer[tih.getNumberOfTimeSlots()];
        for (i = 0; i < roleSchedule.length; i++) {
            if (r._roleSchedule.contains(new TimeSlot(i))) {
                roleSchedule[i] = 1;
            } else {
                roleSchedule[i] = 0;
            }
        }
    }

    /** Returns the string in UZun's input format
     * 
     * @return */
    public String getString() {
        StringBuilder s = new StringBuilder();
        // <r.ruleType> <r.startTime> <r.finishTime> <r.role><\n><\t><r.precondition; separator="
        // "><\n><\t><r.roleSchedule; separator=" "><\n>}>
        s.append(ruleType).append(" ").append(startTime).append(" ").append(finishTime).append(" ").append(role)
                .append("\n\t");
        boolean first = true;
        for (Integer i : precondition) {
            if (first) {
                s.append(i.toString());
                first = false;
            } else {
                s.append(" ").append(i.toString());
            }
        }
        s.append("\n\t");
        first = true;
        for (Integer i : roleSchedule) {
            if (first) {
                s.append(i.toString());
                first = false;
            } else {
                s.append(" ").append(i.toString());
            }
        }
        s.append("\n");

        return s.toString();
    }
}
