package mohawk.global.pieces.reduced.rules;

import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.Role;
import mohawk.global.pieces.Rule;

public class TRule_Rule {
    public String ruleType;
    public Integer startTime;
    public Integer finishTime;
    public Integer role;
    public Integer[] precondition;
    public Integer[] roleSchedule;

    public TRule_Rule(Rule r, RoleHelper rh, TimeIntervalHelper tih) {
        ruleType = r._type.toUzun();
        startTime = r._adminTimeInterval.getStartTime();
        finishTime = r._adminTimeInterval.getFinishTime();
        role = rh.indexOf(r._role);

        precondition = new Integer[rh.numberOfRoles()];
        for (int i = 0; i < rh.numberOfRoles(); i++) {
            Role role = rh.get(i);

            // Ignore
            int loc = r._preconditions.indexOf(role.toLimitedRole());
            if (loc != -1) {
                role = r._preconditions.get(loc);

                if (role._not) {
                    precondition[i] = -1;
                } else {
                    precondition[i] = 1;
                }
            } else {
                precondition[i] = 0;
            }
        }

        roleSchedule = new Integer[tih.getNextTimeSlot()];
        for (Integer i = 0; i < roleSchedule.length; i++) {
            if (r._roleSchedule.contains(i)) {
                roleSchedule[i] = 1;
            } else {
                roleSchedule[i] = 0;
            }
        }
    }
}
