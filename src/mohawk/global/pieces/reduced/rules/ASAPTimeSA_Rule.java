package mohawk.global.pieces.reduced.rules;

import java.util.ArrayList;

public class ASAPTimeSA_Rule {
    public String ruleType;
    public Integer adminTime;
    public ArrayList<Integer> precondition = new ArrayList<Integer>();
    public Integer roleTime;
    public Integer role;

    public String getString() {
        return getString(false);
    }

    public String getString(boolean getSimpleNSA) {
        StringBuilder s = new StringBuilder();
        String sp = " ";
        String t = "t";
        String sep1 = " , ";
        String sep2 = " ; ";

        // can_revoke t3 , 28 -5 -3 -8 ; t5 , 14
        // can_assign t3 , true ; t1 , 2
        s.append(ruleType).append(sp);
        if (getSimpleNSA) {
            s.append("true").append(sep1);
        }
        s.append(t).append(adminTime).append(sep1);

        if (precondition.isEmpty()) {
            s.append("true");
        } else {
            boolean first = true;
            for (Integer i : precondition) {
                if (first) {
                    s.append(i);
                    first = false;
                } else {
                    s.append(sp).append(i);
                }
            }
        }
        s.append(sep2).append(t).append(roleTime).append(sep1).append(role);

        return s.toString();
    }

    @Override
    public String toString() {
        return getString();
    }
}
