package mohawk.global.pieces.reduced.rules;

import java.util.ArrayList;

public class ASAPTimeNSA_Rule {
    public String ruleType;
    public Integer adminRole;
    public Integer adminTime;
    public Integer role;
    public ArrayList<Integer> precondition;
    public Integer roleTime;

    public String getString() {
        StringBuilder s = new StringBuilder();
        String sp = " ";
        String t = "t";
        String sep1 = " , ";
        String sep2 = " ; ";

        // can_revoke r1 , t3 , 28 -5 -3 -8 ; t5 , 14
        // can_assign true , t3 , true ; t1 , 2
        s.append(ruleType).append(sp);
        if (adminRole == null) {
            s.append("true").append(sep1);
        } else {
            s.append(adminRole).append(sep1);
        }
        s.append(t).append(adminTime).append(sep1);

        if (precondition.isEmpty()) {
            s.append("true");
        } else {
            boolean first = true;
            for (Integer i : precondition) {
                if (first) {
                    s.append(i);
                    first = false;
                } else {
                    s.append(sp).append(i);
                }
            }
        }
        s.append(sep2).append(t).append(roleTime).append(sep1).append(role);

        return s.toString();
    }

    @Override
    public String toString() {
        return getString();
    }
}
