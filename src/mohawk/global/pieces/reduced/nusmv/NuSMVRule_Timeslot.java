package mohawk.global.pieces.reduced.nusmv;

import java.util.ArrayList;

import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.Rule;

public class NuSMVRule_Timeslot {
    public Integer timeslot;
    public ArrayList<NuSMVRule_Case> cases = null;

    public NuSMVRule_Timeslot(Integer time) {
        this.timeslot = time;
    }

    public void addRule(MohawkT m, Rule r) {
        if (cases == null) {
            cases = new ArrayList<>();
        }
        cases.add(new NuSMVRule_Case(m, r));
    }
}
