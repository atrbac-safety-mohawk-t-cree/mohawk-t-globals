package mohawk.global.pieces.reduced.nusmv;

import java.util.ArrayList;
import java.util.HashMap;

import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.Rule;

public class NuSMVRule_Role {
    public Integer role;
    public HashMap<Integer, NuSMVRule_Timeslot> _timeslots = new HashMap<>();

    public NuSMVRule_Role(Integer role) {
        this.role = role;
    }

    public void addTimeslot(Integer time) {
        _timeslots.put(time, new NuSMVRule_Timeslot(time));
    }

    public void addRule(MohawkT m, Rule r) {
        for (Integer time : _timeslots.keySet()) {
            if (m.ruleOverlapTimeslot(r, time)) {
                _timeslots.get(time).addRule(m, r);
            }
        }
    }

    public ArrayList<NuSMVRule_Timeslot> getTimeslots() {
        return new ArrayList<>(_timeslots.values());
    }
}
