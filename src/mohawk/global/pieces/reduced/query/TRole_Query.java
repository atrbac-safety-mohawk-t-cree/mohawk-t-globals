package mohawk.global.pieces.reduced.query;

import mohawk.global.helper.RoleHelper;
import mohawk.global.pieces.Role;

/** A rule object that assumes a Separate Administrator (SA). Therefore it does not need an administrator role or an
 * administrative interval
 * 
 * @author Jonathan Shahen */
public class TRole_Query {
    public Integer[] goalRoles;
    public Role goalRole;

    public TRole_Query(Role goalRole) {
        this.goalRole = goalRole;
    }

    public void finalize(RoleHelper helper) {
        goalRoles = new Integer[helper.numberOfRoles()];

        int j = 0;
        for (Role role : helper.getArray()) {
            if (goalRole.equals(role.getName())) {
                goalRoles[j] = 1;
            } else {
                goalRoles[j] = 0;
            }
        }
    }
}
