package mohawk.global.pieces.reduced.query;

import java.util.Set;
import java.util.logging.Logger;

import mohawk.global.helper.RoleHelper;
import mohawk.global.pieces.Role;
import mohawk.global.pieces.TimeSlot;

/** @author Jonathan Shahen */
public class Mohawk_Query {
    public static final Logger logger = Logger.getLogger("mohawk");

    public TimeSlot _timeslot;
    public Role _goalRole;
    public String specRole;

    public Mohawk_Query(Role goalRole, TimeSlot timeSlot) {
        _goalRole = new Role(goalRole);
        _timeslot = new TimeSlot(timeSlot);
    }

    public Boolean finalize(RoleHelper roleHelperTemporality, boolean shortRolenames) {
        System.out.println("Number of Roles: " + roleHelperTemporality.numberOfRoles());
        Role newName = new Role(_goalRole, "_" + _timeslot);

        Set<Role> allRoles = roleHelperTemporality.getSet();

        if (allRoles.contains(newName)) {
            logger.fine("Found the Query Goal Role: " + newName);
            specRole = newName.getName();

            if (shortRolenames) {
                specRole = "r" + roleHelperTemporality.indexOf(specRole);
            }

            return true;
        } else {
            logger.warning("FAILED! Searched for: " + _goalRole.getName() + " with role: " + newName);
            return false;
        }

        /*for (Role r : roleHelperTemporality.getArray()) {
        if (r._name.equals(newName)) {
            specRole = r._name;
            return true;
        }
        }*/
    }
}
