package mohawk.global.pieces;

public class CanRevoke extends CanBlock {
    public CanRevoke() {
        super();
        _type = RuleType.REVOKE;
    }
}
