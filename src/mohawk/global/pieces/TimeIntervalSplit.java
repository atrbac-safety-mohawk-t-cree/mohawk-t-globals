package mohawk.global.pieces;

import java.util.ArrayList;

import mohawk.global.testing.DebugMsg;

public class TimeIntervalSplit {
    public TimeInterval lower = null;
    public TimeInterval middle = null;
    public TimeInterval higher = null;
    private boolean DEBUGGING = false;

    public TimeIntervalSplit(TimeInterval first, TimeInterval second) {
        split(first, second);
    }

    private void split(TimeInterval first, TimeInterval second) {
        if (first == null || second == null) { return; }

        Integer minStart = Math.min(first.getStartTime(), second.getStartTime());
        Integer minFinish = Math.min(first.getFinishTime(), second.getFinishTime());

        Integer maxStart = Math.max(first.getStartTime(), second.getStartTime());
        Integer maxFinish = Math.max(first.getFinishTime(), second.getFinishTime());

        Integer overlapType = first.typeOfOverlap(second);

        DebugMsg.msg(DEBUGGING, "Overlap Type: " + overlapType);

        if (overlapType.equals(0)) {
            // Full/Equality Overlap
            lower = first;
            middle = null;
            higher = null;
        } else if (overlapType.equals(1)) {
            // Shared Overlap
            if (first.getStartTime().equals(second.getStartTime())) {
                // minStart == maxStart; minFinish != maxFinish
                lower = new TimeInterval(minStart, minFinish);
                middle = null;
                higher = new TimeInterval(minFinish + 1, maxFinish);
            } else if (first.getFinishTime().equals(second.getFinishTime())) {
                // minStart != maxStart; minFinish == maxFinish
                lower = new TimeInterval(minStart, maxStart - 1);
                middle = null;
                higher = new TimeInterval(maxStart, maxFinish);
            } else {
                // How Did I Get Here??]
                throw new Error("A fault occurred in the matrix...how did you get here?");
            }
        } else if (overlapType.equals(2)) {
            // Normal Overlap
            lower = new TimeInterval(minStart, maxStart - 1);
            middle = new TimeInterval(maxStart, minFinish);
            higher = new TimeInterval(minFinish + 1, maxFinish);
        } else {
            // No Overlap Found
            middle = null;
            if (first.isBefore(second)) {
                lower = first;
                higher = second;
            } else {
                lower = second;
                higher = first;
            }
        }
    }

    public ArrayList<TimeInterval> getTimeIntervals() {
        ArrayList<TimeInterval> tia = new ArrayList<TimeInterval>();

        if (lower != null) {
            tia.add(lower);
        }

        if (middle != null) {
            tia.add(middle);
        }

        if (higher != null) {
            tia.add(higher);
        }

        return tia;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        s.append("Lower: ");
        if (lower != null) {
            s.append(lower.toString() + ", ");
        } else {
            s.append("NULL, ");
        }

        s.append("Middle: ");
        if (middle != null) {
            s.append(middle.toString() + ", ");
        } else {
            s.append("NULL, ");
        }

        s.append("Higher: ");
        if (higher != null) {
            s.append(higher.toString() + "");
        } else {
            s.append("NULL");
        }

        return s.toString();
    }
}
