package mohawk.global.pieces;

public class CanDisable extends CanBlock {
    public CanDisable() {
        super();
        _type = RuleType.DISABLE;
    }
}
