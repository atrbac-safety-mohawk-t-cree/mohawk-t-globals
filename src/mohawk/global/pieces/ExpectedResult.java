package mohawk.global.pieces;

import mohawk.global.results.ExecutionResult;

public enum ExpectedResult {
    REACHABLE("Query Reachable", "REACHABLE"), //
    UNREACHABLE("Query Unreachable", "UNREACHABLE"), //
    UNKNOWN("Unknown", "UNKNOWN");

    private String _str;
    private String _mohawk;

    ExpectedResult(String str, String mohawk) {
        _str = str;
        _mohawk = mohawk;
    }

    @Override
    public String toString() {
        return "Expected Result: " + _str;
    }

    /** Returns the MohawkT representation of this class
     * 
     * @return */
    public String getString() {
        return "Expected: " + _mohawk;
    }

    public ExecutionResult toExecutionResult() {
        if (this == REACHABLE) { return ExecutionResult.GOAL_REACHABLE; }
        if (this == UNREACHABLE) { return ExecutionResult.GOAL_UNREACHABLE; }
        if (this == ExpectedResult.UNKNOWN) { return ExecutionResult.GOAL_UNKNOWN; }
        return ExecutionResult.ERROR_OCCURRED;
    }

    public Character getChar() {
        switch (this) {
            case REACHABLE :
                return 'R';
            case UNREACHABLE :
                return 'U';
            case UNKNOWN :
                return null;
        }
        return null;
    }
}
