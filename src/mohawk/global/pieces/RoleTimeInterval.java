package mohawk.global.pieces;

public class RoleTimeInterval {
    public Role _role;
    public TimeInterval _timeInterval;

    public RoleTimeInterval(Role role, TimeInterval timeInterval) {
        _role = role;
        _timeInterval = timeInterval;
    }

}
