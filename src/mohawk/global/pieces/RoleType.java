package mohawk.global.pieces;

public enum RoleType {
    ASSIGNED("Assigned"), ENABLED("CanDisable"), COMBINED("Combined");

    private final String _name;

    RoleType(String name) {
        this._name = name;
    }

    @Override
    public String toString() {
        return this._name;
    }
}
