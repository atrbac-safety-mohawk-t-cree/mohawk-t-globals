package mohawk.global.pieces;

import java.util.*;

import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;

public class RoleTimeSlot {
    public Integer _role;
    public Integer _time;

    public RoleTimeSlot(Integer role, Integer time) {
        _role = role;
        _time = time;
    }

    public static Set<RoleTimeSlot> grabAll(ArrayList<Role> roles, TimeSlot ts, TimeIntervalHelper tih, RoleHelper rh) {
        Set<RoleTimeSlot> rt = new HashSet<>();
        Integer index = tih.indexOfReduced(ts).first();
        for (Role r : roles) {
            rt.add(new RoleTimeSlot(rh.indexOf(r), index));
        }

        return rt;
    }

    public static Set<RoleTimeSlot> grabAll(Rule r, TimeIntervalHelper tih, RoleHelper rh) {
        Set<RoleTimeSlot> rt = new HashSet<>();

        Integer index;
        for (TimeSlot ts : r._roleSchedule) {
            index = tih.indexOfReduced(ts).first();
            // Target role
            rt.add(new RoleTimeSlot(rh.indexOf(r._role), index));

            // Precondition roles
            for (Role rr : r._preconditions) {
                rt.add(new RoleTimeSlot(rh.indexOf(rr), index));
            }
        }

        if (!r._adminRole.isAllRoles()) {
            index = rh.indexOf(r._adminRole);
            for (Integer t : tih.indexOfReduced(r._adminTimeInterval)) {
                rt.add(new RoleTimeSlot(index, t));
            }
        }

        return rt;
    }

    public String getString() {
        return "[" + _role + "][" + _time + "]";
    }

    @Override
    public String toString() {
        return "R" + _role + "T" + _time;
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj instanceof RoleTimeSlot)) {
            RoleTimeSlot _obj = (RoleTimeSlot) obj;

            return (this._role.equals(_obj._role) && this._time.equals(_obj._time));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}
