package mohawk.global.pieces;

public class CanAssign extends CanBlock {
    public CanAssign() {
        super();
        _type = RuleType.ASSIGN;
    }
}
