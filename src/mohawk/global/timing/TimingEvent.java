package mohawk.global.timing;

import java.sql.Timestamp;

public class TimingEvent {
    public String category = "Timing";
    public String comment = "";
    public Long startTimeMilliSec = -1L;
    public Long finishTimeMilliSec = -1L;
    public Timestamp startTime;
    public Timestamp finishTime;
    public Boolean failed = false;

    public boolean hasFinished() {
        return finishTimeMilliSec != -1L;
    }

    public Long duration() {
        return finishTimeMilliSec - startTimeMilliSec;
    }

    public Double durationSec() {
        return duration() / 1000.0;
    }

    public Timestamp getCurrentTimestamp() {
        return new Timestamp(getCurrentMilliSec());
    }

    public Long getCurrentMilliSec() {
        return System.currentTimeMillis();
    }

    public void setStartTimeNow() {
        startTime = getCurrentTimestamp();
        startTimeMilliSec = getCurrentMilliSec();
    }

    public void setFinishTimeNow() {
        finishTime = getCurrentTimestamp();
        finishTimeMilliSec = getCurrentMilliSec();
    }

    public void failed() {
        failed = true;
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        t.append(category + ": ");
        if (failed) {
            t.append("[FAILED] ");
        }
        t.append(duration() + " ms");

        if (!comment.isEmpty()) {
            t.append(" [COMMENT] " + comment);
        }

        return t.toString();
    }
}
