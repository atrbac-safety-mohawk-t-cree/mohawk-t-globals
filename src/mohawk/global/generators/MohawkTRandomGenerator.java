package mohawk.global.generators;

import java.util.Random;
import java.util.logging.Logger;

import mohawk.global.pieces.*;

public class MohawkTRandomGenerator {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static final String VERSION = "v1.0";

    public MohawkT lastTestcase;
    public String rolePrefix = "r";
    /** Changes the generation of Preconditions
     * 
     * {equallyRandom, 1/2Empty, 3/5Empty} */
    public String preconditionGen = "equallyRandom";

    /** Returns a pseudo-random number between min and max, inclusive. The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min
     *            Minimum value
     * @param max
     *            Maximum value. Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int) */
    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public MohawkT generate(Integer NUMBER_OF_ROLES, Integer NUMBER_OF_TIME_SLOTS, Integer NUMBER_OF_RULES) {
        Rule newRule;
        Role r; // temporary role variable

        lastTestcase = new MohawkT("RandomGenerator " + VERSION);

        switch (preconditionGen) {
            case "1/2Empty" :
                logger.info("[GENERATOR] Using a probability of 1/2 of getting 0, 1/4 for 1, and 1/4 for -1");
                break;
            case "3/5Empty" :
                logger.info("[GENERATOR] Using a probability of 3/5 of getting 0, 1/5 for 1, and 1/5 for -1");
                break;
            case "equallyRandom" :
            default :
                logger.info("[GENERATOR] Using a probability of 1/3 of getting 0, 1/3 for 1, and 1/3 for -1");
                break;
        }

        // Create random rules
        Integer start, tmp;
        for (Integer i = 0; i < NUMBER_OF_RULES; i++) {
            newRule = new Rule();

            // All roles act as Administrator
            newRule._adminRole = Role.allRoles();

            // Random Administrator Time Interval
            // START TIME
            start = randInt(0, NUMBER_OF_TIME_SLOTS - 1);
            newRule._adminTimeInterval = new TimeInterval(start);
            lastTestcase.timeIntervalHelper.add(newRule._adminTimeInterval);

            // Random Positive and Negative Preconditions
            for (Integer j = 0; j < NUMBER_OF_ROLES; j++) {
                switch (preconditionGen) {
                    case "1/2Empty" :
                        /*
                         * tmp has a probability of 1/2 of getting 0, 1/4 for 1, and 1/4 for -1
                         */
                        tmp = randInt(0, 3) - 1;
                        if (tmp >= 2) {
                            tmp = 0;
                        }
                        break;
                    case "3/5Empty" :
                        /*
                         * tmp has a probability of 3/5 of getting 0, 1/5 for 1, and 1/5 for -1
                         */
                        tmp = randInt(0, 4) - 1;
                        if (tmp >= 2) {
                            tmp = 0;
                        }
                        break;
                    case "equallyRandom" :
                    default :
                        tmp = randInt(0, 2) - 1;
                        break;
                }

                switch (tmp) {
                    case 0 :
                        // Do Nothing
                        break;
                    case 1 :
                        // Positive Condition
                        r = new Role(rolePrefix, j.toString());
                        newRule._preconditions.add(r);
                        lastTestcase.roleHelper.add(r);
                        break;
                    case -1 :
                        // Negative Condition
                        r = new Role(rolePrefix, j.toString(), true);
                        newRule._preconditions.add(r);
                        lastTestcase.roleHelper.add(r);
                        break;
                }
            }

            // Random Role Schedule
            TimeSlot ts;
            for (Integer j = 0; j < NUMBER_OF_TIME_SLOTS; j++) {
                tmp = randInt(0, 1);
                if (tmp == 1) {
                    ts = new TimeSlot(j);
                    newRule._roleSchedule.add(ts);
                    lastTestcase.timeIntervalHelper.add(ts);
                }
            }

            // Random Role Assignment and Revocation
            tmp = randInt(0, NUMBER_OF_ROLES - 1);
            r = new Role(rolePrefix + tmp.toString());
            newRule._role = r;
            lastTestcase.roleHelper.add(r);

            // Random Rule Type (only CanAssign and CanRevoke)
            if (randInt(0, 1) == 0) {
                newRule._type = RuleType.ASSIGN;
            } else {
                newRule._type = RuleType.REVOKE;
            }

            if (lastTestcase.addRule(newRule) == true) {
                logger.fine("[GENERATOR] Successfully added a new rule");
            }
        }

        // All Roles for the Query
        for (Integer i = 0; i < NUMBER_OF_ROLES; i++) {
            lastTestcase.query._roles.add(new Role("role" + i.toString()));
        }

        // query._timeslot = new TimeSlot(0);
        lastTestcase.query._timeslot = new TimeSlot((int) randInt(0, NUMBER_OF_TIME_SLOTS - 1));

        return lastTestcase;
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        t.append(lastTestcase.query + "\n");
        t.append(lastTestcase.canAssign + "\n");
        t.append(lastTestcase.canRevoke + "\n");

        return t.toString();
    }
}
