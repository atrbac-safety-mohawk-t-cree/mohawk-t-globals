package mohawk.global.helper;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.logging.Logger;

public class FileHelper {
    public final static Logger logger = Logger.getLogger("mohawk");
    public String smvFilepath = "latestRBAC2SMV.smv";
    public Boolean smvDeleteFile = false;
    public String specFile = "";
    public String fileExt = ".mohawkT";
    public ArrayList<File> specFiles = new ArrayList<File>();
    public boolean bulk = false;

    public String lastError = null;
    public StringBuilder errorLog = new StringBuilder();

    public void loadSpecFiles() throws IOException {
        if (this.bulk == true) {
            this.loadSpecFilesFromFolder(this.specFile);
        } else {
            this.addSpecFile(this.specFile);
        }
    }

    public static Comparator<File> UzunFilenameComparator = new Comparator<File>() {
        public int compare(File f1, File f2) {
            String filename1 = f1.getName();
            String filename2 = f2.getName();

            if (filename1.startsWith("random_role") && filename2.startsWith("random_role")) {
                String[] nums1 = filename1.split("_");
                String[] nums2 = filename2.split("_");

                if (nums1.length == 5 && nums2.length == 5) {
                    Integer role1 = Integer.parseInt(nums1[1].replace("role", ""));
                    Integer rule1 = Integer.parseInt(nums1[2].replace("rule", ""));
                    Integer time1 = Integer.parseInt(nums1[3].replace("time", ""));
                    Integer count1 = Integer.parseInt(nums1[4].replace(".mohawkT", ""));

                    Integer role2 = Integer.parseInt(nums2[1].replace("role", ""));
                    Integer rule2 = Integer.parseInt(nums2[2].replace("rule", ""));
                    Integer time2 = Integer.parseInt(nums2[3].replace("time", ""));
                    Integer count2 = Integer.parseInt(nums2[4].replace(".mohawkT", ""));

                    int tmp = role1.compareTo(role2);
                    if (tmp != 0) { return tmp; }

                    tmp = rule1.compareTo(rule2);
                    if (tmp != 0) { return tmp; }

                    tmp = time1.compareTo(time2);
                    if (tmp != 0) { return tmp; }

                    tmp = count1.compareTo(count2);
                    return tmp;
                }
            }
            // ascending order
            return filename1.compareTo(filename2);
        }
    };

    public void loadSpecFilesFromFolder(String path) {
        if (path == null || path == "") {
            logError("[ERROR] No SPEC Folder provided");
        }

        File folder = new File(path);

        if (!folder.exists()) {
            logError("[ERROR] Spec Folder: '" + path + "' does not exists!");
        }
        if (!folder.isDirectory()) {
            logError("[ERROR] Spec Folder: '" + path + "' is not a folder and the 'bulk' option is present. "
                    + "Try removing the '-bulk' option if you wish to use "
                    + "a specific file, or change the option to point to a folder");
        }

        for (File f : folder.listFiles()) {
            if (f.getName().endsWith(fileExt)) {
                logger.fine("Adding file to specFiles: " + f.getAbsolutePath());
                specFiles.add(f);
            }
        }

        // Sort the Uzun Files by name
        Collections.sort(specFiles, UzunFilenameComparator);
    }

    private void logError(String msg) {
        lastError = msg;
        errorLog.append(msg);
        logger.severe(msg);
    }

    public void addSpecFile(String path) throws IOException {
        if (path == null || path.equals("")) {
            logError("[ERROR] No SPEC File provided");
        }

        File file2 = new File(path);
        if (!file2.exists()) {
            logError("[ERROR] Spec File: '" + path + "' does not exists!");
        }
        if (!file2.isFile()) {
            logError("[ERROR] Spec File: '" + path + "' is not a file and the 'bulk' option was not set. "
                    + "Try setting the '-bulk' option if you wish to search "
                    + "through the folder, or change the option to point to a specific file");
        }

        logger.fine("[FILE IO] Using SPEC File: " + file2.getCanonicalPath());
        specFiles.add(file2);
    }

    /** Prints the file list with an optional character limit
     * 
     * @param characterLimit
     *            if less than 0 then it will have no limit,
     *            else the resulting string will be less than or equal to the characterLimit
     * @param absolutePath
     *            if TRUE then it will print the absolute path of the file,
     *            else it will print the filename
     * @return */
    public String printFileNames(int characterLimit, boolean absolutePath) {
        StringBuilder s = new StringBuilder();
        String tmp;

        s.append("[");
        for (int i = 0; i < specFiles.size(); i++) {
            if (absolutePath) {
                tmp = specFiles.get(i).getAbsolutePath();
            } else {
                tmp = specFiles.get(i).getName();
            }
            if (s.length() + tmp.length() > characterLimit && characterLimit > 0) {
                break;
            }
            s.append(tmp).append(",");
        }
        s.append("]");
        return s.toString();
    }

    /**
     * Returns the SHA256 Hash for a file
     * @param file the file to be hashed
     * @return a 256 character string (make sure to save space for the NULL zero, 257 characters).
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static String sha256(File file) throws IOException, NoSuchAlgorithmException {
        return hashFile(file, "SHA-256", null);
    }

    public static String sha256(File file, String suffixStr) throws IOException, NoSuchAlgorithmException {
        return hashFile(file, "SHA-256", suffixStr);
    }

    /**
     * Returns the Hash for a file. Can be MD5, SHA-1, SHA-256, SHA-512.
     * @param file the file to be hashed
     * @return a string in hexadecimal that represents the hashed bytes
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static String hashFile(File file, String algorithm, String suffixStr)
            throws IOException, NoSuchAlgorithmException {
        MessageDigest sha256 = MessageDigest.getInstance(algorithm);
        StringBuffer sb = new StringBuffer();
        byte[] data = new byte[1024];
        int read = 0;

        try (FileInputStream fis = new FileInputStream(file)) {
            while ((read = fis.read(data)) != -1) {
                sha256.update(data, 0, read);
            }

            if (suffixStr != null) {
                sha256.update(suffixStr.getBytes());
            }

            byte[] hashBytes = sha256.digest();

            for (int i = 0; i < hashBytes.length; i++) {
                sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
        }

        return sb.toString();
    }
}
