package mohawk.global.helper;

import java.io.File;
import java.io.FilenameFilter;

public class FileExtensions {
    public String Mohawk = ".mohawk";
    public String Mohawk_T = ".mohawkT";
    public String ASASPTime_SA = ".asasptime_sa";
    public String ASASPTime_NSA = ".asasptime_nsa";
    public String TRole = ".trole";
    public String TRule = ".trule";

    public String[] getFileExtensions() {
        return new String[]{Mohawk, Mohawk_T, ASASPTime_SA, ASASPTime_NSA, TRole, TRule};
    }

    public static FilenameFilter getFilter(final String ext) {
        return new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(ext);
            }
        };
    }

    /**
     * Replaces the last occurrence of the oldExtension it finds with newExtension.
     * <br>
     * If the oldExtension is not found then the original filename is returned.
     * 
     * @param filename
     * @param oldExtension
     * @param newExtension
     * @return
     */
    public static String swapExtension(String filename, String oldExtension, String newExtension) {
        int lastIndex = filename.lastIndexOf(oldExtension);
        if (lastIndex < 0) return filename;
        String tail = filename.substring(lastIndex).replaceFirst(oldExtension, newExtension);
        return filename.substring(0, lastIndex) + tail;
    }
    /**
     * Finds the last occurrence of '.' and replaces everything after it (including the period) with newExtension.
     * <br>
     * If '.' is not found, then newExtension will be added to the filename.
     * 
     * @param filename
     * @param newExtension
     * @return
     */
    public static String swapExtension(String filename, String newExtension) {
        int lastIndex = filename.lastIndexOf(".");
        if (lastIndex < 0) return filename + newExtension;

        return filename.substring(0, lastIndex) + newExtension;
    }

    public static File[] getFiles(String directory, FilenameFilter filter) {
        File dir = new File(directory);
        if (!dir.isDirectory()) { return null; }

        File[] files = dir.listFiles(filter);

        return files;
    }
}
