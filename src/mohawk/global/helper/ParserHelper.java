package mohawk.global.helper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import org.antlr.v4.runtime.*;

import mohawk.global.parser.BooleanErrorListener;
import mohawk.global.parser.mohawkT.MohawkTARBACLexer;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;

public class ParserHelper {
    public final static Logger logger = Logger.getLogger("mohawk");

    public BooleanErrorListener error = new BooleanErrorListener();

    public MohawkTARBACParser parseMohawkTString(String policyStr) throws IOException {
        InputStream stream = new ByteArrayInputStream(policyStr.getBytes(StandardCharsets.UTF_8));

        error.errorFound = false; // reset the error listener
        MohawkTARBACParser parser = ParserHelper.runParser(stream, error, false);

        if (error.errorFound) {
            logger.warning("Unable to parse the string: \n" + policyStr);
        }

        return parser;
    }

    public MohawkTARBACParser parseMohawkTFile(File specFile) throws IOException {
        FileInputStream fis = new FileInputStream(specFile);

        error.errorFound = false; // reset the error listener
        MohawkTARBACParser parser = ParserHelper.runParser(fis, error, false);

        if (error.errorFound) {
            logger.warning("Unable to parse the file: " + specFile.getAbsolutePath());
        }

        return parser;
    }

    public static MohawkTARBACParser runParser(InputStream is, BaseErrorListener errorListener, Boolean displayStats)
            throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(is);
        MohawkTARBACLexer lexer = new MohawkTARBACLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MohawkTARBACParser parser = new MohawkTARBACParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        parser.init();

        if (displayStats) {
            System.out.println(parser.mohawkT.toString());
        }

        return parser;
    }
}
