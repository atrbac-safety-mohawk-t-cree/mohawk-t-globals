package mohawk.global.results;

import java.util.ArrayList;

import mohawk.global.pieces.*;

public class CounterExample {
    public ArrayList<CounterExampleStep> steps = new ArrayList<>();
    public boolean goalReached = true;
    public MohawkT _policy;

    public CounterExample() {}
    public CounterExample(boolean goalReached) {
        this.goalReached = goalReached;
    }

    public void addStep(CounterExampleStep step) {
        steps.add(step);
    }

    public int numberOfSteps() {
        return steps.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Initial State -> ");

        for (CounterExampleStep ces : steps) {
            sb.append(ces.toString()).append(" -> ");
        }

        if (goalReached) {
            sb.append("Goal State");
        } else {
            sb.append("Goal State is Unreachable");
        }
        return sb.toString();
    }

    public String ruleString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Initial State -> ");

        for (CounterExampleStep ces : steps) {
            sb.append(ces.ruleString(_policy)).append(" -> ");
        }

        if (goalReached) {
            sb.append("Goal State");
        } else {
            sb.append("Goal State is Unreachable");
        }
        return sb.toString();
    }

    public CounterExample addMohawkT(MohawkT testcase) {
        _policy = testcase;
        return this;
    }

    /**
     * If the last step was added in error, then you can remove it with this line
     */
    public void removeLastStep() {
        steps.remove(steps.size() - 1);
    }

    /**
     * Only removes the last step if it is a CR/CE/CD rule (because the path must end in a CA rule),
     * and it will remove a CA rule if the target role is not in the policy's query.
     * @param testcase
     * @return
     */
    public boolean conditionalRemoveLastStep() {
        boolean removed = false;

        if (steps.size() == 0) { return false; }

        CounterExampleStep ces = steps.get(steps.size() - 1);

        if (ces.getRuleType() != RuleType.ASSIGN) {
            removeLastStep();
            removed = true;
        } else {
            Rule r = ces.getRule(_policy);
            if (!_policy.query.contains(r._role)) {
                removeLastStep();
                removed = true;
            }
        }

        return removed;
    }
}
