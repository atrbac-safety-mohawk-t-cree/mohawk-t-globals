package mohawk.global.results;

import mohawk.global.pieces.ExpectedResult;

public enum ExecutionResult {
    GOAL_REACHABLE("System is UNSAFE!", "System is UNSAFE!", "Goal is reachable!", "Goal is reachable!",
            "Result: GOAL_REACHABLE", "Result: GOAL_REACHABLE"),//
    GOAL_UNREACHABLE("System is SAFE!", "System is SAFE!", "Goal was not reachable!", "Goal was not reachable!",
            "Result: GOAL_UNREACHABLE", "Result: GOAL_UNREACHABLE"), //
    ERROR_OCCURRED("", "", "", "", "", ""), //
    TIMEOUT("", "", "", "", "", ""), //
    CONVERT_ONLY("", "", "", "", "", ""), //
    OUT_OF_MEMORY("", "", "", "", "java.lang.OutOfMemoryError", "java.lang.OutOfMemoryError"),
    /** Used for the conversion of {@link ExpectedResult#UNKNOWN} */
    GOAL_UNKNOWN("", "", "", "", "", ""); //

    private String _asaptimeNSA;
    private String _asaptimeSA;
    private String _trole;
    private String _trule;
    private String _mohawk;
    private String _mohawkT;

    private ExecutionResult(String asaptimeNSA, String asaptimeSA, String trole, String trule, String mohawk,
            String mohawkT) {
        _asaptimeNSA = asaptimeNSA;
        _asaptimeSA = asaptimeSA;
        _trole = trole;
        _trule = trule;
        _mohawk = mohawk;
        _mohawkT = mohawkT;
    }

    public String getASAPTimeNSASearchString() {
        return _asaptimeNSA;
    }

    public String getASAPTimeSASearchString() {
        return _asaptimeSA;
    }

    public String getTRoleSearchString() {
        return _trole;
    }

    public String getTRuleSearchString() {
        return _trule;
    }

    public String getMohawkSearchString() {
        return _mohawk;
    }

    public String getMohawkTSearchString() {
        return _mohawkT;
    }
}
