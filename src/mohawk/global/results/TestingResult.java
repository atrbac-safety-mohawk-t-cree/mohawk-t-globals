package mohawk.global.results;

import java.io.File;
import java.time.LocalDateTime;

public class TestingResult {
    /** */
    public String _name;
    /** The name of the file that was passed to the solver */
    public String _filename;
    /** The result from the solver, or in the case of an error the specific error code */
    public ExecutionResult _result;
    /** The expected result from the input policy */
    public ExecutionResult _expectedResult;
    /** The time it took the solver to return after execution */
    public Long _duration;
    /** The Program that was used to solve the program */
    public String _program;
    /** The testing mode that was used: SMC or BMC */
    public String _mode;
    /** Indicates if abstraction refinement was used */
    public boolean _abstractionRefinement;
    /** Indicates if static slicing was used */
    public boolean _staticSlicing;
    /** If bounded model checking was use, this will hold the bound; otherwise null */
    public Double _bound = null;
    /** The return/exit code that was received from the executable/solver (might indicate an error) */
    public String _returnCode;
    /** Any comments that should be attached to this testing result */
    public String _comment;
    /** If the result was reachable, and there exists a counterexample, then it will be stored here. */
    public CounterExample _counterExample;

    /** the date and time that the testing result was created */
    public String _datetime;

    public TestingResult(ExecutionResult result, //
            ExecutionResult expectedResult, //
            Long duration, //
            String name, //
            String filename, //
            String program, //
            String mode, //
            Double bound, //
            boolean abstractionRefinement, //
            boolean staticSlicing, //
            String returnCode, //
            String comment,//
            CounterExample counterExample) {
        _result = result;
        _expectedResult = expectedResult;
        _duration = duration;
        _name = name;
        _filename = filename;
        _program = program;
        _mode = mode;
        _bound = bound;
        _abstractionRefinement = abstractionRefinement;
        _staticSlicing = staticSlicing;
        _comment = comment;
        _datetime = LocalDateTime.now().toString();
        _returnCode = returnCode;
        _counterExample = counterExample;
    }

    public TestingResult(ExecutionResult result, //
            ExecutionResult expectedResult, //
            Long duration, //
            String name, //
            String filename, //
            String program, //
            String mode, //
            Double bound, //
            String returnCode, //
            String comment) {
        this(result, expectedResult, duration, name, filename, program, mode, bound, false, false, returnCode, comment,
                new CounterExample());
    }

    public TestingResult(ExecutionResult result, ExecutionResult expectedResult, Long duration, String name,
            File specFile, String program, String mode, Double bound, String returnCode, String comment) {
        this(result, expectedResult, duration, name,
                (specFile == null) ? "<No File Provided>" : specFile.getAbsolutePath(), program, mode, bound,
                returnCode, comment);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        s.append("TestingResult{").append("Name: ").append(_name).append("; Filename: ").append(_filename)
                .append("; Date: ").append(_datetime).append("; Mode: ").append(_mode).append("; Bound: ")
                .append(_bound).append("; Return Code: ").append(_returnCode).append("; Result: ").append(_result)
                .append("; Expected Result: ").append(_expectedResult).append("; [").append(_duration).append(" ms]")
                .append((_comment.isEmpty()) ? "" : " - Comment: " + _comment)
                .append((_counterExample.numberOfSteps() == 0)
                        ? ""
                        : " - Counter Example: " + _counterExample.ruleString());

        return s.toString();
    }
}
