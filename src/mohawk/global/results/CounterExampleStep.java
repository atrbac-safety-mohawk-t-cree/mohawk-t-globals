package mohawk.global.results;

import java.util.ArrayList;

import mohawk.global.pieces.*;

public class CounterExampleStep {
    public static boolean useShortString = true;
    public static int shortStringVersion = 2;

    public String adminUser = "TRUE";
    public String targetUser = "{ERROR! Target User not Provided}";
    public String rule = "{ERROR! Rule not Provided}";

    public CounterExampleStep(String rule, String targetUser, String adminUser) {
        this.rule = rule;
        this.targetUser = targetUser;
        this.adminUser = adminUser;
    }

    public String getLongString() {
        return "{Rule: " + rule + "; Admin User[" + adminUser + "] -> Target User[" + targetUser + "]}";
    }

    public String getShortString() {
        switch (shortStringVersion) {
            default :
            case 1 :
                return "a" + adminUser + " -" + rule + "> T" + targetUser;
            case 2 :
                return rule + "[a" + adminUser + ", T" + targetUser + "]";
        }
    }

    @Override
    public String toString() {
        if (useShortString) { return getShortString(); }
        return getLongString();
    }

    public RuleType getRuleType() {
        String ruleType = rule.substring(0, 2);
        switch (ruleType) {
            case "CA" :
                return RuleType.ASSIGN;
            case "CR" :
                return RuleType.REVOKE;
            case "CE" :
                return RuleType.ENABLE;
            case "CD" :
                return RuleType.DISABLE;
        }
        return null;
    }

    public String ruleString(MohawkT testcase) {
        RuleType ruleType = getRuleType();
        int ruleIndex = Integer.parseInt(rule.substring(2));
        ArrayList<Rule> rules = new ArrayList<>();

        switch (ruleType) {
            case ASSIGN :
                rules = testcase.canAssign.getRules();
                break;
            case REVOKE :
                rules = testcase.canRevoke.getRules();
                break;
            case ENABLE :
                rules = testcase.canEnable.getRules();
                break;
            case DISABLE :
                rules = testcase.canDisable.getRules();
                break;
        }

        for (Rule r : rules) {
            if (r.ruleIndex == null) {
                continue;
            }
            if (r.ruleIndex == ruleIndex) { return ruleType + ":" + r.getString(null, 0); }
        }

        return "<UNKNOWN RULE>";
    }

    public Rule getRule(MohawkT testcase) {
        RuleType ruleType = getRuleType();
        int ruleIndex = Integer.parseInt(rule.substring(2));
        ArrayList<Rule> rules = new ArrayList<>();

        switch (ruleType) {
            case ASSIGN :
                rules = testcase.canAssign.getRules();
                break;
            case REVOKE :
                rules = testcase.canRevoke.getRules();
                break;
            case ENABLE :
                rules = testcase.canEnable.getRules();
                break;
            case DISABLE :
                rules = testcase.canDisable.getRules();
                break;
        }

        for (Rule r : rules) {
            if (r.ruleIndex == null) {
                continue;
            }
            if (r.ruleIndex == ruleIndex) { return r; }
        }

        return null;
    }
}
