package mohawk.global.results;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

/** @author Jonathan Shahen */

public class MohawkResults {
    public final static Logger logger = Logger.getLogger("mohawk");
    public ArrayList<TestingResult> history = new ArrayList<TestingResult>();
    private TestingResult lastTestResult = null;

    public String[] heading = new String[]{"Result Name", "File", "Datetime", "Program", "Mode", "Bound",
            "Abstraction Refinemnet", "Static Slicing", "Result", "Expected Result", "Return Code", "Duration (ms)",
            "Comment", "Counter Example"};

    public TestingResult getLastResult() {
        return lastTestResult;
    }

    public void add(TestingResult result) {
        history.add(result);
        lastTestResult = result;
    }

    public Boolean writeOut(File results) throws IOException {
        return writeOut(results, false);
    }

    public Boolean writeOut(File results, Boolean forceOverwrite) throws IOException {
        FileWriter fw = getFileWriter(results, forceOverwrite);

        if (fw == null) {
            logger.warning("[TESTING_RESULTS] Unable to create the filewriter");
            return false;
        }

        for (TestingResult result : history) {
            if (writeOutSingle(fw, result) == false) {
                logger.warning("[TESTING_RESULTS] Unable to write the row: " + result.toString());
                return false;
            }
        }
        fw.close();

        return true;
    }

    public FileWriter getFileWriter(File results, Boolean forceOverwrite) throws IOException {
        Boolean writeHeader = forceOverwrite;
        if (!results.exists()) {
            if (!results.createNewFile()) {
                logger.warning("[TESTING_RESULTS] Unable to create the file: " + results.getAbsolutePath());
                return null;
            }
            writeHeader = true;
        }

        if (!results.isFile()) {
            logger.warning("[TESTING_RESULTS] Parameter pointing to something that is not a file: "
                    + results.getAbsolutePath());
            return null;
        }

        FileWriter fw = new FileWriter(results, !forceOverwrite);

        if (writeHeader) {
            writeHeader(fw);
        }
        return fw;
    }

    public Boolean writeOutLast(FileWriter fw) throws IOException {
        return writeOutSingle(fw, lastTestResult);
    }

    public Boolean writeOutSingle(FileWriter fw, TestingResult result) throws IOException {
        if (result == null) { return false; }

        String bound = "";
        if (result._bound != null) {
            bound = String.valueOf(result._bound.intValue());
        }

        fw.write(StringEscapeUtils.escapeCsv(result._name) + "," + // Results name
                StringEscapeUtils.escapeCsv(result._filename) + "," + // Filename of input testcase
                StringEscapeUtils.escapeCsv(result._datetime) + "," + // Date Time of execution
                StringEscapeUtils.escapeCsv(result._program) + "," + // Program used to solve testcase
                StringEscapeUtils.escapeCsv(result._mode) + "," + // Mode (SMC/BMC)
                StringEscapeUtils.escapeCsv(bound) + "," + // Bound
                StringEscapeUtils.escapeCsv(String.valueOf(result._abstractionRefinement)) + "," + //
                StringEscapeUtils.escapeCsv(String.valueOf(result._staticSlicing)) + "," + //
                StringEscapeUtils.escapeCsv(result._result.toString()) + "," + //
                StringEscapeUtils.escapeCsv(String.valueOf(result._expectedResult)) + "," + //
                StringEscapeUtils.escapeCsv(result._returnCode) + "," + //
                result._duration + "," + //
                StringEscapeUtils.escapeCsv(result._comment) + "," +//
                StringEscapeUtils.escapeCsv(result._counterExample.ruleString()) + "\n");

        fw.flush();// flushes just in-case this function is run once in a while!
        return true;
    }

    public void writeHeader(FileWriter fw) throws IOException {
        fw.write(StringUtils.join(heading, ",") + "\n");
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        int i = 0;
        for (TestingResult result : history) {
            if (i != 0) {
                t.append("\n");
            }
            i++;
            t.append(result.toString());
        }
        return t.toString();
    }
}
