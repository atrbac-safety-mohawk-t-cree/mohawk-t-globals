/**
 * 
 */
grammar MohawkTARBAC;

@header {
package mohawk.global.parser.mohawkT;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;
}

@members {
	public final Logger logger = Logger.getLogger("mohawk");

	public boolean canassign_found = false;
	public boolean canrevoke_found = false;
	public boolean canenable_found = false;
	public boolean candisable_found = false;
	public boolean query_found = false;
	public boolean expected_found = false;
	int tabsize = 1;
	
	/* Global States */
	public MohawkT mohawkT = new MohawkT("Mohawk Converter");
	
	
	private void logmsg(String msg) {
		if(logger.getLevel() == Level.FINE) {
			System.out.println(StringUtils.repeat("  ", tabsize) + msg);
		}
	}
}

init
:
	stat stat stat stat stat stat
;

stat
:
	{canassign_found==false}?

	canassign
	{canassign_found=true;}

	|
	{canrevoke_found==false}?

	canrevoke
	{canrevoke_found=true;}

	|
	{canenable_found==false}?

	canenable
	{canenable_found=true;}

	|
	{candisable_found==false}?

	candisable
	{candisable_found=true;}

	|
	{query_found==false}?

	query
	{query_found=true;}

	|
	{expected_found==false}?

	expected
	{expected_found=true;}

;

canassign
:
	{logmsg("Entering Can Assign");tabsize++;}

	CanAssign LeftBrace
	(
		myrule [RuleType.ASSIGN]
	)* RightBrace
	{tabsize--; logmsg("Leaving Can Assign");}

;

canrevoke
:
	{logmsg("Entering Can Revoke");tabsize++;}

	CanRevoke LeftBrace
	(
		myrule [RuleType.REVOKE]
	)* RightBrace
	{tabsize--; logmsg("Leaving Can Revoke");}

;

canenable
:
	{logmsg("Entering Can Enable"); tabsize++;}

	CanEnable LeftBrace
	(
		myrule [RuleType.ENABLE]
	)* RightBrace
	{tabsize--; logmsg("Leaving Can Enable");}

;

candisable
:
	{logmsg("Entering Can Disable");tabsize++;}

	CanDisable LeftBrace
	(
		myrule [RuleType.DISABLE]
	)* RightBrace
	{tabsize--; logmsg("Leaving Can Disable");}

;

query
:
	Query Colon t = timeslot Comma ra = roleArray
	{
		mohawkT.query._timeslot = new TimeSlot($t.val);
		mohawkT.query._roles = $ra.r;
		mohawkT.timeIntervalHelper.add(mohawkT.query._timeslot);
	}

;

expected
:
	Expected Colon
	(
		Reachable
		{mohawkT.expectedResult = ExpectedResult.REACHABLE;}

		| Unreachable
		{mohawkT.expectedResult = ExpectedResult.UNREACHABLE;}

		| Unknown
		{mohawkT.expectedResult = ExpectedResult.UNKNOWN;}

	)
;

myrule [RuleType rt] returns [Rule r]
locals [Boolean adminAllRoles]
:
	{logmsg("Adding Rule: "); tabsize++;}

	LeftAngle
	(
		adminRole = myrole
		{$adminAllRoles=false;}

		| True
		{$adminAllRoles=true;}

	) Comma ti = timeInterval Comma pc = precondition Comma tsa = timeslotArray
	Comma role = myrole RightAngle
	{
		$r = new Rule();
		$r._type = rt;
		if($adminAllRoles == true) {
			$r._adminRole = Role.allRoles();
		} else {
			$r._adminRole = $adminRole.role;
			mohawkT.roleHelper.addAdmin($adminRole.role);
		}
		$r._adminTimeInterval = new TimeInterval($ti.t0, $ti.t1);
		$r._preconditions = $pc.p;
		$r._roleSchedule = $tsa.t;
		$r._role = $role.role;
		
		mohawkT.addRule($r, false);
		tabsize--; 
		logmsg($r.toString());
	}

;

precondition returns [ArrayList<Role> p] @init {
	$p = new ArrayList<Role>();
}
:
	a = rolecondition
	{$p.add($a.r);}

	(
		AND b = rolecondition
		{$p.add($b.r);}

	)*
	| True
;

rolecondition returns [Role r] @init {
	boolean not = false;
}
:
	(
		not
		{not = true;}

	)? myrole
	{
		$r = new Role($myrole.rolet, "", not);
		/* logmsg("Role: "+ $r); */
	}

;

roleArray returns [ArrayList<Role> r] @init {
	$r = new ArrayList<Role>();
}
:
	LeftBracket a = myrole
	{$r.add($a.role);}

	(
		Comma b = myrole
		{$r.add($b.role);}

	)* RightBracket
	| a = myrole
	{$r.add($a.role);}

;

timeslotArray returns [ArrayList<TimeSlot> t] @init {
	$t = new ArrayList<TimeSlot>();
}
:
	LeftBracket a = timeslot
	{
		$t.add(new TimeSlot($a.val));
		mohawkT.timeIntervalHelper.add(new TimeSlot($a.val));
	}

	(
		',' b = timeslot
		{
			$t.add(new TimeSlot($b.val));
			mohawkT.timeIntervalHelper.add(new TimeSlot($b.val));
		}

	)* RightBracket
	| c = timeslot
	{
		$t.add(new TimeSlot($c.val));
		mohawkT.timeIntervalHelper.add(new TimeSlot($c.val));
	}

	| True
;

timeInterval returns [Integer t0, Integer t1]
:
	a = timeslot
	{$t0 = $a.val; $t1=$t0;}

	(
		'-' b = timeslot
		{$t1=$b.val;}

	)?
	{
		mohawkT.timeIntervalHelper.add(new TimeInterval($t0, $t1));
	}

;

timeslot returns [Integer val]
:
	Timeslot
	{
		$val = new Integer($Timeslot.text.substring(1));
		
	}

;

myrole returns [String rolet, Role role]
:
	MyRole
	{
		$rolet = $MyRole.getText();
		$role = new Role($rolet);
		mohawkT.roleHelper.add($role);
	}

;

not
:
	Not '~'
;

/* Whitespace and Comments */
Whitespace
:
	[ \t]+ -> skip
;

Newline
:
	(
		'\r' '\n'?
		| '\n'
	) -> skip
;

BlockComment
:
	'/*' .*? '*/' -> skip
;

LineComment
:
	'//' ~[\r\n]* -> skip
;

/* Key Words */
Timeslot
:
	't' INT
;

CanAssign
:
	'CanAssign'
;

CanRevoke
:
	'CanRevoke'
;

CanEnable
:
	'CanEnable'
;

CanDisable
:
	'CanDisable'
;

/*  NOT USED ANYMORE
MaxRoles
:
	'MaxRoles'
;

MaxTimeSlots
:
	'MaxTimeSlots'
;
*/
Query
:
	'Query'
;

Expected
:
	'Expected'
;

Not
:
	'NOT'
;

True
:
	'TRUE'
;

False
:
	'FALSE'
;

Unknown
:
	'UNKNOWN'
;

Reachable
:
	'REACHABLE'
;

Unreachable
:
	'UNREACHABLE'
;

// Special Notation
/*
RoleStrict
:
	'role' INT
;
*/
/* Called MyRole because there exists a class named Role */
MyRole
:
	[A-Za-z0-9]+
;

// Literals

LeftAngle
:
	'<'
;

RightAngle
:
	'>'
;

LeftBracket
:
	'['
;

RightBracket
:
	']'
;

LeftBrace
:
	'{'
;

RightBrace
:
	'}'
;

AND
:
	'&'
;

Colon
:
	':'
;

Comma
:
	','
;

fragment
DIGIT
:
	[0-9]
;

INT
:
	DIGIT+
;