// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawkT\MohawkTARBAC.g4 by ANTLR 4.4

package mohawk.global.parser.mohawkT;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MohawkTARBACParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__1=1, T__0=2, Whitespace=3, Newline=4, BlockComment=5, LineComment=6, 
		Timeslot=7, CanAssign=8, CanRevoke=9, CanEnable=10, CanDisable=11, Query=12, 
		Expected=13, Not=14, True=15, False=16, Unknown=17, Reachable=18, Unreachable=19, 
		MyRole=20, LeftAngle=21, RightAngle=22, LeftBracket=23, RightBracket=24, 
		LeftBrace=25, RightBrace=26, AND=27, Colon=28, Comma=29, INT=30;
	public static final String[] tokenNames = {
		"<INVALID>", "'-'", "'~'", "Whitespace", "Newline", "BlockComment", "LineComment", 
		"Timeslot", "'CanAssign'", "'CanRevoke'", "'CanEnable'", "'CanDisable'", 
		"'Query'", "'Expected'", "'NOT'", "'TRUE'", "'FALSE'", "'UNKNOWN'", "'REACHABLE'", 
		"'UNREACHABLE'", "MyRole", "'<'", "'>'", "'['", "']'", "'{'", "'}'", "'&'", 
		"':'", "','", "INT"
	};
	public static final int
		RULE_init = 0, RULE_stat = 1, RULE_canassign = 2, RULE_canrevoke = 3, 
		RULE_canenable = 4, RULE_candisable = 5, RULE_query = 6, RULE_expected = 7, 
		RULE_myrule = 8, RULE_precondition = 9, RULE_rolecondition = 10, RULE_roleArray = 11, 
		RULE_timeslotArray = 12, RULE_timeInterval = 13, RULE_timeslot = 14, RULE_myrole = 15, 
		RULE_not = 16;
	public static final String[] ruleNames = {
		"init", "stat", "canassign", "canrevoke", "canenable", "candisable", "query", 
		"expected", "myrule", "precondition", "rolecondition", "roleArray", "timeslotArray", 
		"timeInterval", "timeslot", "myrole", "not"
	};

	@Override
	public String getGrammarFileName() { return "MohawkTARBAC.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		public final Logger logger = Logger.getLogger("mohawk");

		public boolean canassign_found = false;
		public boolean canrevoke_found = false;
		public boolean canenable_found = false;
		public boolean candisable_found = false;
		public boolean query_found = false;
		public boolean expected_found = false;
		int tabsize = 1;
		
		/* Global States */
		public MohawkT mohawkT = new MohawkT("Mohawk Converter");
		
		
		private void logmsg(String msg) {
			if(logger.getLevel() == Level.FINE) {
				System.out.println(StringUtils.repeat("  ", tabsize) + msg);
			}
		}

	public MohawkTARBACParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class InitContext extends ParserRuleContext {
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitInit(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_init);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34); stat();
			setState(35); stat();
			setState(36); stat();
			setState(37); stat();
			setState(38); stat();
			setState(39); stat();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatContext extends ParserRuleContext {
		public CanenableContext canenable() {
			return getRuleContext(CanenableContext.class,0);
		}
		public ExpectedContext expected() {
			return getRuleContext(ExpectedContext.class,0);
		}
		public QueryContext query() {
			return getRuleContext(QueryContext.class,0);
		}
		public CandisableContext candisable() {
			return getRuleContext(CandisableContext.class,0);
		}
		public CanrevokeContext canrevoke() {
			return getRuleContext(CanrevokeContext.class,0);
		}
		public CanassignContext canassign() {
			return getRuleContext(CanassignContext.class,0);
		}
		public StatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitStat(this);
		}
	}

	public final StatContext stat() throws RecognitionException {
		StatContext _localctx = new StatContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_stat);
		try {
			setState(65);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(41);
				if (!(canassign_found==false)) throw new FailedPredicateException(this, "canassign_found==false");
				setState(42); canassign();
				canassign_found=true;
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(45);
				if (!(canrevoke_found==false)) throw new FailedPredicateException(this, "canrevoke_found==false");
				setState(46); canrevoke();
				canrevoke_found=true;
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(49);
				if (!(canenable_found==false)) throw new FailedPredicateException(this, "canenable_found==false");
				setState(50); canenable();
				canenable_found=true;
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(53);
				if (!(candisable_found==false)) throw new FailedPredicateException(this, "candisable_found==false");
				setState(54); candisable();
				candisable_found=true;
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(57);
				if (!(query_found==false)) throw new FailedPredicateException(this, "query_found==false");
				setState(58); query();
				query_found=true;
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(61);
				if (!(expected_found==false)) throw new FailedPredicateException(this, "expected_found==false");
				setState(62); expected();
				expected_found=true;
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CanassignContext extends ParserRuleContext {
		public TerminalNode CanAssign() { return getToken(MohawkTARBACParser.CanAssign, 0); }
		public TerminalNode RightBrace() { return getToken(MohawkTARBACParser.RightBrace, 0); }
		public List<MyruleContext> myrule() {
			return getRuleContexts(MyruleContext.class);
		}
		public MyruleContext myrule(int i) {
			return getRuleContext(MyruleContext.class,i);
		}
		public TerminalNode LeftBrace() { return getToken(MohawkTARBACParser.LeftBrace, 0); }
		public CanassignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_canassign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterCanassign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitCanassign(this);
		}
	}

	public final CanassignContext canassign() throws RecognitionException {
		CanassignContext _localctx = new CanassignContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_canassign);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			logmsg("Entering Can Assign");tabsize++;
			setState(68); match(CanAssign);
			setState(69); match(LeftBrace);
			setState(73);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LeftAngle) {
				{
				{
				setState(70); myrule(RuleType.ASSIGN);
				}
				}
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(76); match(RightBrace);
			tabsize--; logmsg("Leaving Can Assign");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CanrevokeContext extends ParserRuleContext {
		public TerminalNode CanRevoke() { return getToken(MohawkTARBACParser.CanRevoke, 0); }
		public TerminalNode RightBrace() { return getToken(MohawkTARBACParser.RightBrace, 0); }
		public List<MyruleContext> myrule() {
			return getRuleContexts(MyruleContext.class);
		}
		public MyruleContext myrule(int i) {
			return getRuleContext(MyruleContext.class,i);
		}
		public TerminalNode LeftBrace() { return getToken(MohawkTARBACParser.LeftBrace, 0); }
		public CanrevokeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_canrevoke; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterCanrevoke(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitCanrevoke(this);
		}
	}

	public final CanrevokeContext canrevoke() throws RecognitionException {
		CanrevokeContext _localctx = new CanrevokeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_canrevoke);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			logmsg("Entering Can Revoke");tabsize++;
			setState(80); match(CanRevoke);
			setState(81); match(LeftBrace);
			setState(85);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LeftAngle) {
				{
				{
				setState(82); myrule(RuleType.REVOKE);
				}
				}
				setState(87);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(88); match(RightBrace);
			tabsize--; logmsg("Leaving Can Revoke");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CanenableContext extends ParserRuleContext {
		public TerminalNode RightBrace() { return getToken(MohawkTARBACParser.RightBrace, 0); }
		public TerminalNode CanEnable() { return getToken(MohawkTARBACParser.CanEnable, 0); }
		public List<MyruleContext> myrule() {
			return getRuleContexts(MyruleContext.class);
		}
		public MyruleContext myrule(int i) {
			return getRuleContext(MyruleContext.class,i);
		}
		public TerminalNode LeftBrace() { return getToken(MohawkTARBACParser.LeftBrace, 0); }
		public CanenableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_canenable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterCanenable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitCanenable(this);
		}
	}

	public final CanenableContext canenable() throws RecognitionException {
		CanenableContext _localctx = new CanenableContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_canenable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			logmsg("Entering Can Enable"); tabsize++;
			setState(92); match(CanEnable);
			setState(93); match(LeftBrace);
			setState(97);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LeftAngle) {
				{
				{
				setState(94); myrule(RuleType.ENABLE);
				}
				}
				setState(99);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(100); match(RightBrace);
			tabsize--; logmsg("Leaving Can Enable");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CandisableContext extends ParserRuleContext {
		public TerminalNode CanDisable() { return getToken(MohawkTARBACParser.CanDisable, 0); }
		public TerminalNode RightBrace() { return getToken(MohawkTARBACParser.RightBrace, 0); }
		public List<MyruleContext> myrule() {
			return getRuleContexts(MyruleContext.class);
		}
		public MyruleContext myrule(int i) {
			return getRuleContext(MyruleContext.class,i);
		}
		public TerminalNode LeftBrace() { return getToken(MohawkTARBACParser.LeftBrace, 0); }
		public CandisableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_candisable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterCandisable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitCandisable(this);
		}
	}

	public final CandisableContext candisable() throws RecognitionException {
		CandisableContext _localctx = new CandisableContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_candisable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			logmsg("Entering Can Disable");tabsize++;
			setState(104); match(CanDisable);
			setState(105); match(LeftBrace);
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LeftAngle) {
				{
				{
				setState(106); myrule(RuleType.DISABLE);
				}
				}
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(112); match(RightBrace);
			tabsize--; logmsg("Leaving Can Disable");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryContext extends ParserRuleContext {
		public TimeslotContext t;
		public RoleArrayContext ra;
		public RoleArrayContext roleArray() {
			return getRuleContext(RoleArrayContext.class,0);
		}
		public TerminalNode Query() { return getToken(MohawkTARBACParser.Query, 0); }
		public TerminalNode Colon() { return getToken(MohawkTARBACParser.Colon, 0); }
		public TerminalNode Comma() { return getToken(MohawkTARBACParser.Comma, 0); }
		public TimeslotContext timeslot() {
			return getRuleContext(TimeslotContext.class,0);
		}
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitQuery(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115); match(Query);
			setState(116); match(Colon);
			setState(117); ((QueryContext)_localctx).t = timeslot();
			setState(118); match(Comma);
			setState(119); ((QueryContext)_localctx).ra = roleArray();

					mohawkT.query._timeslot = new TimeSlot(((QueryContext)_localctx).t.val);
					mohawkT.query._roles = ((QueryContext)_localctx).ra.r;
					mohawkT.timeIntervalHelper.add(mohawkT.query._timeslot);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpectedContext extends ParserRuleContext {
		public TerminalNode Unreachable() { return getToken(MohawkTARBACParser.Unreachable, 0); }
		public TerminalNode Expected() { return getToken(MohawkTARBACParser.Expected, 0); }
		public TerminalNode Colon() { return getToken(MohawkTARBACParser.Colon, 0); }
		public TerminalNode Unknown() { return getToken(MohawkTARBACParser.Unknown, 0); }
		public TerminalNode Reachable() { return getToken(MohawkTARBACParser.Reachable, 0); }
		public ExpectedContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expected; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterExpected(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitExpected(this);
		}
	}

	public final ExpectedContext expected() throws RecognitionException {
		ExpectedContext _localctx = new ExpectedContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_expected);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122); match(Expected);
			setState(123); match(Colon);
			setState(130);
			switch (_input.LA(1)) {
			case Reachable:
				{
				setState(124); match(Reachable);
				mohawkT.expectedResult = ExpectedResult.REACHABLE;
				}
				break;
			case Unreachable:
				{
				setState(126); match(Unreachable);
				mohawkT.expectedResult = ExpectedResult.UNREACHABLE;
				}
				break;
			case Unknown:
				{
				setState(128); match(Unknown);
				mohawkT.expectedResult = ExpectedResult.UNKNOWN;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MyruleContext extends ParserRuleContext {
		public RuleType rt;
		public Rule r;
		public Boolean adminAllRoles;
		public MyroleContext adminRole;
		public TimeIntervalContext ti;
		public PreconditionContext pc;
		public TimeslotArrayContext tsa;
		public MyroleContext role;
		public MyroleContext myrole(int i) {
			return getRuleContext(MyroleContext.class,i);
		}
		public TerminalNode Comma(int i) {
			return getToken(MohawkTARBACParser.Comma, i);
		}
		public TimeIntervalContext timeInterval() {
			return getRuleContext(TimeIntervalContext.class,0);
		}
		public PreconditionContext precondition() {
			return getRuleContext(PreconditionContext.class,0);
		}
		public TerminalNode RightAngle() { return getToken(MohawkTARBACParser.RightAngle, 0); }
		public List<MyroleContext> myrole() {
			return getRuleContexts(MyroleContext.class);
		}
		public TerminalNode True() { return getToken(MohawkTARBACParser.True, 0); }
		public List<TerminalNode> Comma() { return getTokens(MohawkTARBACParser.Comma); }
		public TimeslotArrayContext timeslotArray() {
			return getRuleContext(TimeslotArrayContext.class,0);
		}
		public TerminalNode LeftAngle() { return getToken(MohawkTARBACParser.LeftAngle, 0); }
		public MyruleContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MyruleContext(ParserRuleContext parent, int invokingState, RuleType rt) {
			super(parent, invokingState);
			this.rt = rt;
		}
		@Override public int getRuleIndex() { return RULE_myrule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterMyrule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitMyrule(this);
		}
	}

	public final MyruleContext myrule(RuleType rt) throws RecognitionException {
		MyruleContext _localctx = new MyruleContext(_ctx, getState(), rt);
		enterRule(_localctx, 16, RULE_myrule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			logmsg("Adding Rule: "); tabsize++;
			setState(133); match(LeftAngle);
			setState(139);
			switch (_input.LA(1)) {
			case MyRole:
				{
				setState(134); ((MyruleContext)_localctx).adminRole = myrole();
				((MyruleContext)_localctx).adminAllRoles = false;
				}
				break;
			case True:
				{
				setState(137); match(True);
				((MyruleContext)_localctx).adminAllRoles = true;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(141); match(Comma);
			setState(142); ((MyruleContext)_localctx).ti = timeInterval();
			setState(143); match(Comma);
			setState(144); ((MyruleContext)_localctx).pc = precondition();
			setState(145); match(Comma);
			setState(146); ((MyruleContext)_localctx).tsa = timeslotArray();
			setState(147); match(Comma);
			setState(148); ((MyruleContext)_localctx).role = myrole();
			setState(149); match(RightAngle);

					((MyruleContext)_localctx).r =  new Rule();
					_localctx.r._type = rt;
					if(_localctx.adminAllRoles == true) {
						_localctx.r._adminRole = Role.allRoles();
					} else {
						_localctx.r._adminRole = ((MyruleContext)_localctx).adminRole.role;
						mohawkT.roleHelper.addAdmin(((MyruleContext)_localctx).adminRole.role);
					}
					_localctx.r._adminTimeInterval = new TimeInterval(((MyruleContext)_localctx).ti.t0, ((MyruleContext)_localctx).ti.t1);
					_localctx.r._preconditions = ((MyruleContext)_localctx).pc.p;
					_localctx.r._roleSchedule = ((MyruleContext)_localctx).tsa.t;
					_localctx.r._role = ((MyruleContext)_localctx).role.role;
					
					mohawkT.addRule(_localctx.r, false);
					tabsize--; 
					logmsg(_localctx.r.toString());
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreconditionContext extends ParserRuleContext {
		public ArrayList<Role> p;
		public RoleconditionContext a;
		public RoleconditionContext b;
		public List<RoleconditionContext> rolecondition() {
			return getRuleContexts(RoleconditionContext.class);
		}
		public TerminalNode AND(int i) {
			return getToken(MohawkTARBACParser.AND, i);
		}
		public RoleconditionContext rolecondition(int i) {
			return getRuleContext(RoleconditionContext.class,i);
		}
		public List<TerminalNode> AND() { return getTokens(MohawkTARBACParser.AND); }
		public TerminalNode True() { return getToken(MohawkTARBACParser.True, 0); }
		public PreconditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterPrecondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitPrecondition(this);
		}
	}

	public final PreconditionContext precondition() throws RecognitionException {
		PreconditionContext _localctx = new PreconditionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_precondition);

			((PreconditionContext)_localctx).p =  new ArrayList<Role>();

		int _la;
		try {
			setState(164);
			switch (_input.LA(1)) {
			case Not:
			case MyRole:
				enterOuterAlt(_localctx, 1);
				{
				setState(152); ((PreconditionContext)_localctx).a = rolecondition();
				_localctx.p.add(((PreconditionContext)_localctx).a.r);
				setState(160);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==AND) {
					{
					{
					setState(154); match(AND);
					setState(155); ((PreconditionContext)_localctx).b = rolecondition();
					_localctx.p.add(((PreconditionContext)_localctx).b.r);
					}
					}
					setState(162);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case True:
				enterOuterAlt(_localctx, 2);
				{
				setState(163); match(True);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoleconditionContext extends ParserRuleContext {
		public Role r;
		public MyroleContext myrole;
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public NotContext not() {
			return getRuleContext(NotContext.class,0);
		}
		public RoleconditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rolecondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterRolecondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitRolecondition(this);
		}
	}

	public final RoleconditionContext rolecondition() throws RecognitionException {
		RoleconditionContext _localctx = new RoleconditionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_rolecondition);

			boolean not = false;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(169);
			_la = _input.LA(1);
			if (_la==Not) {
				{
				setState(166); not();
				not = true;
				}
			}

			setState(171); ((RoleconditionContext)_localctx).myrole = myrole();

					((RoleconditionContext)_localctx).r =  new Role(((RoleconditionContext)_localctx).myrole.rolet, "", not);
					/* logmsg("Role: "+ _localctx.r); */
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoleArrayContext extends ParserRuleContext {
		public ArrayList<Role> r;
		public MyroleContext a;
		public MyroleContext b;
		public TerminalNode RightBracket() { return getToken(MohawkTARBACParser.RightBracket, 0); }
		public MyroleContext myrole(int i) {
			return getRuleContext(MyroleContext.class,i);
		}
		public TerminalNode Comma(int i) {
			return getToken(MohawkTARBACParser.Comma, i);
		}
		public TerminalNode LeftBracket() { return getToken(MohawkTARBACParser.LeftBracket, 0); }
		public List<MyroleContext> myrole() {
			return getRuleContexts(MyroleContext.class);
		}
		public List<TerminalNode> Comma() { return getTokens(MohawkTARBACParser.Comma); }
		public RoleArrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_roleArray; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterRoleArray(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitRoleArray(this);
		}
	}

	public final RoleArrayContext roleArray() throws RecognitionException {
		RoleArrayContext _localctx = new RoleArrayContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_roleArray);

			((RoleArrayContext)_localctx).r =  new ArrayList<Role>();

		int _la;
		try {
			setState(191);
			switch (_input.LA(1)) {
			case LeftBracket:
				enterOuterAlt(_localctx, 1);
				{
				setState(174); match(LeftBracket);
				setState(175); ((RoleArrayContext)_localctx).a = myrole();
				_localctx.r.add(((RoleArrayContext)_localctx).a.role);
				setState(183);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Comma) {
					{
					{
					setState(177); match(Comma);
					setState(178); ((RoleArrayContext)_localctx).b = myrole();
					_localctx.r.add(((RoleArrayContext)_localctx).b.role);
					}
					}
					setState(185);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(186); match(RightBracket);
				}
				break;
			case MyRole:
				enterOuterAlt(_localctx, 2);
				{
				setState(188); ((RoleArrayContext)_localctx).a = myrole();
				_localctx.r.add(((RoleArrayContext)_localctx).a.role);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeslotArrayContext extends ParserRuleContext {
		public ArrayList<TimeSlot> t;
		public TimeslotContext a;
		public TimeslotContext b;
		public TimeslotContext c;
		public TerminalNode RightBracket() { return getToken(MohawkTARBACParser.RightBracket, 0); }
		public TerminalNode LeftBracket() { return getToken(MohawkTARBACParser.LeftBracket, 0); }
		public TimeslotContext timeslot(int i) {
			return getRuleContext(TimeslotContext.class,i);
		}
		public TerminalNode True() { return getToken(MohawkTARBACParser.True, 0); }
		public List<TimeslotContext> timeslot() {
			return getRuleContexts(TimeslotContext.class);
		}
		public TimeslotArrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeslotArray; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterTimeslotArray(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitTimeslotArray(this);
		}
	}

	public final TimeslotArrayContext timeslotArray() throws RecognitionException {
		TimeslotArrayContext _localctx = new TimeslotArrayContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_timeslotArray);

			((TimeslotArrayContext)_localctx).t =  new ArrayList<TimeSlot>();

		int _la;
		try {
			setState(211);
			switch (_input.LA(1)) {
			case LeftBracket:
				enterOuterAlt(_localctx, 1);
				{
				setState(193); match(LeftBracket);
				setState(194); ((TimeslotArrayContext)_localctx).a = timeslot();

						_localctx.t.add(new TimeSlot(((TimeslotArrayContext)_localctx).a.val));
						mohawkT.timeIntervalHelper.add(new TimeSlot(((TimeslotArrayContext)_localctx).a.val));
					
				setState(202);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Comma) {
					{
					{
					setState(196); match(Comma);
					setState(197); ((TimeslotArrayContext)_localctx).b = timeslot();

								_localctx.t.add(new TimeSlot(((TimeslotArrayContext)_localctx).b.val));
								mohawkT.timeIntervalHelper.add(new TimeSlot(((TimeslotArrayContext)_localctx).b.val));
							
					}
					}
					setState(204);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(205); match(RightBracket);
				}
				break;
			case Timeslot:
				enterOuterAlt(_localctx, 2);
				{
				setState(207); ((TimeslotArrayContext)_localctx).c = timeslot();

						_localctx.t.add(new TimeSlot(((TimeslotArrayContext)_localctx).c.val));
						mohawkT.timeIntervalHelper.add(new TimeSlot(((TimeslotArrayContext)_localctx).c.val));
					
				}
				break;
			case True:
				enterOuterAlt(_localctx, 3);
				{
				setState(210); match(True);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeIntervalContext extends ParserRuleContext {
		public Integer t0;
		public Integer t1;
		public TimeslotContext a;
		public TimeslotContext b;
		public TimeslotContext timeslot(int i) {
			return getRuleContext(TimeslotContext.class,i);
		}
		public List<TimeslotContext> timeslot() {
			return getRuleContexts(TimeslotContext.class);
		}
		public TimeIntervalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeInterval; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterTimeInterval(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitTimeInterval(this);
		}
	}

	public final TimeIntervalContext timeInterval() throws RecognitionException {
		TimeIntervalContext _localctx = new TimeIntervalContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_timeInterval);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(213); ((TimeIntervalContext)_localctx).a = timeslot();
			((TimeIntervalContext)_localctx).t0 =  ((TimeIntervalContext)_localctx).a.val; ((TimeIntervalContext)_localctx).t1 = _localctx.t0;
			setState(219);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(215); match(T__1);
				setState(216); ((TimeIntervalContext)_localctx).b = timeslot();
				((TimeIntervalContext)_localctx).t1 = ((TimeIntervalContext)_localctx).b.val;
				}
			}


					mohawkT.timeIntervalHelper.add(new TimeInterval(_localctx.t0, _localctx.t1));
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeslotContext extends ParserRuleContext {
		public Integer val;
		public Token Timeslot;
		public TerminalNode Timeslot() { return getToken(MohawkTARBACParser.Timeslot, 0); }
		public TimeslotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeslot; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterTimeslot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitTimeslot(this);
		}
	}

	public final TimeslotContext timeslot() throws RecognitionException {
		TimeslotContext _localctx = new TimeslotContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_timeslot);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223); ((TimeslotContext)_localctx).Timeslot = match(Timeslot);

					((TimeslotContext)_localctx).val =  new Integer((((TimeslotContext)_localctx).Timeslot!=null?((TimeslotContext)_localctx).Timeslot.getText():null).substring(1));
					
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MyroleContext extends ParserRuleContext {
		public String rolet;
		public Role role;
		public Token MyRole;
		public TerminalNode MyRole() { return getToken(MohawkTARBACParser.MyRole, 0); }
		public MyroleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_myrole; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterMyrole(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitMyrole(this);
		}
	}

	public final MyroleContext myrole() throws RecognitionException {
		MyroleContext _localctx = new MyroleContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_myrole);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226); ((MyroleContext)_localctx).MyRole = match(MyRole);

					((MyroleContext)_localctx).rolet =  ((MyroleContext)_localctx).MyRole.getText();
					((MyroleContext)_localctx).role =  new Role(_localctx.rolet);
					mohawkT.roleHelper.add(_localctx.role);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotContext extends ParserRuleContext {
		public TerminalNode Not() { return getToken(MohawkTARBACParser.Not, 0); }
		public NotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).enterNot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MohawkTARBACListener ) ((MohawkTARBACListener)listener).exitNot(this);
		}
	}

	public final NotContext not() throws RecognitionException {
		NotContext _localctx = new NotContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_not);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229); match(Not);
			setState(230); match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1: return stat_sempred((StatContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean stat_sempred(StatContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return canassign_found==false;
		case 1: return canrevoke_found==false;
		case 2: return canenable_found==false;
		case 3: return candisable_found==false;
		case 4: return query_found==false;
		case 5: return expected_found==false;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3 \u00eb\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3D\n\3\3\4\3"+
		"\4\3\4\3\4\7\4J\n\4\f\4\16\4M\13\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\7\5V\n"+
		"\5\f\5\16\5Y\13\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\7\6b\n\6\f\6\16\6e\13\6"+
		"\3\6\3\6\3\6\3\7\3\7\3\7\3\7\7\7n\n\7\f\7\16\7q\13\7\3\7\3\7\3\7\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u0085\n\t\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u008e\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u00a1\n\13\f\13\16\13"+
		"\u00a4\13\13\3\13\5\13\u00a7\n\13\3\f\3\f\3\f\5\f\u00ac\n\f\3\f\3\f\3"+
		"\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\7\r\u00b8\n\r\f\r\16\r\u00bb\13\r\3\r\3"+
		"\r\3\r\3\r\3\r\5\r\u00c2\n\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u00cb"+
		"\n\16\f\16\16\16\u00ce\13\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00d6"+
		"\n\16\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00de\n\17\3\17\3\17\3\20\3\20"+
		"\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\22\2\2\23\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"\2\2\u00ee\2$\3\2\2\2\4C\3\2\2\2\6E\3\2\2\2\bQ\3\2\2"+
		"\2\n]\3\2\2\2\fi\3\2\2\2\16u\3\2\2\2\20|\3\2\2\2\22\u0086\3\2\2\2\24\u00a6"+
		"\3\2\2\2\26\u00ab\3\2\2\2\30\u00c1\3\2\2\2\32\u00d5\3\2\2\2\34\u00d7\3"+
		"\2\2\2\36\u00e1\3\2\2\2 \u00e4\3\2\2\2\"\u00e7\3\2\2\2$%\5\4\3\2%&\5\4"+
		"\3\2&\'\5\4\3\2\'(\5\4\3\2()\5\4\3\2)*\5\4\3\2*\3\3\2\2\2+,\6\3\2\2,-"+
		"\5\6\4\2-.\b\3\1\2.D\3\2\2\2/\60\6\3\3\2\60\61\5\b\5\2\61\62\b\3\1\2\62"+
		"D\3\2\2\2\63\64\6\3\4\2\64\65\5\n\6\2\65\66\b\3\1\2\66D\3\2\2\2\678\6"+
		"\3\5\289\5\f\7\29:\b\3\1\2:D\3\2\2\2;<\6\3\6\2<=\5\16\b\2=>\b\3\1\2>D"+
		"\3\2\2\2?@\6\3\7\2@A\5\20\t\2AB\b\3\1\2BD\3\2\2\2C+\3\2\2\2C/\3\2\2\2"+
		"C\63\3\2\2\2C\67\3\2\2\2C;\3\2\2\2C?\3\2\2\2D\5\3\2\2\2EF\b\4\1\2FG\7"+
		"\n\2\2GK\7\33\2\2HJ\5\22\n\2IH\3\2\2\2JM\3\2\2\2KI\3\2\2\2KL\3\2\2\2L"+
		"N\3\2\2\2MK\3\2\2\2NO\7\34\2\2OP\b\4\1\2P\7\3\2\2\2QR\b\5\1\2RS\7\13\2"+
		"\2SW\7\33\2\2TV\5\22\n\2UT\3\2\2\2VY\3\2\2\2WU\3\2\2\2WX\3\2\2\2XZ\3\2"+
		"\2\2YW\3\2\2\2Z[\7\34\2\2[\\\b\5\1\2\\\t\3\2\2\2]^\b\6\1\2^_\7\f\2\2_"+
		"c\7\33\2\2`b\5\22\n\2a`\3\2\2\2be\3\2\2\2ca\3\2\2\2cd\3\2\2\2df\3\2\2"+
		"\2ec\3\2\2\2fg\7\34\2\2gh\b\6\1\2h\13\3\2\2\2ij\b\7\1\2jk\7\r\2\2ko\7"+
		"\33\2\2ln\5\22\n\2ml\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2pr\3\2\2\2q"+
		"o\3\2\2\2rs\7\34\2\2st\b\7\1\2t\r\3\2\2\2uv\7\16\2\2vw\7\36\2\2wx\5\36"+
		"\20\2xy\7\37\2\2yz\5\30\r\2z{\b\b\1\2{\17\3\2\2\2|}\7\17\2\2}\u0084\7"+
		"\36\2\2~\177\7\24\2\2\177\u0085\b\t\1\2\u0080\u0081\7\25\2\2\u0081\u0085"+
		"\b\t\1\2\u0082\u0083\7\23\2\2\u0083\u0085\b\t\1\2\u0084~\3\2\2\2\u0084"+
		"\u0080\3\2\2\2\u0084\u0082\3\2\2\2\u0085\21\3\2\2\2\u0086\u0087\b\n\1"+
		"\2\u0087\u008d\7\27\2\2\u0088\u0089\5 \21\2\u0089\u008a\b\n\1\2\u008a"+
		"\u008e\3\2\2\2\u008b\u008c\7\21\2\2\u008c\u008e\b\n\1\2\u008d\u0088\3"+
		"\2\2\2\u008d\u008b\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0090\7\37\2\2\u0090"+
		"\u0091\5\34\17\2\u0091\u0092\7\37\2\2\u0092\u0093\5\24\13\2\u0093\u0094"+
		"\7\37\2\2\u0094\u0095\5\32\16\2\u0095\u0096\7\37\2\2\u0096\u0097\5 \21"+
		"\2\u0097\u0098\7\30\2\2\u0098\u0099\b\n\1\2\u0099\23\3\2\2\2\u009a\u009b"+
		"\5\26\f\2\u009b\u00a2\b\13\1\2\u009c\u009d\7\35\2\2\u009d\u009e\5\26\f"+
		"\2\u009e\u009f\b\13\1\2\u009f\u00a1\3\2\2\2\u00a0\u009c\3\2\2\2\u00a1"+
		"\u00a4\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a7\3\2"+
		"\2\2\u00a4\u00a2\3\2\2\2\u00a5\u00a7\7\21\2\2\u00a6\u009a\3\2\2\2\u00a6"+
		"\u00a5\3\2\2\2\u00a7\25\3\2\2\2\u00a8\u00a9\5\"\22\2\u00a9\u00aa\b\f\1"+
		"\2\u00aa\u00ac\3\2\2\2\u00ab\u00a8\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00ad"+
		"\3\2\2\2\u00ad\u00ae\5 \21\2\u00ae\u00af\b\f\1\2\u00af\27\3\2\2\2\u00b0"+
		"\u00b1\7\31\2\2\u00b1\u00b2\5 \21\2\u00b2\u00b9\b\r\1\2\u00b3\u00b4\7"+
		"\37\2\2\u00b4\u00b5\5 \21\2\u00b5\u00b6\b\r\1\2\u00b6\u00b8\3\2\2\2\u00b7"+
		"\u00b3\3\2\2\2\u00b8\u00bb\3\2\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2"+
		"\2\2\u00ba\u00bc\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00bd\7\32\2\2\u00bd"+
		"\u00c2\3\2\2\2\u00be\u00bf\5 \21\2\u00bf\u00c0\b\r\1\2\u00c0\u00c2\3\2"+
		"\2\2\u00c1\u00b0\3\2\2\2\u00c1\u00be\3\2\2\2\u00c2\31\3\2\2\2\u00c3\u00c4"+
		"\7\31\2\2\u00c4\u00c5\5\36\20\2\u00c5\u00cc\b\16\1\2\u00c6\u00c7\7\37"+
		"\2\2\u00c7\u00c8\5\36\20\2\u00c8\u00c9\b\16\1\2\u00c9\u00cb\3\2\2\2\u00ca"+
		"\u00c6\3\2\2\2\u00cb\u00ce\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cc\u00cd\3\2"+
		"\2\2\u00cd\u00cf\3\2\2\2\u00ce\u00cc\3\2\2\2\u00cf\u00d0\7\32\2\2\u00d0"+
		"\u00d6\3\2\2\2\u00d1\u00d2\5\36\20\2\u00d2\u00d3\b\16\1\2\u00d3\u00d6"+
		"\3\2\2\2\u00d4\u00d6\7\21\2\2\u00d5\u00c3\3\2\2\2\u00d5\u00d1\3\2\2\2"+
		"\u00d5\u00d4\3\2\2\2\u00d6\33\3\2\2\2\u00d7\u00d8\5\36\20\2\u00d8\u00dd"+
		"\b\17\1\2\u00d9\u00da\7\3\2\2\u00da\u00db\5\36\20\2\u00db\u00dc\b\17\1"+
		"\2\u00dc\u00de\3\2\2\2\u00dd\u00d9\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00df"+
		"\3\2\2\2\u00df\u00e0\b\17\1\2\u00e0\35\3\2\2\2\u00e1\u00e2\7\t\2\2\u00e2"+
		"\u00e3\b\20\1\2\u00e3\37\3\2\2\2\u00e4\u00e5\7\26\2\2\u00e5\u00e6\b\21"+
		"\1\2\u00e6!\3\2\2\2\u00e7\u00e8\7\20\2\2\u00e8\u00e9\7\4\2\2\u00e9#\3"+
		"\2\2\2\21CKWco\u0084\u008d\u00a2\u00a6\u00ab\u00b9\u00c1\u00cc\u00d5\u00dd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}