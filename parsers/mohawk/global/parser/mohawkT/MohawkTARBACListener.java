// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawkT\MohawkTARBAC.g4 by ANTLR 4.4

package mohawk.global.parser.mohawkT;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MohawkTARBACParser}.
 */
public interface MohawkTARBACListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#init}.
	 * @param ctx the parse tree
	 */
	void enterInit(@NotNull MohawkTARBACParser.InitContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#init}.
	 * @param ctx the parse tree
	 */
	void exitInit(@NotNull MohawkTARBACParser.InitContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterStat(@NotNull MohawkTARBACParser.StatContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitStat(@NotNull MohawkTARBACParser.StatContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(@NotNull MohawkTARBACParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(@NotNull MohawkTARBACParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#expected}.
	 * @param ctx the parse tree
	 */
	void enterExpected(@NotNull MohawkTARBACParser.ExpectedContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#expected}.
	 * @param ctx the parse tree
	 */
	void exitExpected(@NotNull MohawkTARBACParser.ExpectedContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#precondition}.
	 * @param ctx the parse tree
	 */
	void enterPrecondition(@NotNull MohawkTARBACParser.PreconditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#precondition}.
	 * @param ctx the parse tree
	 */
	void exitPrecondition(@NotNull MohawkTARBACParser.PreconditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#candisable}.
	 * @param ctx the parse tree
	 */
	void enterCandisable(@NotNull MohawkTARBACParser.CandisableContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#candisable}.
	 * @param ctx the parse tree
	 */
	void exitCandisable(@NotNull MohawkTARBACParser.CandisableContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#timeslotArray}.
	 * @param ctx the parse tree
	 */
	void enterTimeslotArray(@NotNull MohawkTARBACParser.TimeslotArrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#timeslotArray}.
	 * @param ctx the parse tree
	 */
	void exitTimeslotArray(@NotNull MohawkTARBACParser.TimeslotArrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#not}.
	 * @param ctx the parse tree
	 */
	void enterNot(@NotNull MohawkTARBACParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#not}.
	 * @param ctx the parse tree
	 */
	void exitNot(@NotNull MohawkTARBACParser.NotContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#canrevoke}.
	 * @param ctx the parse tree
	 */
	void enterCanrevoke(@NotNull MohawkTARBACParser.CanrevokeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#canrevoke}.
	 * @param ctx the parse tree
	 */
	void exitCanrevoke(@NotNull MohawkTARBACParser.CanrevokeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#myrule}.
	 * @param ctx the parse tree
	 */
	void enterMyrule(@NotNull MohawkTARBACParser.MyruleContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#myrule}.
	 * @param ctx the parse tree
	 */
	void exitMyrule(@NotNull MohawkTARBACParser.MyruleContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#timeslot}.
	 * @param ctx the parse tree
	 */
	void enterTimeslot(@NotNull MohawkTARBACParser.TimeslotContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#timeslot}.
	 * @param ctx the parse tree
	 */
	void exitTimeslot(@NotNull MohawkTARBACParser.TimeslotContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#canenable}.
	 * @param ctx the parse tree
	 */
	void enterCanenable(@NotNull MohawkTARBACParser.CanenableContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#canenable}.
	 * @param ctx the parse tree
	 */
	void exitCanenable(@NotNull MohawkTARBACParser.CanenableContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#canassign}.
	 * @param ctx the parse tree
	 */
	void enterCanassign(@NotNull MohawkTARBACParser.CanassignContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#canassign}.
	 * @param ctx the parse tree
	 */
	void exitCanassign(@NotNull MohawkTARBACParser.CanassignContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#timeInterval}.
	 * @param ctx the parse tree
	 */
	void enterTimeInterval(@NotNull MohawkTARBACParser.TimeIntervalContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#timeInterval}.
	 * @param ctx the parse tree
	 */
	void exitTimeInterval(@NotNull MohawkTARBACParser.TimeIntervalContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#rolecondition}.
	 * @param ctx the parse tree
	 */
	void enterRolecondition(@NotNull MohawkTARBACParser.RoleconditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#rolecondition}.
	 * @param ctx the parse tree
	 */
	void exitRolecondition(@NotNull MohawkTARBACParser.RoleconditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#roleArray}.
	 * @param ctx the parse tree
	 */
	void enterRoleArray(@NotNull MohawkTARBACParser.RoleArrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#roleArray}.
	 * @param ctx the parse tree
	 */
	void exitRoleArray(@NotNull MohawkTARBACParser.RoleArrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link MohawkTARBACParser#myrole}.
	 * @param ctx the parse tree
	 */
	void enterMyrole(@NotNull MohawkTARBACParser.MyroleContext ctx);
	/**
	 * Exit a parse tree produced by {@link MohawkTARBACParser#myrole}.
	 * @param ctx the parse tree
	 */
	void exitMyrole(@NotNull MohawkTARBACParser.MyroleContext ctx);
}