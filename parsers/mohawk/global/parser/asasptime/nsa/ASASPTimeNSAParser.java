// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\asasptime\nsa\ASASPTimeNSA.g4 by ANTLR 4.4

package mohawk.global.parser.asasptime.nsa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ASASPTimeNSAParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, TRUE=2, CONFIG=3, GOAL=4, CANASSIGN=5, CANREVOKE=6, CANENABLE=7, 
		CANDISABLE=8, NEG=9, SEP=10, SEP2=11, SPACE=12, INT=13, WS=14, NL=15;
	public static final String[] tokenNames = {
		"<INVALID>", "'t'", "'true'", "'CONFIG'", "'GOAL'", "'can_assign'", "'can_revoke'", 
		"'can_enable'", "'can_disable'", "'-'", "SEP", "SEP2", "SPACE", "INT", 
		"WS", "NL"
	};
	public static final int
		RULE_init = 0, RULE_config = 1, RULE_query = 2, RULE_myrule = 3, RULE_canassign = 4, 
		RULE_canrevoke = 5, RULE_canenable = 6, RULE_candisable = 7, RULE_rolecondition = 8, 
		RULE_myrole = 9, RULE_myroleA = 10, RULE_timeslot = 11, RULE_precondition = 12;
	public static final String[] ruleNames = {
		"init", "config", "query", "myrule", "canassign", "canrevoke", "canenable", 
		"candisable", "rolecondition", "myrole", "myroleA", "timeslot", "precondition"
	};

	@Override
	public String getGrammarFileName() { return "ASASPTimeNSA.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		public final Logger logger = Logger.getLogger("mohawk");
		
		/* Global States */
		int tabsize = 1;
		public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: ASAPTime NSA");
		public Integer claimedRoles = null;
		
		
		private void logmsg(String msg) {
			if(logger.getLevel() == Level.FINE) {
				System.out.println(StringUtils.repeat("  ", tabsize) + msg);
			}
		}

	public ASASPTimeNSAParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class InitContext extends ParserRuleContext {
		public List<MyruleContext> myrule() {
			return getRuleContexts(MyruleContext.class);
		}
		public QueryContext query() {
			return getRuleContext(QueryContext.class,0);
		}
		public MyruleContext myrule(int i) {
			return getRuleContext(MyruleContext.class,i);
		}
		public ConfigContext config() {
			return getRuleContext(ConfigContext.class,0);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitInit(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_init);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26); config();
			setState(27); query();
			setState(29); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(28); myrule();
				}
				}
				setState(31); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CANASSIGN) | (1L << CANREVOKE) | (1L << CANENABLE) | (1L << CANDISABLE))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConfigContext extends ParserRuleContext {
		public Token r;
		public Token t;
		public TerminalNode INT(int i) {
			return getToken(ASASPTimeNSAParser.INT, i);
		}
		public TerminalNode SPACE(int i) {
			return getToken(ASASPTimeNSAParser.SPACE, i);
		}
		public TerminalNode CONFIG() { return getToken(ASASPTimeNSAParser.CONFIG, 0); }
		public List<TerminalNode> SPACE() { return getTokens(ASASPTimeNSAParser.SPACE); }
		public List<TerminalNode> INT() { return getTokens(ASASPTimeNSAParser.INT); }
		public ConfigContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_config; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterConfig(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitConfig(this);
		}
	}

	public final ConfigContext config() throws RecognitionException {
		ConfigContext _localctx = new ConfigContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_config);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33); match(CONFIG);
			setState(34); match(SPACE);
			setState(35); ((ConfigContext)_localctx).r = match(INT);
			setState(36); match(SPACE);
			setState(37); ((ConfigContext)_localctx).t = match(INT);

					claimedRoles = new Integer((((ConfigContext)_localctx).r!=null?((ConfigContext)_localctx).r.getText():null));
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryContext extends ParserRuleContext {
		public Token role_ref;
		public Token timeslot_ref;
		public TerminalNode INT(int i) {
			return getToken(ASASPTimeNSAParser.INT, i);
		}
		public TerminalNode SPACE(int i) {
			return getToken(ASASPTimeNSAParser.SPACE, i);
		}
		public TerminalNode GOAL() { return getToken(ASASPTimeNSAParser.GOAL, 0); }
		public List<TerminalNode> SPACE() { return getTokens(ASASPTimeNSAParser.SPACE); }
		public List<TerminalNode> INT() { return getTokens(ASASPTimeNSAParser.INT); }
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitQuery(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40); match(GOAL);
			setState(41); match(SPACE);
			setState(42); ((QueryContext)_localctx).role_ref = match(INT);
			setState(43); match(SPACE);
			setState(44); ((QueryContext)_localctx).timeslot_ref = match(INT);

					mohawkT.query._timeslot = new TimeSlot(new Integer((((QueryContext)_localctx).timeslot_ref!=null?((QueryContext)_localctx).timeslot_ref.getText():null)));
					mohawkT.query._roles.add(new Role("role" + (((QueryContext)_localctx).role_ref!=null?((QueryContext)_localctx).role_ref.getText():null)));
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MyruleContext extends ParserRuleContext {
		public CanenableContext canenable() {
			return getRuleContext(CanenableContext.class,0);
		}
		public CandisableContext candisable() {
			return getRuleContext(CandisableContext.class,0);
		}
		public CanrevokeContext canrevoke() {
			return getRuleContext(CanrevokeContext.class,0);
		}
		public CanassignContext canassign() {
			return getRuleContext(CanassignContext.class,0);
		}
		public MyruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_myrule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterMyrule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitMyrule(this);
		}
	}

	public final MyruleContext myrule() throws RecognitionException {
		MyruleContext _localctx = new MyruleContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_myrule);
		try {
			setState(51);
			switch (_input.LA(1)) {
			case CANASSIGN:
				enterOuterAlt(_localctx, 1);
				{
				setState(47); canassign();
				}
				break;
			case CANREVOKE:
				enterOuterAlt(_localctx, 2);
				{
				setState(48); canrevoke();
				}
				break;
			case CANENABLE:
				enterOuterAlt(_localctx, 3);
				{
				setState(49); canenable();
				}
				break;
			case CANDISABLE:
				enterOuterAlt(_localctx, 4);
				{
				setState(50); candisable();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CanassignContext extends ParserRuleContext {
		public MyroleAContext arole_ref;
		public TimeslotContext atimeslot_ref;
		public PreconditionContext precond;
		public TimeslotContext timeslot_ref;
		public MyroleContext role_ref;
		public PreconditionContext precondition() {
			return getRuleContext(PreconditionContext.class,0);
		}
		public List<TerminalNode> SEP() { return getTokens(ASASPTimeNSAParser.SEP); }
		public MyroleAContext myroleA() {
			return getRuleContext(MyroleAContext.class,0);
		}
		public TerminalNode CANASSIGN() { return getToken(ASASPTimeNSAParser.CANASSIGN, 0); }
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public TerminalNode SEP2() { return getToken(ASASPTimeNSAParser.SEP2, 0); }
		public TimeslotContext timeslot(int i) {
			return getRuleContext(TimeslotContext.class,i);
		}
		public TerminalNode SPACE() { return getToken(ASASPTimeNSAParser.SPACE, 0); }
		public TerminalNode SEP(int i) {
			return getToken(ASASPTimeNSAParser.SEP, i);
		}
		public List<TimeslotContext> timeslot() {
			return getRuleContexts(TimeslotContext.class);
		}
		public CanassignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_canassign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterCanassign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitCanassign(this);
		}
	}

	public final CanassignContext canassign() throws RecognitionException {
		CanassignContext _localctx = new CanassignContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_canassign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53); match(CANASSIGN);
			setState(54); match(SPACE);
			setState(55); ((CanassignContext)_localctx).arole_ref = myroleA();
			setState(56); match(SEP);
			setState(57); ((CanassignContext)_localctx).atimeslot_ref = timeslot();
			setState(58); match(SEP);
			setState(59); ((CanassignContext)_localctx).precond = precondition();
			setState(60); match(SEP2);
			setState(61); ((CanassignContext)_localctx).timeslot_ref = timeslot();
			setState(62); match(SEP);
			setState(63); ((CanassignContext)_localctx).role_ref = myrole();

					Rule r = new Rule();
					r._type = RuleType.ASSIGN;
					r._adminRole = ((CanassignContext)_localctx).arole_ref.role;
					r._adminTimeInterval = ((CanassignContext)_localctx).atimeslot_ref.ts;
					r._preconditions = ((CanassignContext)_localctx).precond.p;
					r._roleSchedule.add(((CanassignContext)_localctx).timeslot_ref.ts);
					r._role = ((CanassignContext)_localctx).role_ref.role;
					
					mohawkT.canAssign.addRule(r);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CanrevokeContext extends ParserRuleContext {
		public MyroleAContext arole_ref;
		public TimeslotContext atimeslot_ref;
		public PreconditionContext precond;
		public TimeslotContext timeslot_ref;
		public MyroleContext role_ref;
		public PreconditionContext precondition() {
			return getRuleContext(PreconditionContext.class,0);
		}
		public List<TerminalNode> SEP() { return getTokens(ASASPTimeNSAParser.SEP); }
		public MyroleAContext myroleA() {
			return getRuleContext(MyroleAContext.class,0);
		}
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public TerminalNode SEP2() { return getToken(ASASPTimeNSAParser.SEP2, 0); }
		public TimeslotContext timeslot(int i) {
			return getRuleContext(TimeslotContext.class,i);
		}
		public TerminalNode SPACE() { return getToken(ASASPTimeNSAParser.SPACE, 0); }
		public TerminalNode SEP(int i) {
			return getToken(ASASPTimeNSAParser.SEP, i);
		}
		public TerminalNode CANREVOKE() { return getToken(ASASPTimeNSAParser.CANREVOKE, 0); }
		public List<TimeslotContext> timeslot() {
			return getRuleContexts(TimeslotContext.class);
		}
		public CanrevokeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_canrevoke; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterCanrevoke(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitCanrevoke(this);
		}
	}

	public final CanrevokeContext canrevoke() throws RecognitionException {
		CanrevokeContext _localctx = new CanrevokeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_canrevoke);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66); match(CANREVOKE);
			setState(67); match(SPACE);
			setState(68); ((CanrevokeContext)_localctx).arole_ref = myroleA();
			setState(69); match(SEP);
			setState(70); ((CanrevokeContext)_localctx).atimeslot_ref = timeslot();
			setState(71); match(SEP);
			setState(72); ((CanrevokeContext)_localctx).precond = precondition();
			setState(73); match(SEP2);
			setState(74); ((CanrevokeContext)_localctx).timeslot_ref = timeslot();
			setState(75); match(SEP);
			setState(76); ((CanrevokeContext)_localctx).role_ref = myrole();

					Rule r = new Rule();
					r._type = RuleType.REVOKE;
					r._adminRole = ((CanrevokeContext)_localctx).arole_ref.role;
					r._adminTimeInterval = ((CanrevokeContext)_localctx).atimeslot_ref.ts;
					r._preconditions = ((CanrevokeContext)_localctx).precond.p;
					r._roleSchedule.add(((CanrevokeContext)_localctx).timeslot_ref.ts);
					r._role = ((CanrevokeContext)_localctx).role_ref.role;
					
					mohawkT.canRevoke.addRule(r);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CanenableContext extends ParserRuleContext {
		public MyroleAContext arole_ref;
		public TimeslotContext atimeslot_ref;
		public PreconditionContext precond;
		public TimeslotContext timeslot_ref;
		public MyroleContext role_ref;
		public TerminalNode CANENABLE() { return getToken(ASASPTimeNSAParser.CANENABLE, 0); }
		public PreconditionContext precondition() {
			return getRuleContext(PreconditionContext.class,0);
		}
		public List<TerminalNode> SEP() { return getTokens(ASASPTimeNSAParser.SEP); }
		public MyroleAContext myroleA() {
			return getRuleContext(MyroleAContext.class,0);
		}
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public TerminalNode SEP2() { return getToken(ASASPTimeNSAParser.SEP2, 0); }
		public TimeslotContext timeslot(int i) {
			return getRuleContext(TimeslotContext.class,i);
		}
		public TerminalNode SPACE() { return getToken(ASASPTimeNSAParser.SPACE, 0); }
		public TerminalNode SEP(int i) {
			return getToken(ASASPTimeNSAParser.SEP, i);
		}
		public List<TimeslotContext> timeslot() {
			return getRuleContexts(TimeslotContext.class);
		}
		public CanenableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_canenable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterCanenable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitCanenable(this);
		}
	}

	public final CanenableContext canenable() throws RecognitionException {
		CanenableContext _localctx = new CanenableContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_canenable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79); match(CANENABLE);
			setState(80); match(SPACE);
			setState(81); ((CanenableContext)_localctx).arole_ref = myroleA();
			setState(82); match(SEP);
			setState(83); ((CanenableContext)_localctx).atimeslot_ref = timeslot();
			setState(84); match(SEP);
			setState(85); ((CanenableContext)_localctx).precond = precondition();
			setState(86); match(SEP2);
			setState(87); ((CanenableContext)_localctx).timeslot_ref = timeslot();
			setState(88); match(SEP);
			setState(89); ((CanenableContext)_localctx).role_ref = myrole();

					Rule r = new Rule();
					r._type = RuleType.ENABLE;
					r._adminRole = ((CanenableContext)_localctx).arole_ref.role;
					r._adminTimeInterval = ((CanenableContext)_localctx).atimeslot_ref.ts;
					r._preconditions = ((CanenableContext)_localctx).precond.p;
					r._roleSchedule.add(((CanenableContext)_localctx).timeslot_ref.ts);
					r._role = ((CanenableContext)_localctx).role_ref.role;
					
					mohawkT.canEnable.addRule(r);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CandisableContext extends ParserRuleContext {
		public MyroleAContext arole_ref;
		public TimeslotContext atimeslot_ref;
		public PreconditionContext precond;
		public TimeslotContext timeslot_ref;
		public MyroleContext role_ref;
		public PreconditionContext precondition() {
			return getRuleContext(PreconditionContext.class,0);
		}
		public List<TerminalNode> SEP() { return getTokens(ASASPTimeNSAParser.SEP); }
		public MyroleAContext myroleA() {
			return getRuleContext(MyroleAContext.class,0);
		}
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public TerminalNode SEP2() { return getToken(ASASPTimeNSAParser.SEP2, 0); }
		public TimeslotContext timeslot(int i) {
			return getRuleContext(TimeslotContext.class,i);
		}
		public TerminalNode CANDISABLE() { return getToken(ASASPTimeNSAParser.CANDISABLE, 0); }
		public TerminalNode SPACE() { return getToken(ASASPTimeNSAParser.SPACE, 0); }
		public TerminalNode SEP(int i) {
			return getToken(ASASPTimeNSAParser.SEP, i);
		}
		public List<TimeslotContext> timeslot() {
			return getRuleContexts(TimeslotContext.class);
		}
		public CandisableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_candisable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterCandisable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitCandisable(this);
		}
	}

	public final CandisableContext candisable() throws RecognitionException {
		CandisableContext _localctx = new CandisableContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_candisable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92); match(CANDISABLE);
			setState(93); match(SPACE);
			setState(94); ((CandisableContext)_localctx).arole_ref = myroleA();
			setState(95); match(SEP);
			setState(96); ((CandisableContext)_localctx).atimeslot_ref = timeslot();
			setState(97); match(SEP);
			setState(98); ((CandisableContext)_localctx).precond = precondition();
			setState(99); match(SEP2);
			setState(100); ((CandisableContext)_localctx).timeslot_ref = timeslot();
			setState(101); match(SEP);
			setState(102); ((CandisableContext)_localctx).role_ref = myrole();

					Rule r = new Rule();
					r._type = RuleType.DISABLE;
					r._adminRole = ((CandisableContext)_localctx).arole_ref.role;
					r._adminTimeInterval = ((CandisableContext)_localctx).atimeslot_ref.ts;
					r._preconditions = ((CandisableContext)_localctx).precond.p;
					r._roleSchedule.add(((CandisableContext)_localctx).timeslot_ref.ts);
					r._role = ((CandisableContext)_localctx).role_ref.role;
					
					mohawkT.canDisable.addRule(r);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoleconditionContext extends ParserRuleContext {
		public Role role;
		public MyroleContext r;
		public TerminalNode NEG() { return getToken(ASASPTimeNSAParser.NEG, 0); }
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public RoleconditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rolecondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterRolecondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitRolecondition(this);
		}
	}

	public final RoleconditionContext rolecondition() throws RecognitionException {
		RoleconditionContext _localctx = new RoleconditionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rolecondition);

			boolean not = false;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			_la = _input.LA(1);
			if (_la==NEG) {
				{
				setState(105); match(NEG);
				not = true;
				}
			}

			setState(109); ((RoleconditionContext)_localctx).r = myrole();

					((RoleconditionContext)_localctx).role =  new Role(((RoleconditionContext)_localctx).r.role);
					_localctx.role._not = not;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MyroleContext extends ParserRuleContext {
		public Role role;
		public Token a;
		public TerminalNode INT() { return getToken(ASASPTimeNSAParser.INT, 0); }
		public MyroleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_myrole; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterMyrole(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitMyrole(this);
		}
	}

	public final MyroleContext myrole() throws RecognitionException {
		MyroleContext _localctx = new MyroleContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_myrole);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112); ((MyroleContext)_localctx).a = match(INT);

					((MyroleContext)_localctx).role =  new Role("role" + (((MyroleContext)_localctx).a!=null?((MyroleContext)_localctx).a.getText():null));
					mohawkT.roleHelper.add(_localctx.role);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MyroleAContext extends ParserRuleContext {
		public Role role;
		public MyroleContext a;
		public TerminalNode TRUE() { return getToken(ASASPTimeNSAParser.TRUE, 0); }
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public MyroleAContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_myroleA; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterMyroleA(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitMyroleA(this);
		}
	}

	public final MyroleAContext myroleA() throws RecognitionException {
		MyroleAContext _localctx = new MyroleAContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_myroleA);
		try {
			setState(120);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(115); ((MyroleAContext)_localctx).a = myrole();

						((MyroleAContext)_localctx).role =  ((MyroleAContext)_localctx).a.role;
					
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 2);
				{
				setState(118); match(TRUE);

						((MyroleAContext)_localctx).role =  Role.allRoles();
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeslotContext extends ParserRuleContext {
		public TimeSlot ts;
		public Token t;
		public TerminalNode INT() { return getToken(ASASPTimeNSAParser.INT, 0); }
		public TimeslotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeslot; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterTimeslot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitTimeslot(this);
		}
	}

	public final TimeslotContext timeslot() throws RecognitionException {
		TimeslotContext _localctx = new TimeslotContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_timeslot);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122); match(T__0);
			setState(123); ((TimeslotContext)_localctx).t = match(INT);

					((TimeslotContext)_localctx).ts =  new TimeSlot(new Integer((((TimeslotContext)_localctx).t!=null?((TimeslotContext)_localctx).t.getText():null)));
					mohawkT.timeIntervalHelper.add(_localctx.ts);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreconditionContext extends ParserRuleContext {
		public ArrayList<Role> p;
		public RoleconditionContext a;
		public RoleconditionContext b;
		public List<RoleconditionContext> rolecondition() {
			return getRuleContexts(RoleconditionContext.class);
		}
		public RoleconditionContext rolecondition(int i) {
			return getRuleContext(RoleconditionContext.class,i);
		}
		public TerminalNode TRUE() { return getToken(ASASPTimeNSAParser.TRUE, 0); }
		public TerminalNode SPACE(int i) {
			return getToken(ASASPTimeNSAParser.SPACE, i);
		}
		public List<TerminalNode> SPACE() { return getTokens(ASASPTimeNSAParser.SPACE); }
		public PreconditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).enterPrecondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASASPTimeNSAListener ) ((ASASPTimeNSAListener)listener).exitPrecondition(this);
		}
	}

	public final PreconditionContext precondition() throws RecognitionException {
		PreconditionContext _localctx = new PreconditionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_precondition);

			((PreconditionContext)_localctx).p =  new ArrayList<Role>();

		int _la;
		try {
			setState(138);
			switch (_input.LA(1)) {
			case NEG:
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(126); ((PreconditionContext)_localctx).a = rolecondition();

						_localctx.p.add(((PreconditionContext)_localctx).a.role);
					
				setState(134);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==SPACE) {
					{
					{
					setState(128); match(SPACE);
					setState(129); ((PreconditionContext)_localctx).b = rolecondition();

								_localctx.p.add(((PreconditionContext)_localctx).b.role);
							
					}
					}
					setState(136);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 2);
				{
				setState(137); match(TRUE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\21\u008f\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\6\2 \n\2\r\2\16\2!\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\5\5\66"+
		"\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\n\3\n\5\nn\n\n\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\5\f"+
		"{\n\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u0087\n\16\f"+
		"\16\16\16\u008a\13\16\3\16\5\16\u008d\n\16\3\16\2\2\17\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\2\2\u0089\2\34\3\2\2\2\4#\3\2\2\2\6*\3\2\2\2\b\65\3"+
		"\2\2\2\n\67\3\2\2\2\fD\3\2\2\2\16Q\3\2\2\2\20^\3\2\2\2\22m\3\2\2\2\24"+
		"r\3\2\2\2\26z\3\2\2\2\30|\3\2\2\2\32\u008c\3\2\2\2\34\35\5\4\3\2\35\37"+
		"\5\6\4\2\36 \5\b\5\2\37\36\3\2\2\2 !\3\2\2\2!\37\3\2\2\2!\"\3\2\2\2\""+
		"\3\3\2\2\2#$\7\5\2\2$%\7\16\2\2%&\7\17\2\2&\'\7\16\2\2\'(\7\17\2\2()\b"+
		"\3\1\2)\5\3\2\2\2*+\7\6\2\2+,\7\16\2\2,-\7\17\2\2-.\7\16\2\2./\7\17\2"+
		"\2/\60\b\4\1\2\60\7\3\2\2\2\61\66\5\n\6\2\62\66\5\f\7\2\63\66\5\16\b\2"+
		"\64\66\5\20\t\2\65\61\3\2\2\2\65\62\3\2\2\2\65\63\3\2\2\2\65\64\3\2\2"+
		"\2\66\t\3\2\2\2\678\7\7\2\289\7\16\2\29:\5\26\f\2:;\7\f\2\2;<\5\30\r\2"+
		"<=\7\f\2\2=>\5\32\16\2>?\7\r\2\2?@\5\30\r\2@A\7\f\2\2AB\5\24\13\2BC\b"+
		"\6\1\2C\13\3\2\2\2DE\7\b\2\2EF\7\16\2\2FG\5\26\f\2GH\7\f\2\2HI\5\30\r"+
		"\2IJ\7\f\2\2JK\5\32\16\2KL\7\r\2\2LM\5\30\r\2MN\7\f\2\2NO\5\24\13\2OP"+
		"\b\7\1\2P\r\3\2\2\2QR\7\t\2\2RS\7\16\2\2ST\5\26\f\2TU\7\f\2\2UV\5\30\r"+
		"\2VW\7\f\2\2WX\5\32\16\2XY\7\r\2\2YZ\5\30\r\2Z[\7\f\2\2[\\\5\24\13\2\\"+
		"]\b\b\1\2]\17\3\2\2\2^_\7\n\2\2_`\7\16\2\2`a\5\26\f\2ab\7\f\2\2bc\5\30"+
		"\r\2cd\7\f\2\2de\5\32\16\2ef\7\r\2\2fg\5\30\r\2gh\7\f\2\2hi\5\24\13\2"+
		"ij\b\t\1\2j\21\3\2\2\2kl\7\13\2\2ln\b\n\1\2mk\3\2\2\2mn\3\2\2\2no\3\2"+
		"\2\2op\5\24\13\2pq\b\n\1\2q\23\3\2\2\2rs\7\17\2\2st\b\13\1\2t\25\3\2\2"+
		"\2uv\5\24\13\2vw\b\f\1\2w{\3\2\2\2xy\7\4\2\2y{\b\f\1\2zu\3\2\2\2zx\3\2"+
		"\2\2{\27\3\2\2\2|}\7\3\2\2}~\7\17\2\2~\177\b\r\1\2\177\31\3\2\2\2\u0080"+
		"\u0081\5\22\n\2\u0081\u0088\b\16\1\2\u0082\u0083\7\16\2\2\u0083\u0084"+
		"\5\22\n\2\u0084\u0085\b\16\1\2\u0085\u0087\3\2\2\2\u0086\u0082\3\2\2\2"+
		"\u0087\u008a\3\2\2\2\u0088\u0086\3\2\2\2\u0088\u0089\3\2\2\2\u0089\u008d"+
		"\3\2\2\2\u008a\u0088\3\2\2\2\u008b\u008d\7\4\2\2\u008c\u0080\3\2\2\2\u008c"+
		"\u008b\3\2\2\2\u008d\33\3\2\2\2\b!\65mz\u0088\u008c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}