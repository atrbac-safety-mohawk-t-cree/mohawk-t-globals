// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\asasptime\sa\ASASPTimeSA.g4 by ANTLR 4.4

package mohawk.global.parser.asasptime.sa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ASASPTimeSAParser}.
 */
public interface ASASPTimeSAListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#init}.
	 * @param ctx the parse tree
	 */
	void enterInit(@NotNull ASASPTimeSAParser.InitContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#init}.
	 * @param ctx the parse tree
	 */
	void exitInit(@NotNull ASASPTimeSAParser.InitContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#canrevoke}.
	 * @param ctx the parse tree
	 */
	void enterCanrevoke(@NotNull ASASPTimeSAParser.CanrevokeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#canrevoke}.
	 * @param ctx the parse tree
	 */
	void exitCanrevoke(@NotNull ASASPTimeSAParser.CanrevokeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#myrule}.
	 * @param ctx the parse tree
	 */
	void enterMyrule(@NotNull ASASPTimeSAParser.MyruleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#myrule}.
	 * @param ctx the parse tree
	 */
	void exitMyrule(@NotNull ASASPTimeSAParser.MyruleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(@NotNull ASASPTimeSAParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(@NotNull ASASPTimeSAParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#timeslot}.
	 * @param ctx the parse tree
	 */
	void enterTimeslot(@NotNull ASASPTimeSAParser.TimeslotContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#timeslot}.
	 * @param ctx the parse tree
	 */
	void exitTimeslot(@NotNull ASASPTimeSAParser.TimeslotContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#canassign}.
	 * @param ctx the parse tree
	 */
	void enterCanassign(@NotNull ASASPTimeSAParser.CanassignContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#canassign}.
	 * @param ctx the parse tree
	 */
	void exitCanassign(@NotNull ASASPTimeSAParser.CanassignContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#rolecondition}.
	 * @param ctx the parse tree
	 */
	void enterRolecondition(@NotNull ASASPTimeSAParser.RoleconditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#rolecondition}.
	 * @param ctx the parse tree
	 */
	void exitRolecondition(@NotNull ASASPTimeSAParser.RoleconditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#precondition}.
	 * @param ctx the parse tree
	 */
	void enterPrecondition(@NotNull ASASPTimeSAParser.PreconditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#precondition}.
	 * @param ctx the parse tree
	 */
	void exitPrecondition(@NotNull ASASPTimeSAParser.PreconditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#myrole}.
	 * @param ctx the parse tree
	 */
	void enterMyrole(@NotNull ASASPTimeSAParser.MyroleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#myrole}.
	 * @param ctx the parse tree
	 */
	void exitMyrole(@NotNull ASASPTimeSAParser.MyroleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ASASPTimeSAParser#config}.
	 * @param ctx the parse tree
	 */
	void enterConfig(@NotNull ASASPTimeSAParser.ConfigContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASASPTimeSAParser#config}.
	 * @param ctx the parse tree
	 */
	void exitConfig(@NotNull ASASPTimeSAParser.ConfigContext ctx);
}