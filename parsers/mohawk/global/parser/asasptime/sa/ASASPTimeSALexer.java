// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\asasptime\sa\ASASPTimeSA.g4 by ANTLR 4.4

package mohawk.global.parser.asasptime.sa;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ASASPTimeSALexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, TRUE=2, CONFIG=3, GOAL=4, CANASSIGN=5, CANREVOKE=6, NEG=7, SEP=8, 
		SEP2=9, SPACE=10, INT=11, WS=12, NL=13;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'"
	};
	public static final String[] ruleNames = {
		"T__0", "TRUE", "CONFIG", "GOAL", "CANASSIGN", "CANREVOKE", "NEG", "SEP", 
		"SEP2", "SPACE", "INT", "DIGIT", "WS", "NL"
	};


		public final Logger logger = Logger.getLogger("mohawk");
		
		/* Global States */
		int tabsize = 1;
		public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: ASAPTime SA");
		public Integer claimedRoles = null;
		
		
		private void logmsg(String msg) {
			if(logger.getLevel() == Level.FINE) {
				System.out.println(StringUtils.repeat("  ", tabsize) + msg);
			}
		}


	public ASASPTimeSALexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "ASASPTimeSA.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\17l\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3"+
		"\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\6\13T\n\13\r\13\16\13U\3\f\6\f"+
		"Y\n\f\r\f\16\fZ\3\r\3\r\3\16\6\16`\n\16\r\16\16\16a\3\16\3\16\3\17\5\17"+
		"g\n\17\3\17\3\17\3\17\3\17\2\2\20\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23"+
		"\13\25\f\27\r\31\2\33\16\35\17\3\2\5\3\2\"\"\3\2\62;\4\2\13\13\16\16n"+
		"\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2"+
		"\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2"+
		"\2\33\3\2\2\2\2\35\3\2\2\2\3\37\3\2\2\2\5!\3\2\2\2\7&\3\2\2\2\t-\3\2\2"+
		"\2\13\62\3\2\2\2\r=\3\2\2\2\17H\3\2\2\2\21J\3\2\2\2\23N\3\2\2\2\25S\3"+
		"\2\2\2\27X\3\2\2\2\31\\\3\2\2\2\33_\3\2\2\2\35f\3\2\2\2\37 \7v\2\2 \4"+
		"\3\2\2\2!\"\7v\2\2\"#\7t\2\2#$\7w\2\2$%\7g\2\2%\6\3\2\2\2&\'\7E\2\2\'"+
		"(\7Q\2\2()\7P\2\2)*\7H\2\2*+\7K\2\2+,\7I\2\2,\b\3\2\2\2-.\7I\2\2./\7Q"+
		"\2\2/\60\7C\2\2\60\61\7N\2\2\61\n\3\2\2\2\62\63\7e\2\2\63\64\7c\2\2\64"+
		"\65\7p\2\2\65\66\7a\2\2\66\67\7c\2\2\678\7u\2\289\7u\2\29:\7k\2\2:;\7"+
		"i\2\2;<\7p\2\2<\f\3\2\2\2=>\7e\2\2>?\7c\2\2?@\7p\2\2@A\7a\2\2AB\7t\2\2"+
		"BC\7g\2\2CD\7x\2\2DE\7q\2\2EF\7m\2\2FG\7g\2\2G\16\3\2\2\2HI\7/\2\2I\20"+
		"\3\2\2\2JK\5\25\13\2KL\7.\2\2LM\5\25\13\2M\22\3\2\2\2NO\5\25\13\2OP\7"+
		"=\2\2PQ\5\25\13\2Q\24\3\2\2\2RT\t\2\2\2SR\3\2\2\2TU\3\2\2\2US\3\2\2\2"+
		"UV\3\2\2\2V\26\3\2\2\2WY\5\31\r\2XW\3\2\2\2YZ\3\2\2\2ZX\3\2\2\2Z[\3\2"+
		"\2\2[\30\3\2\2\2\\]\t\3\2\2]\32\3\2\2\2^`\t\4\2\2_^\3\2\2\2`a\3\2\2\2"+
		"a_\3\2\2\2ab\3\2\2\2bc\3\2\2\2cd\b\16\2\2d\34\3\2\2\2eg\7\17\2\2fe\3\2"+
		"\2\2fg\3\2\2\2gh\3\2\2\2hi\7\f\2\2ij\3\2\2\2jk\b\17\2\2k\36\3\2\2\2\7"+
		"\2UZaf\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}