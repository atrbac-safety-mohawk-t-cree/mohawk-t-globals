// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawk\v1\Mohawk.g4 by ANTLR 4.4

package mohawk.global.parser.mohawk.v1;

import java.util.Vector;
import java.util.Stack;
import java.util.HashMap;
import java.util.Map;
import mohawk.global.pieces.mohawk.PreCondProcessorInt;
import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.CREntry;
import mohawk.global.pieces.mohawk.PreCondition;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MohawkLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__8=1, T__7=2, T__6=3, T__5=4, T__4=5, T__3=6, T__2=7, T__1=8, T__0=9, 
		Whitespace=10, Newline=11, BlockComment=12, LineComment=13, ID=14, LANGLE=15, 
		RANGLE=16, COMMA=17, COND=18, NOT=19, SEMI=20;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'"
	};
	public static final String[] ruleNames = {
		"T__8", "T__7", "T__6", "T__5", "T__4", "T__3", "T__2", "T__1", "T__0", 
		"Whitespace", "Newline", "BlockComment", "LineComment", "ID", "LANGLE", 
		"RANGLE", "COMMA", "COND", "NOT", "SEMI"
	};


		public Vector<String> vRoles;
		public Vector<String> vUsers;
		public Vector<String> vAdmin;
		public Map<Integer, String> mRoleIndex;
		public Map<String, Integer> mRole2Index;
		public Map<Integer, String> mUserIndex;
		public Map<String,Vector<Integer>> mUA;
		public Map<String,Vector<CREntry>> mCR;
		public Map<String,Vector<CAEntry>> mCA;	
		public PreCondProcessorInt preCndP;
		public Stack<Integer> stackOperators;
		public Vector<String> vSpec; // This vector holds two strings - user and role that will be used in the LTL formulae
		
		// Indices for user and roles while parsing
		// Each user has an index corresponding to the order in which the name appears in the list.
		public int iRoleIndex = 0;
		public int iUserIndex = 0;
		
		// Counters
		public int numUA = 0;
		public int numCARules = 0;
		public int numCRRules = 0;
		

		public void initRbac() {
			vRoles = new Vector<String>();
			vUsers = new Vector<String>();
			vAdmin = new Vector<String>();
			mRoleIndex = new HashMap<Integer, String>();
			mRole2Index = new HashMap<String,Integer>();
			mUserIndex = new HashMap<Integer, String>();
			mUA = new HashMap<String,Vector<Integer>>();
			mCR = new HashMap<String,Vector<CREntry>>();
			mCA = new HashMap<String,Vector<CAEntry>>();
			vSpec = new Vector<String>();		
		}
		
		/*
		public RBACInstance getRBAC() {
			return new RBACInstance(vRoles, vUsers, vAdmin, mUA, mCR, mCA,vSpec);
		}
		*/
		
		public void setUA(String strUser, String strRole) {
		
			Vector<Integer> vUserUA = mUA.get(strUser);		
			if(vUserUA == null)
			{
				vUserUA = new Vector<Integer>();
				mUA.put(strUser,vUserUA);
			}
			
			int iRoleIndex = mRole2Index.get(strRole); //getMapKey(mRoleIndex, strRole);		
			vUserUA.add(iRoleIndex);
		}
		
		public void addCREntry(String inStrPreCond, String inStrRole) {
			
			CREntry crEntry = new CREntry(inStrPreCond, inStrRole);
			Vector<CREntry> vCR = mCR.get(inStrRole);
			
			if(vCR == null)
				vCR = new Vector<CREntry>();
			
			vCR.add(new CREntry(inStrPreCond, inStrRole));
			mCR.put(inStrRole,vCR);
		}
		
		public void addCAEntry(String inStrAdminRole, PreCondition pcPreCond, String inStrRole) {
			
			CAEntry caEntry = new CAEntry(inStrAdminRole, pcPreCond, inStrRole);
			Vector<CAEntry> vCA = mCA.get(inStrRole);
			
			if(vCA == null)
				vCA = new Vector<CAEntry>();
			vCA.add(caEntry);
			mCA.put(inStrRole,vCA);
		}
		/*	
		private int getMapKey(Map<Integer,String> inMap, String inString) {
			
			for(int i=0; i<inMap.size(); i++) {
				
				if(inMap.get(i).equals(inString)) {
					return i;
				}
			}
			
			System.out.println("Error - BTree::getMapIndex - Value not found in map");
			return 0;
		}
		*/
		
		public void addSpec(String inStrUser, String inStrRole) {
		
			vSpec.add(inStrUser);
			vSpec.add(inStrRole);
		}


	public MohawkLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Mohawk.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\26\u0092\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3"+
		"\13\6\13X\n\13\r\13\16\13Y\3\13\3\13\3\f\3\f\5\f`\n\f\3\f\5\fc\n\f\3\f"+
		"\3\f\3\r\3\r\3\r\3\r\7\rk\n\r\f\r\16\rn\13\r\3\r\3\r\3\r\3\r\3\r\3\16"+
		"\3\16\3\16\3\16\7\16y\n\16\f\16\16\16|\13\16\3\16\3\16\3\17\3\17\7\17"+
		"\u0082\n\17\f\17\16\17\u0085\13\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23"+
		"\3\23\3\24\3\24\3\25\3\25\3l\2\26\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23"+
		"\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26\3\2\6\4\2\13"+
		"\13\"\"\4\2\f\f\17\17\5\2C\\aac|\6\2\62;C\\aac|\u0097\2\3\3\2\2\2\2\5"+
		"\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2"+
		"\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33"+
		"\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2"+
		"\'\3\2\2\2\2)\3\2\2\2\3+\3\2\2\2\5\61\3\2\2\2\7\67\3\2\2\2\t:\3\2\2\2"+
		"\13@\3\2\2\2\rF\3\2\2\2\17I\3\2\2\2\21N\3\2\2\2\23S\3\2\2\2\25W\3\2\2"+
		"\2\27b\3\2\2\2\31f\3\2\2\2\33t\3\2\2\2\35\177\3\2\2\2\37\u0086\3\2\2\2"+
		"!\u0088\3\2\2\2#\u008a\3\2\2\2%\u008c\3\2\2\2\'\u008e\3\2\2\2)\u0090\3"+
		"\2\2\2+,\7C\2\2,-\7F\2\2-.\7O\2\2./\7K\2\2/\60\7P\2\2\60\4\3\2\2\2\61"+
		"\62\7T\2\2\62\63\7q\2\2\63\64\7n\2\2\64\65\7g\2\2\65\66\7u\2\2\66\6\3"+
		"\2\2\2\678\7W\2\289\7C\2\29\b\3\2\2\2:;\7H\2\2;<\7C\2\2<=\7N\2\2=>\7U"+
		"\2\2>?\7G\2\2?\n\3\2\2\2@A\7W\2\2AB\7u\2\2BC\7g\2\2CD\7t\2\2DE\7u\2\2"+
		"E\f\3\2\2\2FG\7E\2\2GH\7T\2\2H\16\3\2\2\2IJ\7U\2\2JK\7R\2\2KL\7G\2\2L"+
		"M\7E\2\2M\20\3\2\2\2NO\7V\2\2OP\7T\2\2PQ\7W\2\2QR\7G\2\2R\22\3\2\2\2S"+
		"T\7E\2\2TU\7C\2\2U\24\3\2\2\2VX\t\2\2\2WV\3\2\2\2XY\3\2\2\2YW\3\2\2\2"+
		"YZ\3\2\2\2Z[\3\2\2\2[\\\b\13\2\2\\\26\3\2\2\2]_\7\17\2\2^`\7\f\2\2_^\3"+
		"\2\2\2_`\3\2\2\2`c\3\2\2\2ac\7\f\2\2b]\3\2\2\2ba\3\2\2\2cd\3\2\2\2de\b"+
		"\f\2\2e\30\3\2\2\2fg\7\61\2\2gh\7,\2\2hl\3\2\2\2ik\13\2\2\2ji\3\2\2\2"+
		"kn\3\2\2\2lm\3\2\2\2lj\3\2\2\2mo\3\2\2\2nl\3\2\2\2op\7,\2\2pq\7\61\2\2"+
		"qr\3\2\2\2rs\b\r\2\2s\32\3\2\2\2tu\7\61\2\2uv\7\61\2\2vz\3\2\2\2wy\n\3"+
		"\2\2xw\3\2\2\2y|\3\2\2\2zx\3\2\2\2z{\3\2\2\2{}\3\2\2\2|z\3\2\2\2}~\b\16"+
		"\2\2~\34\3\2\2\2\177\u0083\t\4\2\2\u0080\u0082\t\5\2\2\u0081\u0080\3\2"+
		"\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084"+
		"\36\3\2\2\2\u0085\u0083\3\2\2\2\u0086\u0087\7>\2\2\u0087 \3\2\2\2\u0088"+
		"\u0089\7@\2\2\u0089\"\3\2\2\2\u008a\u008b\7.\2\2\u008b$\3\2\2\2\u008c"+
		"\u008d\7(\2\2\u008d&\3\2\2\2\u008e\u008f\7/\2\2\u008f(\3\2\2\2\u0090\u0091"+
		"\7=\2\2\u0091*\3\2\2\2\t\2Y_blz\u0083\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}