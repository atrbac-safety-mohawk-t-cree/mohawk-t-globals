grammar Mohawkv2;

/* Convert Mohawk testcase into the Mohawk+T internal format */

/* Code */
@header {
package mohawk.global.parser.mohawk.v2;

import java.util.*;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;
}

@members {
	public final Logger logger = Logger.getLogger("mohawk");

	int tabsize = 1;
	
	/* Global States */
	public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: Mohawk");
	public Integer claimedRoles = null;
	
	
	public Rule simpleRule(RuleType ruleType, Role role) {
		Rule r = new Rule(ruleType, Role.allRoles(), new TimeInterval(0), 
			new ArrayList<Role>(), new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), 
			role);
		return r;
	}
	
	private void logmsg(String msg) {
		if(logger.getLevel() == Level.FINE) {
			System.out.println(StringUtils.repeat("  ", tabsize) + msg);
		}
	}
}

/* Rules */
init
:
	roles users ua cr ca admin spec
;

roles
:
	'Roles'
	(
		ID
		{
			if(claimedRoles == null) {
				claimedRoles = 0;
			}
			claimedRoles++;
		}
	)+ SEMI
;

users
:
	(
		'Users'
	)
	(
		u = ID
	)+ SEMI
;

ua
:
	'UA'
	(
		LANGLE x = ID COMMA y = ID RANGLE
	)+ SEMI
;

ca
:
	'CA'
	(
		caentry
	)* SEMI
;

caentry
:
	LANGLE d = ID COMMA e = precondition COMMA f = myrole RANGLE
	{
		Rule r = simpleRule(RuleType.ASSIGN, $f.role);
		r._preconditions = $e.p;
		mohawkT.canAssign.addRule(r);
	}

;

precondition returns [ArrayList<Role> p] @init {
	$p = new ArrayList<Role>();
}
:
	a = rolecondition
	{
		$p.add($a.role);
	}

	(
		COND b = rolecondition
		{
			$p.add($b.role);
		}

	)*
	| 'TRUE'
;

rolecondition returns [Role role] @init {
	boolean not = false;
}
:
	(
		NOT
		{not = true;}

	)? r = myrole
	{
		$role = new Role($r.role);
		$role._not = not;
	}

;

cr
:
	'CR'
	(
		crentry
	)* SEMI
;

crentry
:
	LANGLE mm = ID COMMA nn = myrole RANGLE
	{
		Rule r = simpleRule(RuleType.REVOKE, $nn.role);
		mohawkT.canRevoke.addRule(r);
	}

	| LANGLE 'FALSE' COMMA ID RANGLE
	{
		logmsg("Skipping CR entry as the admin is 'FALSE'");
	}

;

admin
:
	'ADMIN'
	(
		ID
	)+ SEMI
;

spec
:
	'SPEC' su = ID sr = myrole SEMI
	{
		mohawkT.query._timeslot = new TimeSlot(0);
		mohawkT.query._roles.add($sr.role);
		
		mohawkT.timeIntervalHelper.add(mohawkT.query._timeslot);
	}

;

myrole returns [Role role]
:
	a = ID
	{
		$role = new Role($a.text);
		mohawkT.roleHelper.add($role);
	}

;

/* TOKENS */
ID
:
	(
		'a' .. 'z'
		| 'A' .. 'Z'
		| '_'
	)
	(
		'a' .. 'z'
		| 'A' .. 'Z'
		| '0' .. '9'
		| '_'
	)*
;

LANGLE
:
	'<'
;

RANGLE
:
	'>'
;

COMMA
:
	','
;

COND
:
	'&'
;

NOT
:
	'-'
;

SEMI
:
	';'
;

COMMENT
:
	'//'
	(
		~( '\n' | '\r' )
	)*
	(
		'\r'
	)? '\n' -> skip
;

WS
:
	[ \t\f]+ -> skip
;

NL
:
	'\r'? '\n' -> skip
;