// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawk\v2\Mohawkv2.g4 by ANTLR 4.4

package mohawk.global.parser.mohawk.v2;

import java.util.*;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class Mohawkv2Parser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__8=1, T__7=2, T__6=3, T__5=4, T__4=5, T__3=6, T__2=7, T__1=8, T__0=9, 
		ID=10, LANGLE=11, RANGLE=12, COMMA=13, COND=14, NOT=15, SEMI=16, COMMENT=17, 
		WS=18, NL=19;
	public static final String[] tokenNames = {
		"<INVALID>", "'ADMIN'", "'Roles'", "'UA'", "'FALSE'", "'Users'", "'CR'", 
		"'SPEC'", "'TRUE'", "'CA'", "ID", "'<'", "'>'", "','", "'&'", "'-'", "';'", 
		"COMMENT", "WS", "NL"
	};
	public static final int
		RULE_init = 0, RULE_roles = 1, RULE_users = 2, RULE_ua = 3, RULE_ca = 4, 
		RULE_caentry = 5, RULE_precondition = 6, RULE_rolecondition = 7, RULE_cr = 8, 
		RULE_crentry = 9, RULE_admin = 10, RULE_spec = 11, RULE_myrole = 12;
	public static final String[] ruleNames = {
		"init", "roles", "users", "ua", "ca", "caentry", "precondition", "rolecondition", 
		"cr", "crentry", "admin", "spec", "myrole"
	};

	@Override
	public String getGrammarFileName() { return "Mohawkv2.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		public final Logger logger = Logger.getLogger("mohawk");

		int tabsize = 1;
		
		/* Global States */
		public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: Mohawk");
		public Integer claimedRoles = null;
		
		
		public Rule simpleRule(RuleType ruleType, Role role) {
			Rule r = new Rule(ruleType, Role.allRoles(), new TimeInterval(0), 
				new ArrayList<Role>(), new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), 
				role);
			return r;
		}
		
		private void logmsg(String msg) {
			if(logger.getLevel() == Level.FINE) {
				System.out.println(StringUtils.repeat("  ", tabsize) + msg);
			}
		}

	public Mohawkv2Parser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class InitContext extends ParserRuleContext {
		public RolesContext roles() {
			return getRuleContext(RolesContext.class,0);
		}
		public UaContext ua() {
			return getRuleContext(UaContext.class,0);
		}
		public AdminContext admin() {
			return getRuleContext(AdminContext.class,0);
		}
		public UsersContext users() {
			return getRuleContext(UsersContext.class,0);
		}
		public SpecContext spec() {
			return getRuleContext(SpecContext.class,0);
		}
		public CaContext ca() {
			return getRuleContext(CaContext.class,0);
		}
		public CrContext cr() {
			return getRuleContext(CrContext.class,0);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitInit(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_init);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26); roles();
			setState(27); users();
			setState(28); ua();
			setState(29); cr();
			setState(30); ca();
			setState(31); admin();
			setState(32); spec();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RolesContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(Mohawkv2Parser.ID); }
		public TerminalNode SEMI() { return getToken(Mohawkv2Parser.SEMI, 0); }
		public TerminalNode ID(int i) {
			return getToken(Mohawkv2Parser.ID, i);
		}
		public RolesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_roles; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterRoles(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitRoles(this);
		}
	}

	public final RolesContext roles() throws RecognitionException {
		RolesContext _localctx = new RolesContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_roles);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34); match(T__7);
			setState(37); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(35); match(ID);

							if(claimedRoles == null) {
								claimedRoles = 0;
							}
							claimedRoles++;
						
				}
				}
				setState(39); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			setState(41); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UsersContext extends ParserRuleContext {
		public Token u;
		public List<TerminalNode> ID() { return getTokens(Mohawkv2Parser.ID); }
		public TerminalNode SEMI() { return getToken(Mohawkv2Parser.SEMI, 0); }
		public TerminalNode ID(int i) {
			return getToken(Mohawkv2Parser.ID, i);
		}
		public UsersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_users; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterUsers(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitUsers(this);
		}
	}

	public final UsersContext users() throws RecognitionException {
		UsersContext _localctx = new UsersContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_users);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(43); match(T__4);
			}
			setState(45); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(44); ((UsersContext)_localctx).u = match(ID);
				}
				}
				setState(47); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			setState(49); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UaContext extends ParserRuleContext {
		public Token x;
		public Token y;
		public List<TerminalNode> LANGLE() { return getTokens(Mohawkv2Parser.LANGLE); }
		public List<TerminalNode> ID() { return getTokens(Mohawkv2Parser.ID); }
		public List<TerminalNode> RANGLE() { return getTokens(Mohawkv2Parser.RANGLE); }
		public TerminalNode SEMI() { return getToken(Mohawkv2Parser.SEMI, 0); }
		public TerminalNode LANGLE(int i) {
			return getToken(Mohawkv2Parser.LANGLE, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(Mohawkv2Parser.COMMA); }
		public TerminalNode RANGLE(int i) {
			return getToken(Mohawkv2Parser.RANGLE, i);
		}
		public TerminalNode ID(int i) {
			return getToken(Mohawkv2Parser.ID, i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(Mohawkv2Parser.COMMA, i);
		}
		public UaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ua; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterUa(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitUa(this);
		}
	}

	public final UaContext ua() throws RecognitionException {
		UaContext _localctx = new UaContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_ua);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51); match(T__6);
			setState(57); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(52); match(LANGLE);
				setState(53); ((UaContext)_localctx).x = match(ID);
				setState(54); match(COMMA);
				setState(55); ((UaContext)_localctx).y = match(ID);
				setState(56); match(RANGLE);
				}
				}
				setState(59); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==LANGLE );
			setState(61); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaContext extends ParserRuleContext {
		public TerminalNode SEMI() { return getToken(Mohawkv2Parser.SEMI, 0); }
		public List<CaentryContext> caentry() {
			return getRuleContexts(CaentryContext.class);
		}
		public CaentryContext caentry(int i) {
			return getRuleContext(CaentryContext.class,i);
		}
		public CaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ca; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterCa(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitCa(this);
		}
	}

	public final CaContext ca() throws RecognitionException {
		CaContext _localctx = new CaContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_ca);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(63); match(T__0);
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LANGLE) {
				{
				{
				setState(64); caentry();
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(70); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaentryContext extends ParserRuleContext {
		public Token d;
		public PreconditionContext e;
		public MyroleContext f;
		public TerminalNode LANGLE() { return getToken(Mohawkv2Parser.LANGLE, 0); }
		public TerminalNode ID() { return getToken(Mohawkv2Parser.ID, 0); }
		public TerminalNode RANGLE() { return getToken(Mohawkv2Parser.RANGLE, 0); }
		public PreconditionContext precondition() {
			return getRuleContext(PreconditionContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(Mohawkv2Parser.COMMA); }
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public TerminalNode COMMA(int i) {
			return getToken(Mohawkv2Parser.COMMA, i);
		}
		public CaentryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caentry; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterCaentry(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitCaentry(this);
		}
	}

	public final CaentryContext caentry() throws RecognitionException {
		CaentryContext _localctx = new CaentryContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_caentry);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72); match(LANGLE);
			setState(73); ((CaentryContext)_localctx).d = match(ID);
			setState(74); match(COMMA);
			setState(75); ((CaentryContext)_localctx).e = precondition();
			setState(76); match(COMMA);
			setState(77); ((CaentryContext)_localctx).f = myrole();
			setState(78); match(RANGLE);

					Rule r = simpleRule(RuleType.ASSIGN, ((CaentryContext)_localctx).f.role);
					r._preconditions = ((CaentryContext)_localctx).e.p;
					mohawkT.canAssign.addRule(r);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreconditionContext extends ParserRuleContext {
		public ArrayList<Role> p;
		public RoleconditionContext a;
		public RoleconditionContext b;
		public List<RoleconditionContext> rolecondition() {
			return getRuleContexts(RoleconditionContext.class);
		}
		public RoleconditionContext rolecondition(int i) {
			return getRuleContext(RoleconditionContext.class,i);
		}
		public TerminalNode COND(int i) {
			return getToken(Mohawkv2Parser.COND, i);
		}
		public List<TerminalNode> COND() { return getTokens(Mohawkv2Parser.COND); }
		public PreconditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterPrecondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitPrecondition(this);
		}
	}

	public final PreconditionContext precondition() throws RecognitionException {
		PreconditionContext _localctx = new PreconditionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_precondition);

			((PreconditionContext)_localctx).p =  new ArrayList<Role>();

		int _la;
		try {
			setState(93);
			switch (_input.LA(1)) {
			case ID:
			case NOT:
				enterOuterAlt(_localctx, 1);
				{
				setState(81); ((PreconditionContext)_localctx).a = rolecondition();

						_localctx.p.add(((PreconditionContext)_localctx).a.role);
					
				setState(89);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COND) {
					{
					{
					setState(83); match(COND);
					setState(84); ((PreconditionContext)_localctx).b = rolecondition();

								_localctx.p.add(((PreconditionContext)_localctx).b.role);
							
					}
					}
					setState(91);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case T__1:
				enterOuterAlt(_localctx, 2);
				{
				setState(92); match(T__1);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoleconditionContext extends ParserRuleContext {
		public Role role;
		public MyroleContext r;
		public TerminalNode NOT() { return getToken(Mohawkv2Parser.NOT, 0); }
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public RoleconditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rolecondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterRolecondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitRolecondition(this);
		}
	}

	public final RoleconditionContext rolecondition() throws RecognitionException {
		RoleconditionContext _localctx = new RoleconditionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_rolecondition);

			boolean not = false;

		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(95); match(NOT);
				not = true;
				}
			}

			setState(99); ((RoleconditionContext)_localctx).r = myrole();

					((RoleconditionContext)_localctx).role =  new Role(((RoleconditionContext)_localctx).r.role);
					_localctx.role._not = not;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CrContext extends ParserRuleContext {
		public List<CrentryContext> crentry() {
			return getRuleContexts(CrentryContext.class);
		}
		public TerminalNode SEMI() { return getToken(Mohawkv2Parser.SEMI, 0); }
		public CrentryContext crentry(int i) {
			return getRuleContext(CrentryContext.class,i);
		}
		public CrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterCr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitCr(this);
		}
	}

	public final CrContext cr() throws RecognitionException {
		CrContext _localctx = new CrContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_cr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102); match(T__3);
			setState(106);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LANGLE) {
				{
				{
				setState(103); crentry();
				}
				}
				setState(108);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(109); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CrentryContext extends ParserRuleContext {
		public Token mm;
		public MyroleContext nn;
		public TerminalNode LANGLE() { return getToken(Mohawkv2Parser.LANGLE, 0); }
		public TerminalNode ID() { return getToken(Mohawkv2Parser.ID, 0); }
		public TerminalNode RANGLE() { return getToken(Mohawkv2Parser.RANGLE, 0); }
		public TerminalNode COMMA() { return getToken(Mohawkv2Parser.COMMA, 0); }
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public CrentryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_crentry; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterCrentry(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitCrentry(this);
		}
	}

	public final CrentryContext crentry() throws RecognitionException {
		CrentryContext _localctx = new CrentryContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_crentry);
		try {
			setState(124);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(111); match(LANGLE);
				setState(112); ((CrentryContext)_localctx).mm = match(ID);
				setState(113); match(COMMA);
				setState(114); ((CrentryContext)_localctx).nn = myrole();
				setState(115); match(RANGLE);

						Rule r = simpleRule(RuleType.REVOKE, ((CrentryContext)_localctx).nn.role);
						mohawkT.canRevoke.addRule(r);
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(118); match(LANGLE);
				setState(119); match(T__5);
				setState(120); match(COMMA);
				setState(121); match(ID);
				setState(122); match(RANGLE);

						logmsg("Skipping CR entry as the admin is 'FALSE'");
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AdminContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(Mohawkv2Parser.ID); }
		public TerminalNode SEMI() { return getToken(Mohawkv2Parser.SEMI, 0); }
		public TerminalNode ID(int i) {
			return getToken(Mohawkv2Parser.ID, i);
		}
		public AdminContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_admin; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterAdmin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitAdmin(this);
		}
	}

	public final AdminContext admin() throws RecognitionException {
		AdminContext _localctx = new AdminContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_admin);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126); match(T__8);
			setState(128); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(127); match(ID);
				}
				}
				setState(130); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			setState(132); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpecContext extends ParserRuleContext {
		public Token su;
		public MyroleContext sr;
		public TerminalNode ID() { return getToken(Mohawkv2Parser.ID, 0); }
		public TerminalNode SEMI() { return getToken(Mohawkv2Parser.SEMI, 0); }
		public MyroleContext myrole() {
			return getRuleContext(MyroleContext.class,0);
		}
		public SpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_spec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterSpec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitSpec(this);
		}
	}

	public final SpecContext spec() throws RecognitionException {
		SpecContext _localctx = new SpecContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_spec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(134); match(T__2);
			setState(135); ((SpecContext)_localctx).su = match(ID);
			setState(136); ((SpecContext)_localctx).sr = myrole();
			setState(137); match(SEMI);

					mohawkT.query._timeslot = new TimeSlot(0);
					mohawkT.query._roles.add(((SpecContext)_localctx).sr.role);
					
					mohawkT.timeIntervalHelper.add(mohawkT.query._timeslot);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MyroleContext extends ParserRuleContext {
		public Role role;
		public Token a;
		public TerminalNode ID() { return getToken(Mohawkv2Parser.ID, 0); }
		public MyroleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_myrole; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).enterMyrole(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Mohawkv2Listener ) ((Mohawkv2Listener)listener).exitMyrole(this);
		}
	}

	public final MyroleContext myrole() throws RecognitionException {
		MyroleContext _localctx = new MyroleContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_myrole);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140); ((MyroleContext)_localctx).a = match(ID);

					((MyroleContext)_localctx).role =  new Role((((MyroleContext)_localctx).a!=null?((MyroleContext)_localctx).a.getText():null));
					mohawkT.roleHelper.add(_localctx.role);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\25\u0092\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3"+
		"\3\3\3\6\3(\n\3\r\3\16\3)\3\3\3\3\3\4\3\4\6\4\60\n\4\r\4\16\4\61\3\4\3"+
		"\4\3\5\3\5\3\5\3\5\3\5\3\5\6\5<\n\5\r\5\16\5=\3\5\3\5\3\6\3\6\7\6D\n\6"+
		"\f\6\16\6G\13\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\7\bZ\n\b\f\b\16\b]\13\b\3\b\5\b`\n\b\3\t\3\t\5\td\n\t\3"+
		"\t\3\t\3\t\3\n\3\n\7\nk\n\n\f\n\16\nn\13\n\3\n\3\n\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\177\n\13\3\f\3\f\6"+
		"\f\u0083\n\f\r\f\16\f\u0084\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16"+
		"\3\16\3\16\2\2\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\2\u008e\2\34\3\2"+
		"\2\2\4$\3\2\2\2\6-\3\2\2\2\b\65\3\2\2\2\nA\3\2\2\2\fJ\3\2\2\2\16_\3\2"+
		"\2\2\20c\3\2\2\2\22h\3\2\2\2\24~\3\2\2\2\26\u0080\3\2\2\2\30\u0088\3\2"+
		"\2\2\32\u008e\3\2\2\2\34\35\5\4\3\2\35\36\5\6\4\2\36\37\5\b\5\2\37 \5"+
		"\22\n\2 !\5\n\6\2!\"\5\26\f\2\"#\5\30\r\2#\3\3\2\2\2$\'\7\4\2\2%&\7\f"+
		"\2\2&(\b\3\1\2\'%\3\2\2\2()\3\2\2\2)\'\3\2\2\2)*\3\2\2\2*+\3\2\2\2+,\7"+
		"\22\2\2,\5\3\2\2\2-/\7\7\2\2.\60\7\f\2\2/.\3\2\2\2\60\61\3\2\2\2\61/\3"+
		"\2\2\2\61\62\3\2\2\2\62\63\3\2\2\2\63\64\7\22\2\2\64\7\3\2\2\2\65;\7\5"+
		"\2\2\66\67\7\r\2\2\678\7\f\2\289\7\17\2\29:\7\f\2\2:<\7\16\2\2;\66\3\2"+
		"\2\2<=\3\2\2\2=;\3\2\2\2=>\3\2\2\2>?\3\2\2\2?@\7\22\2\2@\t\3\2\2\2AE\7"+
		"\13\2\2BD\5\f\7\2CB\3\2\2\2DG\3\2\2\2EC\3\2\2\2EF\3\2\2\2FH\3\2\2\2GE"+
		"\3\2\2\2HI\7\22\2\2I\13\3\2\2\2JK\7\r\2\2KL\7\f\2\2LM\7\17\2\2MN\5\16"+
		"\b\2NO\7\17\2\2OP\5\32\16\2PQ\7\16\2\2QR\b\7\1\2R\r\3\2\2\2ST\5\20\t\2"+
		"T[\b\b\1\2UV\7\20\2\2VW\5\20\t\2WX\b\b\1\2XZ\3\2\2\2YU\3\2\2\2Z]\3\2\2"+
		"\2[Y\3\2\2\2[\\\3\2\2\2\\`\3\2\2\2][\3\2\2\2^`\7\n\2\2_S\3\2\2\2_^\3\2"+
		"\2\2`\17\3\2\2\2ab\7\21\2\2bd\b\t\1\2ca\3\2\2\2cd\3\2\2\2de\3\2\2\2ef"+
		"\5\32\16\2fg\b\t\1\2g\21\3\2\2\2hl\7\b\2\2ik\5\24\13\2ji\3\2\2\2kn\3\2"+
		"\2\2lj\3\2\2\2lm\3\2\2\2mo\3\2\2\2nl\3\2\2\2op\7\22\2\2p\23\3\2\2\2qr"+
		"\7\r\2\2rs\7\f\2\2st\7\17\2\2tu\5\32\16\2uv\7\16\2\2vw\b\13\1\2w\177\3"+
		"\2\2\2xy\7\r\2\2yz\7\6\2\2z{\7\17\2\2{|\7\f\2\2|}\7\16\2\2}\177\b\13\1"+
		"\2~q\3\2\2\2~x\3\2\2\2\177\25\3\2\2\2\u0080\u0082\7\3\2\2\u0081\u0083"+
		"\7\f\2\2\u0082\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0082\3\2\2\2\u0084"+
		"\u0085\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\7\22\2\2\u0087\27\3\2\2"+
		"\2\u0088\u0089\7\t\2\2\u0089\u008a\7\f\2\2\u008a\u008b\5\32\16\2\u008b"+
		"\u008c\7\22\2\2\u008c\u008d\b\r\1\2\u008d\31\3\2\2\2\u008e\u008f\7\f\2"+
		"\2\u008f\u0090\b\16\1\2\u0090\33\3\2\2\2\f)\61=E[_cl~\u0084";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}