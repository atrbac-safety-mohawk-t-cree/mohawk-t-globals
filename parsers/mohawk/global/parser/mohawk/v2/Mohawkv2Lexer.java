// Generated from D:\Work\Masters_Phd\workspace (GradSchool)\Mohawk-T Globals\parsers\mohawk\global\parser\mohawk\v2\Mohawkv2.g4 by ANTLR 4.4

package mohawk.global.parser.mohawk.v2;

import java.util.*;
import java.util.logging.*;

import org.apache.commons.lang3.StringUtils;
import mohawk.global.pieces.*;
import mohawk.global.helper.*;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class Mohawkv2Lexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__8=1, T__7=2, T__6=3, T__5=4, T__4=5, T__3=6, T__2=7, T__1=8, T__0=9, 
		ID=10, LANGLE=11, RANGLE=12, COMMA=13, COND=14, NOT=15, SEMI=16, COMMENT=17, 
		WS=18, NL=19;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'"
	};
	public static final String[] ruleNames = {
		"T__8", "T__7", "T__6", "T__5", "T__4", "T__3", "T__2", "T__1", "T__0", 
		"ID", "LANGLE", "RANGLE", "COMMA", "COND", "NOT", "SEMI", "COMMENT", "WS", 
		"NL"
	};


		public final Logger logger = Logger.getLogger("mohawk");

		int tabsize = 1;
		
		/* Global States */
		public MohawkT mohawkT = new MohawkT("Mohawk Reverse Converter: Mohawk");
		public Integer claimedRoles = null;
		
		
		public Rule simpleRule(RuleType ruleType, Role role) {
			Rule r = new Rule(ruleType, Role.allRoles(), new TimeInterval(0), 
				new ArrayList<Role>(), new ArrayList<TimeSlot>(Arrays.asList(new TimeSlot(0))), 
				role);
			return r;
		}
		
		private void logmsg(String msg) {
			if(logger.getLevel() == Level.FINE) {
				System.out.println(StringUtils.repeat("  ", tabsize) + msg);
			}
		}


	public Mohawkv2Lexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Mohawkv2.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\25\u0085\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3"+
		"\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\7"+
		"\13W\n\13\f\13\16\13Z\13\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3"+
		"\20\3\21\3\21\3\22\3\22\3\22\3\22\7\22l\n\22\f\22\16\22o\13\22\3\22\5"+
		"\22r\n\22\3\22\3\22\3\22\3\22\3\23\6\23y\n\23\r\23\16\23z\3\23\3\23\3"+
		"\24\5\24\u0080\n\24\3\24\3\24\3\24\3\24\2\2\25\3\3\5\4\7\5\t\6\13\7\r"+
		"\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25"+
		"\3\2\6\5\2C\\aac|\6\2\62;C\\aac|\4\2\f\f\17\17\5\2\13\13\16\16\"\"\u0089"+
		"\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2"+
		"\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2"+
		"\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2"+
		"\2\2\2%\3\2\2\2\2\'\3\2\2\2\3)\3\2\2\2\5/\3\2\2\2\7\65\3\2\2\2\t8\3\2"+
		"\2\2\13>\3\2\2\2\rD\3\2\2\2\17G\3\2\2\2\21L\3\2\2\2\23Q\3\2\2\2\25T\3"+
		"\2\2\2\27[\3\2\2\2\31]\3\2\2\2\33_\3\2\2\2\35a\3\2\2\2\37c\3\2\2\2!e\3"+
		"\2\2\2#g\3\2\2\2%x\3\2\2\2\'\177\3\2\2\2)*\7C\2\2*+\7F\2\2+,\7O\2\2,-"+
		"\7K\2\2-.\7P\2\2.\4\3\2\2\2/\60\7T\2\2\60\61\7q\2\2\61\62\7n\2\2\62\63"+
		"\7g\2\2\63\64\7u\2\2\64\6\3\2\2\2\65\66\7W\2\2\66\67\7C\2\2\67\b\3\2\2"+
		"\289\7H\2\29:\7C\2\2:;\7N\2\2;<\7U\2\2<=\7G\2\2=\n\3\2\2\2>?\7W\2\2?@"+
		"\7u\2\2@A\7g\2\2AB\7t\2\2BC\7u\2\2C\f\3\2\2\2DE\7E\2\2EF\7T\2\2F\16\3"+
		"\2\2\2GH\7U\2\2HI\7R\2\2IJ\7G\2\2JK\7E\2\2K\20\3\2\2\2LM\7V\2\2MN\7T\2"+
		"\2NO\7W\2\2OP\7G\2\2P\22\3\2\2\2QR\7E\2\2RS\7C\2\2S\24\3\2\2\2TX\t\2\2"+
		"\2UW\t\3\2\2VU\3\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y\26\3\2\2\2ZX\3\2"+
		"\2\2[\\\7>\2\2\\\30\3\2\2\2]^\7@\2\2^\32\3\2\2\2_`\7.\2\2`\34\3\2\2\2"+
		"ab\7(\2\2b\36\3\2\2\2cd\7/\2\2d \3\2\2\2ef\7=\2\2f\"\3\2\2\2gh\7\61\2"+
		"\2hi\7\61\2\2im\3\2\2\2jl\n\4\2\2kj\3\2\2\2lo\3\2\2\2mk\3\2\2\2mn\3\2"+
		"\2\2nq\3\2\2\2om\3\2\2\2pr\7\17\2\2qp\3\2\2\2qr\3\2\2\2rs\3\2\2\2st\7"+
		"\f\2\2tu\3\2\2\2uv\b\22\2\2v$\3\2\2\2wy\t\5\2\2xw\3\2\2\2yz\3\2\2\2zx"+
		"\3\2\2\2z{\3\2\2\2{|\3\2\2\2|}\b\23\2\2}&\3\2\2\2~\u0080\7\17\2\2\177"+
		"~\3\2\2\2\177\u0080\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082\7\f\2\2\u0082"+
		"\u0083\3\2\2\2\u0083\u0084\b\24\2\2\u0084(\3\2\2\2\b\2Xmqz\177\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}