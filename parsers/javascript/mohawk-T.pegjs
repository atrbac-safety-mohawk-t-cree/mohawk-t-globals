// Simple Mohawk+T / Cree Policy Parser
// Convert to js using https://pegjs.org/online
{
var policy = {
	query: {},
	expected: "UNKNOWN",
	canassign: [],
    canrevoke: [],
    canenable: [],
    candisable: [],
};
console.clear();
}

start
	= _ stat _ stat _ stat _ stat _ stat _ stat _
    {return policy;}
   
stat
	= canassign / canrevoke / canenable / candisable / query / expected
    
canassign
	= "CanAssign" _ "{" _ rules:myrule* _ "}"
	{
    	for(var r of rules) {
        	//console.log(r);
            policy.canassign.push(r);
        }
    	console.log("Leaving CanAssign");
    }
    
canrevoke
	= "CanRevoke" _ "{" _ rules:myrule* _ "}"
	{
    	for(var r of rules) {
        	//console.log(r);
            policy.canrevoke.push(r);
        }
    	console.log("Leaving CanRevoke");
   }

canenable
	= "CanEnable" _ "{" _ rules:myrule* _ "}"
	{
    	for(var r of rules) {
        	//console.log(r);
            policy.canenable.push(r);
        }
    	console.log("Leaving CanEnable");
    }
    
candisable
	= "CanDisable" _ "{" _ rules:myrule* _ "}"
	{
    	for(var r of rules) {
        	//console.log(r);
            policy.candisable.push(r);
        }
    	console.log("Leaving CanDisable");
   }
    
query=
	"Query" _ ":" _ t:timeslot _ "," _ ra:roleArray
	{
		policy.query = {timeslot:t, roles:ra};
	}

expected=
	"Expected" _ ":" _ a:("REACHABLE" / "UNREACHABLE" / "UNKNOWN")
	{
		policy.expected = a;
	}

myrule
	= "<" _ adminRole:myrole _ "," _ adminTimeInterval:timeInterval _ "," _
		precondition:precondition _ "," _ timeslotArray:timeslotArray _ "," _ targetRole:myrole _ ">" _
    {
    	return {
			adminRole:adminRole, adminTimeInterval:adminTimeInterval,
			precondition:precondition, timeslotArray:timeslotArray, targetRole:targetRole
		};
    }
    
precondition=
	"TRUE"{return [];}
	/
	a:rolecondition
	b:(
		_ "&" _ b:rolecondition {return b;}
	)*
	{
		var myvar = [];
		myvar.push(a);

		for(var bb of b) {
			myvar.push(bb);
		}
		return myvar;
	}

rolecondition =
	not:("NOT" _ "~" _)? role:myrole
	{
		return {role:role, not:(not === null)?false : true};
	}
    
roleArray =
	"[" _ a:myrole b:(_ "," _ b:myrole {return b;})* _ "]"
	{
		var myvar = [];
		myvar.push(a);

		for(var bb of b) {
			myvar.push(bb);
		}
		return myvar;
	}
	/ a:myrole {return [a];}

timeslotArray=
	"[" _ a:timeslot
	b:(_',' _ b:timeslot {return b;})* "]"
	{
		var myvar = [];
		myvar.push(a);

		for(var bb of b) {
			myvar.push(bb);
		}
		return myvar;
	}
	/ c:timeslot
	{
		return [c];
	}
	/
	"TRUE"
	{
		return [];
	}

myrole=
	a:[A-Za-z0-9]+
	{return text();}
    
    
timeInterval =
	a:timeslot b:('-' b:timeslot{return b;})?
	{
		return {start:a, end:(b === null)?a : b};
	}
    
timeslot=
	"t" t:[0-9]+
	{
		return parseInt(t);
	}

_ = [ \t\n\r]* comment? [ \t\n\r]*

comment = ws p:(single / multi) {return ['COMMENT',p]}

single = '//' p:([^\n]*) {return p.join('')}

multi = "/*" inner:(!"*/" i:. {return i})* "*/" {return inner.join('')}

ws = [ \t\n\r]*